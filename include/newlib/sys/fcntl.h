#ifndef _SYS_FCNTL_H_
#define _SYS_FCNTL_H_

#include <sys/_default_fcntl.h>

// Some extra defs that are disabled in newlib by default:
#define _FBINARY        0x10000
#define _FTEXT          0x20000
#define _FNOINHERIT     0x40000
#define _FDIRECT        0x80000
#define _FNOFOLLOW      0x100000
#define _FDIRECTORY     0x200000
#define _FEXECSRCH      0x400000

#define O_BINARY        _FBINARY
#define O_TEXT          _FTEXT
#define O_CLOEXEC       _FNOINHERIT
#define O_DIRECT        _FDIRECT
#define O_NOFOLLOW      _FNOFOLLOW
#define O_DSYNC         _FSYNC
#define O_RSYNC         _FSYNC
#define O_DIRECTORY     _FDIRECTORY
#define O_EXEC          _FEXECSRCH
#define O_SEARCH        _FEXECSRCH

#endif
