
#ifndef _SYS_IOCTL_H_
#define _SYS_IOCTL_H_

/**
 * Performs device-specific I/O.
 */
int ioctl(int fd, int request, void *arg);

// rootfs ioctl defs. First byte is 'r' for "uniqueness".
#define IO_SYNC 0x7200

#endif
