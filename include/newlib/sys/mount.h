
#ifndef _SYS_MOUNT_H_
#define _SYS_MOUNT_H_

/**
 * Mounts a filesystem or device file. Note that the latter is not normally
 * done through the mount syscall (because device files are normally created by
 * the kernel, which we don't really have). It just involves setting driver to
 * the device driver and (usually) not supplying a source.
 */
int mount(const char *source, const char *target, const char *driver, unsigned long mountflags, const void *arg);

/**
 * Unmounts a filesystem or device file.
 */
int umount(const char *path);

#endif
