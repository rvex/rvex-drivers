#ifndef _SYS_ROOTFS_H_
#define _SYS_ROOTFS_H_

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <rootfs.h>
#include <string.h>

typedef struct dirent sys_dirent_t;

/**
 * Forward declaration for driver descriptor structure.
 */
struct rootfs_driver_t;

/**
 * File attribute definitions.
 */
#define ROOTFS_ATTR_DIR             0x01
#define ROOTFS_ATTR_OBJECT_STATIC   0x02
#define ROOTFS_ATTR_CONTENTS_STATIC 0x04

/**
 * File mode definitions (for as far as they're used).
 */
#define ROOTFS_MODE_EXEC  0100
#define ROOTFS_MODE_WRITE 0200
#define ROOTFS_MODE_READ  0400
#define ROOTFS_MODE_RW    0600

/**
 * Represents a filesystem object; that is, the structure that defines a file
 * on the storage medium, which is just memory in this case.
 */
typedef struct rootfs_object_t {
  
  // If null, this is a regular file. 
  const struct rootfs_driver_t *mount;
  
  // Driver reference for the mount if mount is non-null. Unused if mount is
  // null.
  void *mount_ref;
  
  // POSIX file mode. Note that there is only one user and all files belong to
  // said user, so only the owner permissions are checked.
  short mode;
  
  // File attributes:
  //   ROOTFS_ATTR_DIR:
  //     indicates that this is a directory instead of a regular
  //     file. This is used both for normal file objects and mounted files.
  //   ROOTFS_ATTR_OBJECT_STATIC:
  //     Indicates that this object is statically allocated and should thus not
  //     be free()d if it is deleted.
  //   ROOTFS_ATTR_CONTENTS_STATIC:
  //     Indicates that contents points to a static object instead of a dynamic
  //     heap-allocated object.
  short attrib;
  
  // Number of directories that link to this object. . and .. entries are not
  // counted. A file can be deleted if [it is not mounted or arg is zero] and
  // links is zero. Deletion refers to freeing the contents and/or file object,
  // depending on the ROOTFS_ATTR_*_STATIC flags in attrib.
  short links;
  
  // Number of times that this object is currently open. An object can have
  // either a single exclusive reference, in which case anon_links is set to
  // -1, or it can have any number of shared references, in which case arg
  // indicates the amount of open files. Mounting or unmounting an object
  // (i.e. modifying mount) requires that anon_links is zero.
  short anon_links;
  
  // This points to the last directory in which this object was successfully
  // linked.
  struct rootfs_object_t *parent;
  
  // Amount of data currently allocated for the file. If this is a static file,
  // this cannot change.
  unsigned long capacity;
  
  // Current file size. Must be less than or equal to capacity.
  unsigned long size;
  
  // Pointer to the file contents. If the ROOTFS_ATTR_STATIC attribute is set,
  // this points to a statically allocated buffer. Otherwise, it is
  // heap-allocated by malloc() and friends. capacity always indicates the size
  // of this buffer. If capacity is 0, it is permissible for this to be NULL.
  void *contents;
  
} rootfs_object_t;

/**
 * Represents a directory entry. Directories are just files that contain an
 * array of these.
 */
typedef struct rootfs_dirent_t {
  
  // Pointer to rootfs_object representing the file. If null, this slot is
  // empty and should be skipped when seeking through a directory; this
  // prevents the need to memmove() objects when an entry is deleted.
  rootfs_object_t *ob;
  
  // Name of the file. Only filenames up to 12 characters are supported here.
  // The null byte is omitted when the length is 12 characters.
  char name[ROOTFS_NAME_LEN];
  
} rootfs_dirent_t;

/**
 * File open flag definitions.
 */
#define ROOTFS_O_READ       (O_RDONLY+1)
#define ROOTFS_O_WRITE      (O_WRONLY+1)
#define ROOTFS_O_APPEND     O_APPEND
#define ROOTFS_O_NONBLOCK   O_NONBLOCK
#define ROOTFS_O_DIRECTORY  O_DIRECTORY

// O_EXCL is abused for getting exclusive access to a file. That is, if
// ROOTFS_O_EXCLUSIVE is set during an open, the open will fail if the file is
// already open, and when the file is opened this way, subsequent open calls
// will fail until the file is closed.
#define ROOTFS_O_EXCLUSIVE  O_EXCL

/**
 * Represents an open file or directory. These structures are always
 * dynamically allocated. The system maintains a static list of these for
 * a POSIX file descriptor to object reference mapping.
 */
typedef struct rootfs_object_ref_t {
  
  // Pointer to the object. When this file refers to an object within a
  // directory mount, this object points to the mount point, and drv_ref is
  // used to distinguish between files within the mount. null is used to
  // signal an unused file descriptor.
  rootfs_object_t *ob;
  
  // File open flags:
  //   ROOTFS_O_READ:
  //     Calls to read() are permitted.
  //   ROOTFS_O_WRITE:
  //     Calls to write() are permitted.
  //   (ROOTFS_)O_APPEND:
  //     Calls to write() first call an lseek() to the end of the file.
  //   (ROOTFS_)O_DIRECTORY:
  //     Specifies that this object was opened with opendir instead of open.
  // Note that stat() will open a file with no read/write permissions, while
  // open() will use ROOTFS_O_READ and/or ROOTFS_O_WRITE.
  int flags;
  
  union {
    
    // Current file offset, used for normal (non-mounted) files.
    off_t offs;
    
    // Pointer to the driver file structure, used for mounted files.
    void *drv_ref;
    
  };
  
} rootfs_object_ref_t;

/**
 * Driver mount callback definition.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  void *drv_ref; // Driver mount reference.
} rootfs_mount_ret_t;

typedef rootfs_mount_ret_t (*rootfs_mount_t) (
  int,           // Set if the filesystem object is a directory.
  char *,        // Source file path if applicable. Can be mutated by the call.
  unsigned long, // Mount flags.
  const void *   // Mount information.
);

/**
 * Driver umount callback definition.
 */
typedef int (*rootfs_umount_t) (
  void *         // Driver mount reference.
);

/**
 * Driver open callback definition.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  void *drv_ref; // Driver file reference.
} rootfs_open_ret_t;

typedef rootfs_open_ret_t (*rootfs_open_t) (
  void*,         // Driver mount reference.
  const char*,   // Filename within the mounted filesystem, starting with a
                 //   '/', or null for single-file mounts.
  int,           // Open flags from <fcntl.h>, incremented by 1.
  int            // File mode for newly created files.
);

/**
 * Driver read callback definition. This must always behave as if O_NONBLOCK is
 * set. The rootfs_read() function makes it blocking if necessary.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  size_t amount; // Amount of bytes read.
} rootfs_read_ret_t;

typedef rootfs_read_ret_t (*rootfs_read_t) (
  void*,         // Driver mount reference.
  void*,         // Driver file reference.
  void*,         // Buffer for the read bytes.
  size_t         // Number of bytes to read.
);

/**
 * Driver write callback definition. This must always behave as if O_NONBLOCK is
 * set. The rootfs_write() function makes it blocking if necessary.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  size_t amount; // Amount of bytes written.
} rootfs_write_ret_t;

typedef rootfs_write_ret_t (*rootfs_write_t) (
  void*,         // Driver mount reference.
  void*,         // Driver file reference.
  const void*,   // Buffer containing the bytes to be written.
  size_t         // Number of bytes to write.
);

/**
 * Driver close callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_close_t) (
  void*,         // Driver mount reference.
  void*          // Driver file reference.
);

/**
 * Driver lseek callback definition.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  off_t pos;     // Absolute file position after the seek if successful.
} rootfs_lseek_ret_t;

typedef rootfs_lseek_ret_t (*rootfs_lseek_t) (
  void*,         // Driver mount reference.
  void*,         // Driver file reference.
  off_t,         // Seek offset.
  int            // SEEK_SET, SEEK_CUR or SEEK_END.
);

/**
 * Driver stat callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_stat_t) (
  void*,         // Driver mount reference.
  void*,         // Driver file reference.
  struct stat*   // stat structure that the file info is returned in.
);

/**
 * Driver ioctl callback definition. Returns errno or 0 if successful.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  int result;    // Result, depending on request.
} rootfs_ioctl_ret_t;

typedef rootfs_ioctl_ret_t (*rootfs_ioctl_t) (
  void*,         // Driver mount reference.
  void*,         // Driver file reference.
  int,           // Request.
  void*          // Parameter, depending on request.
);

/**
 * Driver link callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_link_t) (
  void*,         // Driver mount reference.
  const char*,   // Path to the target file, starting with '/'.
  const char*    // Path for the new link, starting with '/'.
);

/**
 * Driver unlink callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_unlink_t) (
  void*,         // Driver mount reference.
  const char*    // Path to the target file, starting with '/'.
);

/**
 * Driver rename callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_rename_t) (
  void*,         // Driver mount reference.
  const char*,   // Old path, starting with '/'.
  const char*    // New path, starting with '/'.
);

/**
 * Driver mkdir callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_mkdir_t) (
  void*,         // Driver mount reference.
  const char*,   // Path for the new directory, starting with '/'.
  int            // Directory mode.
);

/**
 * Driver opendir callback definition.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  void *drv_ref; // Driver directory reference.
} rootfs_opendir_ret_t;

typedef rootfs_opendir_ret_t (*rootfs_opendir_t) (
  void*,         // Driver mount reference.
  const char*    // Filename within the mounted filesystem, starting with a '/'.
);

/**
 * Driver readdir_r callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_readdir_r_t) (
  void*,         // Driver mount reference.
  void*,         // Driver directory reference.
  sys_dirent_t*, // Buffer for the directory entry.
  sys_dirent_t** // Result for readdir(); either set to the buffer or to NULL.
);

/**
 * Driver closedir callback definition. Returns errno or 0 if successful.
 */
typedef int (*rootfs_closedir_t) (
  void*,         // Driver mount reference.
  void*          // Driver directory reference.
);

/**
 * Driver seekdir callback definition. The following values are allowed for the
 * seek offset:
 *  - -1: return the current position without manipulating the pointer.
 *  - 0: rewind to the start of the directory.
 *  - value previously returned by seekdir(ref, -1): seek back to that
 *    position.
 */
typedef struct {
  int err_no;    // errno or zero if successful.
  off_t pos;     // Absolute file position after the seek if successful.
} rootfs_seekdir_ret_t;

typedef rootfs_seekdir_ret_t (*rootfs_seekdir_t) (
  void*,         // Driver mount reference.
  void*,         // Driver file reference.
  off_t          // Seek offset.
);

/**
 * Represents a driver information structure.
 */
typedef struct rootfs_driver_t {
  
  /**
   * Callback functions for the driver. When null, syscalls requiring a
   * callback will return ENOSYS. At least mount must be non-null.
   */
  rootfs_mount_t     mount;
  rootfs_umount_t    umount;
  rootfs_open_t      open;
  rootfs_read_t      read;
  rootfs_write_t     write;
  rootfs_close_t     close;
  rootfs_lseek_t     lseek;
  rootfs_stat_t      stat;
  rootfs_ioctl_t     ioctl;
  rootfs_link_t      link;
  rootfs_unlink_t    unlink;
  rootfs_rename_t    rename;
  rootfs_mkdir_t     mkdir;
  rootfs_opendir_t   opendir;
  rootfs_readdir_r_t readdir_r;
  rootfs_closedir_t  closedir;
  rootfs_seekdir_t   seekdir;
  
  /**
   * Type of file created by this driver, one of DT_* from sys/dirent.h.
   */
  int d_type;
  
  /**
   * Driver name, 16 characters max, null-terminated if less. Used by
   * rootfs_mount to look up this structure.
   */
  char name[16];
  
} rootfs_driver_t;

/**
 * Use this macro to register a driver with librootfs. Usage example:
 * REGISTER_DRIVER(rvex_uart) {
 *   .name = "rvex-uart",
 *   .mount = [mount_fn],
 *   .umount = [umount_fn],
 *   ...
 * };
 */
#define ROOTFS_REGISTER_DRIVER(id) \
  REQUIRE_ROOTFS(id); \
  static const volatile rootfs_driver_t __ ## id \
    __attribute__((used)) \
    __attribute__ ((section (".pnp.rootfs_drivers.list"))) =

/**
 * Collapses separators to single slashes, and ensures that the path starts
 * with a '/'. Returns 0 or the appropriate errno.
 */
int sys_rootfs_sanitize_path(char *path);

/**
 * Resolves a path to an object file. Returns 0 or the appropriate errno.
 */
int sys_rootfs_resolve(
  char **path,                // **path is sanitized, *path is set to the path
                              //   remnant for the mount, or NULL for
                              //   non-mounts.
  rootfs_object_t **ob,       // *ob is set to point to the resolved object.
  rootfs_dirent_t **dirent,   // *dirent is set to point to the directory entry
                              //   for the resolved object, or NULL if *ob is
                              //   the root directory. Can be NULL.
  rootfs_object_t **dir,      // *dir is set to point to the directory
                              //   containing the resolved object, or NULL if
                              //   *ob is the root directory. Can be NULL.
  rootfs_object_t *exclude    // If this object is encountered while iterating,
                              //   EINVAL is returned (used by rename).
);

/**
 * Opens an existing file by object. When opening a file within a directory
 * mount, path specifies the portion of the path within the mount; otherwise
 * path must be NULL. Returns errno or 0 on success. flags must already have
 * been incremented by 1 to get read/write access as a bitmask.
 */
int sys_rootfs_open_ob(rootfs_object_ref_t *ref, rootfs_object_t *ob, const char *path, int flags, int mode);

/**
 * Frees the dynamic memory consumed by an object if all references to it are
 * gone.
 */
void sys_rootfs_free_ob(rootfs_object_t *ob);

/**
 * Internal close function, which actually closes a non-mounted reference
 * for sys_rootfs_close() and sys_rootfs_closedir(),
 */
void sys_rootfs_close_int(rootfs_object_ref_t *ref);

/**
 * Adds a directory entry to ob at the specified path. The supplied path is
 * expected to already be sanitized, and is expected to not already exist.
 * Returns errno or 0 on success. *path is undefined after the call.
 */
int sys_rootfs_link_ob(rootfs_object_t *ob, char *path);

/**
 * Returns the file type of the specified object using the dirent.h constants
 * (DT_*). The stat constants (S_IF*) can be retrieved by leftshifting by 12
 * and masking with S_IFMT.
 */
int sys_rootfs_getftype(const rootfs_object_t *ob);

/**
 * Opens/creates a file by name. Returns errno or 0 on success. flags must
 * already have been incremented by 1 to get read/write access as a bitmask.
 * *path is undefined after the call.
 */
int sys_rootfs_open(rootfs_object_ref_t *ref, char *path, int flags, int mode);

/**
 * Reads from an open file.
 */
rootfs_read_ret_t sys_rootfs_read(rootfs_object_ref_t *ref, void *buf, size_t amount);

/**
 * Writes to an open file.
 */
rootfs_write_ret_t sys_rootfs_write(rootfs_object_ref_t *ref, const void *buf, size_t amount);

/**
 * Closes a file.
 */
int sys_rootfs_close(rootfs_object_ref_t *ref);

/**
 * Seeks within a file.
 */
rootfs_lseek_ret_t sys_rootfs_lseek(rootfs_object_ref_t *ref, off_t offset, int whence);

/**
 * Stats an open file.
 */
int sys_rootfs_stat(rootfs_object_ref_t *ref, struct stat *sb);

/**
 * ioctl for an open file.
 */
rootfs_ioctl_ret_t sys_rootfs_ioctl(rootfs_object_ref_t *ref, int request, void *param);

/**
 * Adds a directory entry for path1 to path2. Returns errno or 0 on succeess.
 * The paths are undefined after the call.
 */
int sys_rootfs_link(char *path1, char *path2);

/**
 * Removes the link specified by path. Returns errno or 0 on success.
 */
int sys_rootfs_unlink(char *path);

/**
 * Renames/moves a file. Returns errno or 0 on success.
 */
int sys_rootfs_rename(char *from, char *to);

/**
 * Makes a directory. Returns errno or 0 on success.
 */
int sys_rootfs_mkdir(char *path, int mode);

/**
 * Opens a directory. Returns errno or 0 on success.
 */
int sys_rootfs_opendir(rootfs_object_ref_t *ref, char *path);

/**
 * Reads the next entry from an open directory. entry must point to a
 * caller-allocated buffer for the directory entry. result is set to that
 * buffer after populating it, or to NULL if the end of the directory has been
 * reached. Returns errno or 0 on success/EOF.
 */
int sys_rootfs_readdir_r(rootfs_object_ref_t *ref, sys_dirent_t *entry, sys_dirent_t **result);

/**
 * Closes a directory. Returns errno or 0 on success.
 */
int sys_rootfs_closedir(rootfs_object_ref_t *ref);

/**
 * Seeks within a directory or tells the current position. The following values
 * are allowed for offset:
 *  - -1: return the current position without manipulating the pointer.
 *  - 0: rewind to the start of the directory.
 *  - value previously returned by seekdir(ref, -1): seek back to that
 *    position.
 */
rootfs_seekdir_ret_t sys_rootfs_seekdir(rootfs_object_ref_t *ref, _off_t offset);

/**
 * Mounts a filesystem driver at the specified path. The driver is identified
 * by the "driver" string. arg is driver-specific. Returns errno or 0 on
 * success. The supplied path buffers must be considered to be invalid after
 * the call.
 */
int sys_rootfs_mount(char *source, char *target, const char *driver, unsigned long mountflags, const void *arg);

/**
 * Unmounts the specified mount point. Returns errno or 0 on success. The
 * supplied path buffer must be considered to be invalid after the call.
 */
int sys_rootfs_umount(char *path);

/**
 * Unmounts the specified mount point using an object reference. force allows
 * the umount to be forced; this should only be done by exit if some kind of
 * messed up state prevents this from being done cleanly.
 */
int sys_rootfs_umount_ob(rootfs_object_t *ob, int force);

#endif
