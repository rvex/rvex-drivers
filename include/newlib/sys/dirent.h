#ifndef _SYS_DIRENT_H_
#define _SYS_DIRENT_H_

#include <sys/types.h>

/**
 * dirent structure.
 */
struct dirent {
  
  // "inode" number. This points to the file object for rootfs, but FS drivers
  // can do whatever they want with it.
  long d_ino;
  
  // Filesize, because it is easy to access for the drivers.
  off_t d_size;
  
  // File mode (permissions).
  short d_mode;
  
  // Number of hardlinks.
  short d_nlinks;
  
  // Nonzero for directories, zero for normal files.
  unsigned char d_type;
  
  // Name of the file, null-terminated.
  char d_name[256];
  
};

/**
 * d_type values.
 */
enum {
  DT_UNKNOWN = 0,
  DT_FIFO = 1,
  DT_CHR = 2,
  DT_DIR = 4,
  DT_BLK = 6,
  DT_REG = 8,
  DT_LNK = 10,
  DT_SOCK = 12,
  DT_WHT = 14
};

#define DT_UNKNOWN  DT_UNKNOWN
#define DT_FIFO     DT_FIFO
#define DT_CHR      DT_CHR
#define DT_DIR      DT_DIR
#define DT_BLK      DT_BLK
#define DT_REG      DT_REG
#define DT_LNK      DT_LNK
#define DT_SOCK     DT_SOCK
#define DT_WHT      DT_WHT

/**
 * We don't have a directory structure; for us the pointer itself is just a
 * file descriptor number. We increment the number by 1 such that fd=0 doesn't
 * become NULL.
 */
typedef void DIR;

/**
 * Closes a directory, interface matches POSIX.
 */
int closedir(DIR *dirp);

/**
 * Opens a directory, interface matches POSIX.
 */
DIR *opendir(const char *path);

/**
 * Read from a directory, interfaces match POSIX.
 */
struct dirent *readdir(DIR *dirp);
int readdir_r(DIR *dirp, struct dirent *entry, struct dirent **result);

/**
 * Rewinds a directory, interface matches POSIX.
 */
void rewinddir(DIR *dirp);

/**
 * Seek within a directory, interface matches POSIX.
 */
void seekdir(DIR *dirp, long int pos);

/**
 * Get the current directory pointer, interface matches POSIX.
 */
long int telldir(DIR *dirp);

#endif
