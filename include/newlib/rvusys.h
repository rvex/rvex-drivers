
#ifndef _RVUSYS_H_
#define _RVUSYS_H_

#include <machine/rvex-sys.h>
#include <rvex-uart-defs.h>

/**
 * This macro registers the address of the UART device for librvusys. It also
 * forces inclusion of the library.
 */
#define PNP_REGISTER_RVEX_UART(id, addr) PNP_REGISTER(rvusys_device, id, addr)

#endif
