
#ifndef _GRPS2_H_
#define _GRPS2_H_

#include <input.h>

/**
 * Structure passed to arg of this driver's mount function.
 */
typedef struct mount_ps2_arg_t {
  
  // Address of the APBPS2 peripheral.
  long address;
  
  // Interrupt handler associated with the peripheral.
  int irq;
  
} mount_ps2_arg_t;

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created. irq must be the interrupt number associated
 * with the peripheral, as passed to irq_register(), irq_enable(), and
 * irq_clear(), which must be available through some other library. While we're
 * listing requirements, sys_gettimeofday() is also used.
 */
int mount_grps2(const char *path, long address, int irq);

#endif
