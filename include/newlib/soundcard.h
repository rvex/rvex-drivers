
#ifndef _SOUNDCARD_H_
#define _SOUNDCARD_H_

// This file is an INCOMPATIBLE SUBSET of linux/soundcard.h.

#define SNDCTL_DSP_RESET            0x50000000
#define SNDCTL_DSP_SYNC             0x50010000
#define SNDCTL_DSP_SPEED            0x50020004
#define SNDCTL_DSP_STEREO           0x50030004
#define SNDCTL_DSP_GETBLKSIZE       0x50040004
#define SNDCTL_DSP_SAMPLESIZE       SNDCTL_DSP_SETFMT
#define SNDCTL_DSP_CHANNELS         0x50060004
#define SOUND_PCM_WRITE_CHANNELS    SNDCTL_DSP_CHANNELS
#define SOUND_PCM_WRITE_FILTER      0x50070004
#define SNDCTL_DSP_POST             0x50080000
#define SNDCTL_DSP_SUBDIVIDE        0x50090004
#define SNDCTL_DSP_SETFRAGMENT      0x500A0004

/*	Audio data formats (Note! U8=8 and S16_LE=16 for compatibility) */
#define SNDCTL_DSP_GETFMTS          0x500B0000
#define SNDCTL_DSP_SETFMT           0x50050000
#define AFMT_QUERY                  0x00000000  /* Return current fmt */
#define AFMT_MU_LAW                 0x00000001
#define AFMT_A_LAW                  0x00000002
#define AFMT_IMA_ADPCM              0x00000004
#define AFMT_U8                     0x00000008
#define AFMT_S16_LE                 0x00000010  /* Little endian signed 16*/
#define AFMT_S16_BE                 0x00000020  /* Big endian signed 16 */
#define AFMT_S16_NE                 0x00000020  /* Big endian signed 16 */
#define AFMT_S8                     0x00000040
#define AFMT_U16_LE                 0x00000080  /* Little endian U16 */
#define AFMT_U16_BE                 0x00000100  /* Big endian U16 */
#define AFMT_U16_NE                 0x00000100  /* Big endian U16 */
#define AFMT_MPEG                   0x00000200  /* MPEG (2) audio */
#define AFMT_AC3                    0x00000400  /* Dolby Digital AC3 */

/*
 * Buffer status queries.
 */
typedef struct audio_buf_info {
  int fragments;  /* # of available fragments (partially usend ones not counted) */
  int fragstotal; /* Total # of fragments allocated */
  int fragsize;   /* Size of a fragment in bytes */
  int bytes;      /* Available space in bytes (includes partially used fragments) */
  /* Note! 'bytes' could be more than fragments*fragsize */
} audio_buf_info;

#define SNDCTL_DSP_GETOSPACE        0x500C0010
#define SNDCTL_DSP_GETISPACE        0x500D0010
#define SNDCTL_DSP_NONBLOCK         0x500E0000
#define SNDCTL_DSP_GETCAPS          0x500F0004
#define DSP_CAP_REVISION            0x000000ff  /* Bits for revision level (0 to 255) */
#define DSP_CAP_DUPLEX              0x00000100  /* Full duplex record/playback */
#define DSP_CAP_REALTIME            0x00000200  /* Real time capability */
#define DSP_CAP_BATCH               0x00000400  /* Device has some kind of */
                                                /* internal buffers which may */
                                                /* cause some delays and */
                                                /* decrease precision of timing */
#define DSP_CAP_COPROC              0x00000800  /* Has a coprocessor */
                                                /* Sometimes it's a DSP */
                                                /* but usually not */
#define DSP_CAP_TRIGGER             0x00001000  /* Supports SETTRIGGER */
#define DSP_CAP_MMAP                0x00002000  /* Supports mmap() */
#define DSP_CAP_MULTI               0x00004000  /* support multiple open */
#define DSP_CAP_BIND                0x00008000  /* channel binding to front/rear/cneter/lfe */


#endif
