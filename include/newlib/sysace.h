
#ifndef _SYSACE_H_
#define _SYSACE_H_

/**
 * Mount function for this driver. This is pretty much just mount() in a nice
 * wrapper. This also creates the file if it doesn't already exist. Most
 * importantly though, using this function to do the mount ensures that the
 * driver static library is actually loaded.
 */
int mount_sysace(const char *path, long address);

#endif
