
#ifndef _FB_H_
#define _FB_H_

/**
 * ioctl requests.
 */
#define FBIOGET_VSCREENINFO   0x4600 /* Get fb_var_screeninfo           */
#define FBIOPUT_VSCREENINFO   0x4601 /* Put fb_var_screeninfo           */
#define FBIOGET_FSCREENINFO   0x4602 /* Get fb_fix_screeninfo           */
#define FBIOGETCMAP           0x4604 /* Get 256*3 char (rgb) colormap   */
#define FBIOPUTCMAP           0x4605 /* Put 256*3 char (rgb) colormap   */
#define FBIO_WAITFORVSYNC     0x4620 /* Block until vsync               */

#define __u32 unsigned int
#define __u16 unsigned short

/**
 * Bitfield structure used in variable screen info structure.
 */
struct fb_bitfield {
  __u32 offset;               /* beginning of bitfield                  */
  __u32 length;               /* length of bitfield                     */
  __u32 msb_right;            /* != 0 : Most significant bit is         */ 
                                  /* right                              */ 
};

/**
 * Variable screen info structure for FBIOGET_VSCREENINFO and
 * FBIOPUT_VSCREENINFO.
 */
struct fb_var_screeninfo {
  __u32 xres;                 /* visible resolution                     */
  __u32 yres;
  __u32 xres_virtual;         /* virtual resolution                     */
  __u32 yres_virtual;
  __u32 xoffset;              /* offset from virtual to visible         */
  __u32 yoffset;              /* resolution                             */
  
  __u32 bits_per_pixel;       /* guess what                             */
  __u32 grayscale;            /* != 0 Graylevels instead of colors      */
  
  struct fb_bitfield red;     /* bitfield in fb mem if true color,      */
  struct fb_bitfield green;   /* else only length is significant        */
  struct fb_bitfield blue;
  struct fb_bitfield transp;  /* transparency                           */    
  
  __u32 nonstd;               /* != 0 Non standard pixel format         */
  
  __u32 activate;             /* see FB_ACTIVATE_*                      */
  
  __u32 height;               /* height of picture in mm                */
  __u32 width;                /* width of picture in mm                 */
  
  __u32 accel_flags;          /* acceleration flags (hints)             */

  /* Timing: All values in pixclocks, except pixclock (of course)       */
  __u32 pixclock;             /* pixel clock in ps (pico seconds)       */
  __u32 left_margin;          /* time from sync to picture              */
  __u32 right_margin;         /* time from picture to sync              */
  __u32 upper_margin;         /* time from sync to picture              */
  __u32 lower_margin;                                                   
  __u32 hsync_len;            /* length of horizontal sync              */
  __u32 vsync_len;            /* length of vertical sync                */
  __u32 sync;                 /* see FB_SYNC_*                          */
  __u32 vmode;                /* see FB_VMODE_*                         */
  __u32 reserved[6];          /* Reserved for future compatibility      */
};

/**
 * Fixed screen info structure for FBIOGET_FSCREENINFO.
 */
struct fb_fix_screeninfo {
  char id[16];                /* identification string eg "TT Builtin"  */
  unsigned long smem_start;   /* Start of frame buffer mem              */
                                  /* (physical address)                 */
  __u32 smem_len;             /* Length of frame buffer mem             */
  __u32 type;                 /* see FB_TYPE_*                          */
  __u32 type_aux;             /* Interleave for interleaved Planes      */
  __u32 visual;               /* see FB_VISUAL_*                        */ 
  __u16 xpanstep;             /* zero if no hardware panning            */
  __u16 ypanstep;             /* zero if no hardware panning            */
  __u16 ywrapstep;            /* zero if no hardware ywrap              */
  __u32 line_length;          /* length of a line in bytes              */
  unsigned long mmio_start;   /* Start of Memory Mapped I/O             */
                                  /* (physical address)                 */
  __u32 mmio_len;             /* Length of Memory Mapped I/O            */
  __u32 accel;                /* Type of acceleration available         */
  __u16 reserved[3];          /* Reserved for future compatibility      */
};

#undef __u32
#undef __u16

#endif
