
#ifndef _GRI2C_H_
#define _GRI2C_H_

/**
 * ioctl request for setting the slave address. The value is copied from
 * linux/i2c-dev.h, but that's about where the comparison ends. The
 * implementations are NOT compatible.
 */
#define I2C_SLAVE 0x0703

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created. ambaclk must be the AMBA clock frequency in
 * kHz. This also creates the file if it doesn't already exist. Most
 * importantly though, using this function to do the mount ensures that the
 * driver static library is actually loaded.
 */
int mount_gri2c(const char *path, long ambaclk, long address);

#endif
