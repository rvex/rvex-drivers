
#ifndef _FILESLICE_H_
#define _FILESLICE_H_

/**
 * Argument structure for mounting a fileslice file.
 */
typedef struct mount_fileslice_arg_t {
  
  // Start offset.
  off_t start;
  
  // Size.
  off_t size;
  
} mount_fileslice_arg_t;

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created.
 */
int mount_fileslice(const char *src, const char *target, off_t start, off_t size);

#endif
