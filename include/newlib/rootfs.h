#ifndef _ROOTFS_H_
#define _ROOTFS_H_

#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>

/**
 * Maximum length of a file or directory name within the RAM filesystem.
 * Files within mounted filesystems do not have this limit.
 */
#define ROOTFS_NAME_LEN 12

/**
 * Use this macro to require inclusion of librootfs.
 */
#define REQUIRE_ROOTFS(id) \
  extern const long rootfs_drivers_dummy; \
  static const long *__ ## id ## _needs_rootfs \
    __attribute__ ((section (".discard"))) __attribute__ ((unused)) \
    = (const long *)(&rootfs_drivers_dummy) \

REQUIRE_ROOTFS(rootfs_h);

/**
 * Implementation of touch, used by driver mount() functions a lot to ensure
 * file existence.
 */
int touch(const char *fname);

/**
 * Argument record for mounting a "block" file.
 */
typedef struct mount_block_arg_t {
  
  // Start of the mounted memory region.
  void *start;
  
  // Size of the mounted memory region.
  size_t size;
  
} mount_block_arg_t;

/**
 * Creates a memory-mapped block file.
 */
int mount_block(const char *path, void *start, size_t size);

/**
 * Reads the master boot record in dev and tries to mount its partitions as
 * dev1..4. Returns -1 if the disk does not appear to have a valid MBR, a
 * mount failed, or something else went wrong.
 */
int mount_mbr(const char *dev);

/**
 * Syncs a file to the underlying storage medium.
 */
int fsync(int fd);

/**
 * Syncs a file's contents (not necessarily metadata as well) to the underlying
 * storage medium.
 */
int fdatasync(int fd);

#endif
