
#ifndef _FATFS_H_
#define _FATFS_H_

/**
 * Mount function for this driver. This is pretty much just "mount".
 */
int mount_fatfs(const char *source, const char *target, int readonly);

#endif
