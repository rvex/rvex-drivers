
#ifndef _DSP_H_
#define _DSP_H_

#include <soundcard.h>

// 8-bit or 16-bit register.
#define DSP_MOUNT_FMT_8BIT     0x00000000
#define DSP_MOUNT_FMT_16BIT    0x00000001

// Mono or stereo register (MSB is first channel).
#define DSP_MOUNT_FMT_MONO     0x00000000
#define DSP_MOUNT_FMT_STEREO   0x00000002

// Signed or unsigned format.
#define DSP_MOUNT_FMT_UNSIGNED 0x00000000
#define DSP_MOUNT_FMT_SIGNED   0x00000004

// Big or little endian.
#define DSP_MOUNT_FMT_BIG      0x00000000
#define DSP_MOUNT_FMT_LITTLE   0x00000008

// Mask including the above format flags.
#define DSP_MOUNT_FMT_MASK     0x0000000F

// Specifies that the scaler is diminished-one encoded.
#define DSP_MOUNT_PRES_DIM_ONE 0x00000010

// Status register format. If *_REMAIN, the status register should indicate the
// number of samples that are currently queued up in the hardware. If *_AVAIL,
// it should specify the amount of space available in the buffer.
#define DSP_MOUNT_STAT_REMAIN  0x00000000
#define DSP_MOUNT_STAT_AVAIL   0x00000020

// Whether the buffer size is an address or a value.
#define DSP_MOUNT_SIZE_DIRECT  0x00000000
#define DSP_MOUNT_SIZE_REG     0x00000040

/**
 * DSP mount argument structure.
 */
typedef struct mount_dsp_arg_t {
  
  // DSP_MOUNT_* flags.
  int flags;
  
  // Input frequency for the samplerate scalar, or the fixed samplerate if no
  // scalar is present.
  float frequency;
  
  // Scaler registers. When both are specified, they are written in sequence.
  // This is useful for counters that can get stuck if the value gets greater
  // than the limit register. In that case the second register should be the
  // value. If scale1 is NULL, a fixed samplerate is expected, which should be
  // specified by frequency.
  volatile unsigned int *scale1;
  volatile unsigned int *scale2;
  
  // Data register. Format specified in flags. The performed memory access is
  // always a word, but if the format is less wide, it is replicated across
  // the word.
  volatile unsigned int *data;
  
  // Status register, indicating how many samples are in the buffer. The exact
  // significance is determined by the status flags.
  volatile unsigned int *status;
  
  // Size register or value.
  union {
    const volatile unsigned int *size_reg;
    unsigned int size_direct;
  };
  
} mount_dsp_arg_t;

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created.
 */
int mount_dsp(const char *path, const mount_dsp_arg_t *arg);

#endif
