
#ifndef _GRSVGA_H_
#define _GRSVGA_H_

#include <fb.h>

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created. address must be set to the peripheral address
 * of the svgactrl peripheral.
 */
int mount_grsvga(const char *path, long address);

#endif
