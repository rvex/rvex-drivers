
#ifndef _RVEX_UART_H_
#define _RVEX_UART_H_

#include <rvex-uart-defs.h>

/**
 * UART access functions.
 */
int  rvex_uart_putchar(volatile rvex_uart_regs_t *regs, int c);
int  rvex_uart_puts   (volatile rvex_uart_regs_t *regs, const char *s);
int  rvex_uart_putx   (volatile rvex_uart_regs_t *regs, int value);
int  rvex_uart_putd   (volatile rvex_uart_regs_t *regs, int value);
int  rvex_uart_gets   (volatile rvex_uart_regs_t *regs, char *buf, int len);
int  rvex_uart_write  (volatile rvex_uart_regs_t *regs, const void *buf, int count);
int  rvex_uart_read   (volatile rvex_uart_regs_t *regs, void *buf, int count);

/**
 * Simplified functions that don't take a UART register structure as parameter.
 * Instead they use RVEX_UART, a global const which must be defined by the 
 */
int putchar(int c);
int puts(const char *s);
int putx(int value);
int putd(int value);
int gets(char *buf, int len);

/**
 * Address of the primary UART.
 */
extern volatile rvex_uart_regs_t * const RVEX_UART;

/**
 * Macro to define the address of the primary UART.
 */
#define RVEX_UART_AT(addr) volatile rvex_uart_regs_t * const RVEX_UART = (volatile rvex_uart_regs_t*)(addr);

#endif
