#ifndef _IRQMP_H_
#define _IRQMP_H_

#include <irq.h>

// Interrupt controller interface.
typedef struct {
  unsigned int level;
  unsigned int pending;
  unsigned int RESERVED_A;
  unsigned int clear;
  unsigned int mp_status;
  unsigned int broadcast;
  unsigned int RESERVED_B[10];
  unsigned int mask[16];
  unsigned int force[16];
  unsigned int ext_ack[16];
} irqmp_t;

/**
 * Address of the primary UART.
 */
extern volatile irqmp_t * const IRQMP;

/**
 * Macro to define the address of the interrupt controller.
 */
#define IRQMP_AT(addr) \
  volatile irqmp_t * const IRQMP = (volatile irqmp_t*)(addr)

#endif
