
#ifndef _RVEX_UART_DEFS_H_
#define _RVEX_UART_DEFS_H_

/**
 * Control register layout for the r-VEX UART.
 */
typedef struct {
  char data;
  char dummy1[3];
  char stat;
  char dummy2[3];
  char ctrl;
} rvex_uart_regs_t;

/**
 * stat read masks.
 */
#define RVU_ROV     0x20
#define RVU_CTI     0x10
#define RVU_RXDR    0x08
#define RVU_TOV     0x04
#define RVU_TXDR    0x02
#define RVU_TXDE    0x01

/**
 * stat write masks.
 */
#define RVU_ROVC    0x20
#define RVU_CTIC    0x10
#define RVU_TOVC    0x04

/**
 * ctrl read/write bit indices.
 */
#define RVU_RXTL    0xC0
#define RVU_ROVE    0x20
#define RVU_CTIE    0x10
#define RVU_RXDRE   0x08
#define RVU_TOVE    0x04
#define RVU_TXDRE   0x02
#define RVU_TXDEE   0x01

/**
 * RX trigger levels.
 */
#define RVU_RXTL_1  0x00
#define RVU_RXTL_4  0x40
#define RVU_RXTL_8  0x80
#define RVU_RXTL_14 0xC0

#endif
