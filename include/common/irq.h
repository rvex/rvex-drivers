
#ifndef _IRQ_H_
#define _IRQ_H_

/**
 * Registers the specified interrupt handler function for the specified IRQ.
 * Only one handler can be registered at a time. data is passed to the handler.
 */
void irq_register(
  int irq,
  void (*handler)(unsigned long data),
  unsigned long data
);

/**
 * Enables or masks an interrupt.
 */
void irq_enable(int irq, int enable);

/**
 * Returns whether the specified interrupt is pending.
 */
int irq_ispending(int irq);

/**
 * Clears a pending interrupt.
 */
void irq_clear(int irq);

/**
 * Forces the specified interrupt on the specified context.
 */
void irq_force(int irq, int context);

#endif
