
.SUFFIXES:

INCLUDE_FLAGS = -I$(SRCDIR)/../../config $(patsubst %,-I$(SRCDIR)/../../include/%,$(INCLUDE_DIRS))

CSRCS += $(shell cd $(SRCDIR) && find -maxdepth 2 -name '*.c' -and -not -name '*.x.c')
SSRCS += $(shell cd $(SRCDIR) && find -maxdepth 2 -name '*.S' -and -not -name '*.x.S')
SRCS += $(CSRCS) $(SSRCS)
OBJS += $(CSRCS:%.c=%.o) $(SSRCS:%.S=%.o)
DEPS += $(CSRCS:%.c=%.d) $(SSRCS:%.S=%.d)

PREFIX ?= $(shell pwd)/install

VPATH = $(SRCDIR)

.PHONY: all
all: $(LIBNAME).a

ifdef ALL_OR_NOTHING

# If ALL_OR_NOTHING is defined, all C files are compiled into a single object.
# This causes the linker to include everything in the library if any symbol is
# required.

$(LIBNAME).a: $(LIBNAME).o
	rvex-ar rcs $@ $^

$(LIBNAME).o: $(OBJS)
	rvex-ld -r -T/dev/null $^ -o $@

else

# If ALL_OR_NOTHING is not defined, C files will be linked only if they define
# symbols that are required for the link.

$(LIBNAME).a: $(OBJS)
	rvex-ar rcs $@ $^

endif

%.o: %.c
	mkdir -p $(@D)
#	# This right here is fucking stupid, but for some reason enabling
#	# optimizations generates inane use-of-uninitialized warnings for
#	# rvex_sysreturn_t structs. So we compile once for error checking, and
#	# then again with optimizations and all warnings suppressed...
	rvex-gcc $(INCLUDE_FLAGS) -Wall -Werror $(CFLAGS) -c $< -o /dev/null
	rvex-gcc $(INCLUDE_FLAGS) -w -O2 -MMD $(CFLAGS) -c $< -o $@

%.o: %.S
	mkdir -p $(@D)
	rvex-gcc $(INCLUDE_FLAGS) -MMD $(CFLAGS) -c $< -o $@

-include $(DEPS)

.PHONY: clean-lib
clean-lib:
	rm -f $(LIBNAME).a $(LIBNAME).o $(OBJS) $(DEPS)

.PHONY: clean
clean: clean-lib

.PHONY: install-lib
install-lib: $(LIBNAME).a
	
	# Install library.
	mkdir -p $(PREFIX)/rvex-elf32/lib
	cp -au -t $(PREFIX)/rvex-elf32/lib $^

.PHONY: install
install: install-lib

