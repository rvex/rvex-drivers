
#include <sys/rootfs.h>

/**
 * Closes a file.
 */
int sys_rootfs_close(rootfs_object_ref_t *ref) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (!ref->ob) return EBADF;
  rootfs_object_t *ob = ref->ob;
  
  // Check permissions.
  if (ref->flags & ROOTFS_O_DIRECTORY) return EBADF;
  
  // Handle forwarding to driver.
  if (ob->mount) {
    if (ob->mount->close) {
      int res = ob->mount->close(ob->mount_ref, ref->drv_ref);
      if (res) return res;
    }
  }
  
  // Forward to sys_rootfs_close_int().
  sys_rootfs_close_int(ref);
  return 0;
}

