
#include <sys/rootfs.h>

/**
 * Reads the next entry from an open directory. entry must point to a
 * caller-allocated buffer for the directory entry. result is set to that
 * buffer after populating it, or to NULL if the end of the directory has been
 * reached. Returns errno or 0 on success/EOF.
 */
int sys_rootfs_readdir_r(rootfs_object_ref_t *ref, sys_dirent_t *entry, sys_dirent_t **result) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (!ref->ob) return EBADF;
  if (!entry) return EINVAL;
  if (!result) return EINVAL;
  rootfs_object_t *ob = ref->ob;
  *result = NULL;
  
  // Check permissions.
  if (!(ref->flags & ROOTFS_O_DIRECTORY)) return EBADF;
  
  // Handle forwarding to driver.
  if (ob->mount) {
    if (ob->mount->readdir_r) {
      memset(entry, 0, sizeof(sys_dirent_t));
      return ob->mount->readdir_r(ob->mount_ref, ref->drv_ref, entry, result);
    } else {
      return ENOSYS;
    }
  }
  
  const rootfs_object_t *eob = ob;
  
  // If offs is negative, we have yet to list the special . and .. directories.
  if (ref->offs < 0) {
    
    // Select the directory object itself.
    eob = ob;
    
    if (ref->offs == -2) {
      
      // . entry. Write the filename.
      strcpy(entry->d_name, ".");
      
    } else {
      
      // .. entry. Select the parent directory instead, if any.
      if (eob->parent) {
        eob = eob->parent;
      }
      
      // Write the filename.
      strcpy(entry->d_name, "..");
      
    }
    
    // Move to the next entry.
    ref->offs++;
    
  } else {
    
    // Look for the next non-deleted entry.
    const rootfs_dirent_t *ent;
    while (1) {
      
      // Check for EOF.
      if (ref->offs + sizeof(rootfs_dirent_t) > ob->size) return 0;
      
      // Check alignment.
      if ((long)(ob->contents + ref->offs) & 3) return EIO;
      
      // Get the pointer to the next directory entry.
      ent = (rootfs_dirent_t*)(ob->contents + ref->offs);
      eob = ent->ob;
      
      // Move to the next entry.
      ref->offs += sizeof(rootfs_dirent_t);
      
      // If this is not a deleted entry, break.
      if (eob) break;
    }
    
    // Copy the filename.
    strncpy(entry->d_name, ent->name, ROOTFS_NAME_LEN);
    entry->d_name[ROOTFS_NAME_LEN] = 0;
    
  }
  
  // Set d_ino to the object address.
  entry->d_ino = (long)eob;
  
  // Set the size.
  entry->d_size = eob->size;
  
  // Set the mode.
  entry->d_mode = eob->mode;
  
  // Set the link count.
  entry->d_nlinks = eob->links;
  
  // Set the type.
  entry->d_type = sys_rootfs_getftype(eob);
  
  // Success.
  *result = entry;
  return 0;
}

