
#include <sys/rootfs.h>
#include <sys/ioctl.h>

/**
 * ioctl for an open file.
 */
rootfs_ioctl_ret_t sys_rootfs_ioctl(rootfs_object_ref_t *ref, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!ref) { r.err_no = EBADF; return r; }
  if (!ref->ob) { r.err_no = EBADF; return r; }
  if (ref->flags & ROOTFS_O_DIRECTORY) { r.err_no = EBADF; return r; }
  
  // Handle forwarding to driver.
  if (ref->ob->mount) {
    if (ref->ob->mount->ioctl) {
      return ref->ob->mount->ioctl(ref->ob->mount_ref, ref->drv_ref, request, param);
    } else {
      r.err_no = ENOSYS;
      return r;
    }
  }
  
  // Ignore sync requests, we don't do buffering.
  if (request == IO_SYNC) return r;
  
  // rootfs files have no ioctl commands.
  r.err_no = EINVAL;
  return r;
}

