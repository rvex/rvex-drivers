
#include <sys/rootfs.h>

/**
 * Frees the dynamic memory consumed by an object if all references to it are
 * gone.
 */
void sys_rootfs_free_ob(rootfs_object_t *ob) {
  
  // Don't delete if this is a mount point.
  if (ob->mount) return;
  
  // Don't delete if this file is currently open.
  if (ob->anon_links) return;
  
  // Don't delete if there are at least one named link for a file or at least
  // two named links for a directory.
  if (ob->attrib & ROOTFS_ATTR_DIR) {
    
    // Don't delete if there are any named links to this directory *in addition
    // to the . entry*.
    if (ob->links > 1) return;
    
    // Decrement the link count for the parent directory.
    if (ob->parent && ob->parent->links) ob->parent->links--;
    
  } else {
    
    // Don't delete if there are any named links to this file.
    if (ob->links) return;
    
  }
  
  //printf("*** deleting file 0x%016X ***\n", ob);
  
  // Free contents.
  if (!(ob->attrib & ROOTFS_ATTR_CONTENTS_STATIC)) {
    free(ob->contents);
    ob->size = 0;
    ob->capacity = 0;
    ob->contents = NULL;
  }
  
  // Free the object itself.
  if (!(ob->attrib & ROOTFS_ATTR_OBJECT_STATIC)) {
    free(ob);
  }
  
}

