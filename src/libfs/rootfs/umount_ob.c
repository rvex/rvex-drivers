
#include <sys/rootfs.h>

/**
 * Unmounts the specified mount point using an object reference. force allows
 * the umount to be forced; this should only be done by exit if some kind of
 * messed up state prevents this from being done cleanly.
 */
int sys_rootfs_umount_ob(rootfs_object_t *ob, int force) {
  
  // Make sure the file is actually mounted.
  if (!ob->mount) return EINVAL;
  
  // Make sure the mount is not busy.
  if (ob->anon_links && !force) return EBUSY;
  
  // Forward to the driver if it has a umount.
  if (ob->mount->umount) {
    int ret = ob->mount->umount(ob->mount_ref);
    if (ret) return ret;
  }
  
  // Mark the file as unmounted.
  ob->mount = NULL;
  ob->mount_ref = NULL;
  
  // Success.
  return 0;
}

