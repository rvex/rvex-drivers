
#include <sys/rootfs.h>

/**
 * Opens a directory. Returns errno or 0 on success.
 */
int sys_rootfs_opendir(rootfs_object_ref_t *ref, char *path) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (ref->ob) return EBADF;
  if (!path) return EINVAL;
  
  // Look up the object.
  char *remnant = path;
  rootfs_object_t *ob = NULL;
  int ret = sys_rootfs_resolve(&remnant, &ob, NULL, NULL, NULL);
  if (ret) return ret;
  
  // If the object is not a directory, fail.
  if (!(ob->attrib & ROOTFS_ATTR_DIR)) return ENOTDIR;
  
  // Check that we can list this directory.
  if (!(ob->mode & ROOTFS_MODE_READ)) return EPERM;
  
  // Handle forwarding to driver.
  if (ob->mount) {
    
    // Return ENOSYS if the driver has no opendir.
    if (!ob->mount->opendir) return ENOSYS;
    
    // Call the driver opendir().
    rootfs_opendir_ret_t ro = ob->mount->opendir(ob->mount_ref, remnant);
    if (ro.err_no) return ro.err_no;
    
    // Save the driver reference.
    ref->drv_ref = ro.drv_ref;
    
  } else {
    
    // Can only open a file for reading if the file is not open for writing.
    if (ob->anon_links < 0) return EBUSY;
    
    // Increment the read reference counter.
    ob->anon_links++;
    
    // Set initial offset to -2 to indicate that we still need to output the .
    // and .. entries.
    ref->offs = -2;
    
  }
  
  // Populate the file reference.
  ref->ob = ob;
  ref->flags = ROOTFS_O_DIRECTORY;
  
  // Success.
  return 0;
}

