
#include <sys/rootfs.h>

/**
 * Collapses separators to single slashes, and ensures that the path starts
 * with a '/'. Returns 0 or the appropriate errno.
 */
int sys_rootfs_sanitize_path(char *path) {
  
  // Paths must start with a slash. We can't grow the buffer, so if there is no
  // slash yet, we can only return an error.
  if (*path != '/') return EINVAL;
  
  // Collapse/replace separators to single slashes.
  const char *a = path;
  char *b = path;
  int sep = 0;
  while (*a) {
    if (*a == '/' || *a == '\\') {
      if (!sep) {
        sep = 1;
        *b++ = '/';
      }
      a++;
    } else {
      sep = 0;
      *b++ = *a++;
    }
  }
  if (sep) {
    b--;
  }
  *b = 0;
  
  // Special case: if we're referring to the root directory, we DO want a
  // "trailing slash".
  if (!*path) {
    path[0] = '/';
    path[1] = 0;
  }
  
  return 0;
}

