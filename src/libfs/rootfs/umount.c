
#include <sys/rootfs.h>

/**
 * Unmounts the specified mount point. Returns errno or 0 on success. The
 * supplied path buffer must be considered to be invalid after the call.
 */
int sys_rootfs_umount(char *path) {
  
  // Check arguments.
  if (!path) return EINVAL;
  
  // Look up the file.
  rootfs_object_t *ob = NULL;
  int ret = sys_rootfs_resolve(&path, &ob, NULL, NULL, NULL);
  if (ret) return ret;
  
  // Chain into umount_ob.
  return sys_rootfs_umount_ob(ob, 0);
}

