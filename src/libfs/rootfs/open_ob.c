
#include <sys/rootfs.h>

/**
 * Opens an existing file by object. When opening a file within a directory
 * mount, path specifies the portion of the path within the mount; otherwise
 * path must be NULL. Returns errno or 0 on success. flags must already have
 * been incremented by 1 to get read/write access as a bitmask.
 */
int sys_rootfs_open_ob(rootfs_object_ref_t *ref, rootfs_object_t *ob, const char *path, int flags, int mode) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (ref->ob) return EBADF;
  if (!ob) return EINVAL;
  
  // Check the reference counter.
  if (flags & ROOTFS_O_EXCLUSIVE) {
    
    // Can only open a file with a lock if the file is not open right now.
    if (ob->anon_links) return EBUSY;
    
  } else {
    
    // Can only open a file if it is not locked.
    if (ob->anon_links < 0) return EBUSY;
    
  }
  
  // Check permissions.
  if (path) {
    
    // Application wants to access a file in the mounted directory.
    if (!(ob->mode & ROOTFS_MODE_EXEC)) return EPERM;
    
  } else {
    
    // Application wants to access this file.
    if ((flags & ROOTFS_O_READ) && !(ob->mode & ROOTFS_MODE_READ)) return EPERM;
    if ((flags & ROOTFS_O_WRITE) && !(ob->mode & ROOTFS_MODE_WRITE)) return EPERM;
    
  }
  
  // Handle forwarding to driver.
  if (ob->mount) {
    
    // Forward to the driver's open call. If it doesn't have one, just set
    // drv_ref to zero and continue.
    if (ob->mount->open) {
      
      // Call the driver open().
      rootfs_open_ret_t ro = ob->mount->open(ob->mount_ref, path, flags, mode);
      if (ro.err_no) return ro.err_no;
      
      // Save the driver reference.
      ref->drv_ref = ro.drv_ref;
      
    } else {
      
      // Driver doesn't need to keep its own reference.
      ref->drv_ref = NULL;
      
    }
    
  } else {
    
    // Fail if the specified file is not a directory and this is required.
    if ((flags & O_DIRECTORY) && !(ob->attrib & ROOTFS_ATTR_DIR)) return ENOTDIR;
    
    // Truncate if desired.
    if ((flags & ROOTFS_O_WRITE) && (flags & O_TRUNC)) {
      ob->size = 0;
    }
    
    // Initialize the file offset.
    ref->offs = 0;
    
  }
  
  // Populate the file reference.
  ref->ob = ob;
  ref->flags = flags & (ROOTFS_O_READ | ROOTFS_O_WRITE | ROOTFS_O_APPEND | ROOTFS_O_NONBLOCK);
  
  // Update the reference counter.
  if (flags & ROOTFS_O_EXCLUSIVE) {
    
    // Mark that we have exclusive access to this file.
    ob->anon_links = -1;
    
  } else {
    
    // Increment the read reference counter.
    ob->anon_links++;
    
  }
  
  // Success.
  return 0;
}

