
#include <sys/rootfs.h>

/**
 * Adds a directory entry to ob at the specified path. The supplied path is
 * expected to already be sanitized, and is expected to not already exist.
 * Returns errno or 0 on success. *path is undefined after the call.
 */
int sys_rootfs_link_ob(rootfs_object_t *ob, char *path) {
  
  // Check arguments.
  if (!ob) return EINVAL;
  if (!path) return EINVAL;
  
  // Split the path into the directory and the filename.
  char *fname = strrchr(path, '/');
  if (!fname) return EINVAL;
  *fname++ = 0;
  char slash[2] = "/";
  if (!*path) path = slash;
  
  // Make sure that we have a valid filename.
  if (!*fname) return EINVAL;
  if (strlen(fname) > ROOTFS_NAME_LEN) return ENAMETOOLONG;
  
  // Create a temporary file reference for the directory on the stack.
  rootfs_object_ref_t dir = {};
  
  // Open the directory.
  int ret = sys_rootfs_opendir(&dir, path);
  if (ret) return ret;
  
  // Check that we can write to this directory.
  if (!(dir.ob->mode & ROOTFS_MODE_WRITE)) {
    sys_rootfs_closedir(&dir);
    return EPERM;
  }
  
  // If the directory is a mount point, we have to fail; links don't work
  // cross-mount.
  if (dir.ob->mount) {
    sys_rootfs_closedir(&dir);
    return EXDEV;
  }
  
  // Pull some hackery to get permissions to read/write to the directory.
  // Especially the latter would be annoying to do manually here, because it
  // may need to grow the size and capacity of the directory.
  dir.flags = ROOTFS_O_READ | ROOTFS_O_WRITE;
  dir.offs = 0;
  
  // Find the directory offset for the first empty entry, or the offset
  // immediately following the last entry.
  off_t offs = 0;
  rootfs_dirent_t ent;
  #pragma unroll(0)
  while (1) {
    
    // Read the next entry.
    rootfs_read_ret_t rr = sys_rootfs_read(&dir, &ent, sizeof(rootfs_dirent_t));
    
    // Check for errors.
    if (rr.err_no) {
      sys_rootfs_close(&dir);
      return rr.err_no;
    }
    
    // If we couldn't read everything, we're at the end of the directory
    // without finding an empty entry. In that case we just append a new
    // entry at the end when we do the write.
    if (rr.amount < sizeof(rootfs_dirent_t)) {
      break;
    }
    
    // See if this directory entry is empty.
    if (!ent.ob) {
      break;
    }
    
    // Set the offset to the next entry.
    offs += sizeof(rootfs_dirent_t);
    
  }
  
  // Seek to the offset we just found.
  dir.offs = offs;
  
  // Populate the new directory entry.
  ent.ob = ob;
  strncpy(ent.name, fname, ROOTFS_NAME_LEN);
  
  // Write the new directory entry.
  rootfs_write_ret_t wr = sys_rootfs_write(&dir, &ent, sizeof(rootfs_dirent_t));
  if (wr.err_no) {
    sys_rootfs_close(&dir);
    return wr.err_no;
  }
  if (wr.amount < sizeof(rootfs_dirent_t)) {
    sys_rootfs_close(&dir);
    return ENOSPC;
  }
  
  // Increment the link counter and set the parent.
  ob->links++;
  ob->parent = dir.ob;
  
  // If this is a directory, also add a link to the parent for the .. entry.
  if (ob->attrib & ROOTFS_ATTR_DIR) {
    ob->parent->links++;
  }
  
  // Close the directory.
  sys_rootfs_close(&dir);
  
  // Success.
  return 0;
}

