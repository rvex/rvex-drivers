
#include <sys/rootfs.h>

/**
 * Reads from an open file.
 */
rootfs_read_ret_t sys_rootfs_read(rootfs_object_ref_t *ref, void *buf, size_t amount) {
  rootfs_read_ret_t r = {};
  
  // Check arguments.
  if (!ref) { r.err_no = EBADF; return r; }
  if (!ref->ob) { r.err_no = EBADF; return r; }
  if (!buf) { r.err_no = EINVAL; return r; }
  
  // Check permissions.
  if (ref->flags & ROOTFS_O_DIRECTORY) { r.err_no = EBADF; return r; }
  if (!(ref->flags & ROOTFS_O_READ)) { r.err_no = EBADF; return r; }
  
  // Handle forwarding to driver.
  if (ref->ob->mount) {
    if (ref->ob->mount->read) {
      #pragma unroll(0)
      do {
        r = ref->ob->mount->read(ref->ob->mount_ref, ref->drv_ref, buf, amount);
      } while ((r.err_no == EWOULDBLOCK) && !(ref->flags & O_NONBLOCK));
      return r;
    } else {
      r.err_no = ENOSYS;
      return r;
    }
  }
  
  // Figure out how much we can read before EOF.
  if (ref->offs >= ref->ob->size) {
    r.amount = 0;
  } else if (ref->offs + amount > ref->ob->size) {
    r.amount = ref->ob->size - ref->offs;
  } else {
    r.amount = amount;
  }
  if (!r.amount) {
    return r;
  }
  
  // Copy from the file contents.
  memcpy(buf, ref->ob->contents + ref->offs, r.amount);
  
  // Update the file pointer.
  ref->offs += r.amount;
  
  return r;
}

