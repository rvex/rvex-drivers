
#include <sys/rootfs.h>

/**
 * Stats an open file.
 */
int sys_rootfs_stat(rootfs_object_ref_t *ref, struct stat *sb) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (!ref->ob) return EBADF;
  if (!sb) return EINVAL;
  rootfs_object_t *ob = ref->ob;
  
  // Clear the stat record.
  memset(sb, 0, sizeof(struct stat));
  
  // Set some defaults that should work regardless of mount.
  sb->st_dev  = (dev_t)ob->mount;
  sb->st_ino  = (ino_t)ob;
  sb->st_mode = ((sys_rootfs_getftype(ob) << 12) & S_IFMT)
              | (ob->mode & 07777);
  
  // Handle forwarding to driver.
  if (ref->ob->mount && ref->ob->mount->stat) {
    
    // Forward.
    int res = ref->ob->mount->stat(ref->ob->mount_ref, ref->drv_ref, sb);
    if (res) return res;
    
    // If the driver did not set the block size, do it for it.
    if (!sb->st_blksize) {
      sb->st_blksize = 512;
      sb->st_blocks  = (sb->st_size + 511) >> 9;
    }
    
  } else {
    
    // Set link count and filesize.
    sb->st_nlink   = ob->links;
    sb->st_size    = ob->size;
    sb->st_blksize = 512;
    sb->st_blocks  = (ob->capacity + 511) >> 9;
    
    // NOTE: the following entries exist, but are not used for rootfs files.
    //sb->st_uid     = 0;
    //sb->st_gid     = 0;
    //sb->st_rdev    = 0;
    //sb->st_atime   = 0;
    //sb->st_mtime   = 0;
    //sb->st_ctime   = 0;
    
  }
  
  // Success.
  return 0;
}

