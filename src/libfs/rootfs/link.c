
#include <sys/rootfs.h>

/**
 * Adds a directory entry for path1 to path2. Returns errno or 0 on succeess.
 * The paths are undefined after the call.
 */
int sys_rootfs_link(char *path1, char *path2) {
  
  // Check arguments.
  if (!path1) return EINVAL;
  if (!path2) return EINVAL;
  
  // Resolve the path that's supposed to already exist.
  char *path1_remnant = path1;
  rootfs_object_t *path1_ob = NULL;
  int res = sys_rootfs_resolve(&path1_remnant, &path1_ob, NULL, NULL, NULL);
  if (res) return res;
  
  // Resolve the path that's supposed to not exist yet.
  char *path2_remnant = path2;
  rootfs_object_t *path2_ob = NULL;
  res = sys_rootfs_resolve(&path2_remnant, &path2_ob, NULL, NULL, NULL);
  if (res && (res != ENOENT)) return res;
  
  // If both paths exist and refer to the same mount point, forward to their
  // driver.
  if (path1_remnant) {
    
    // If path1 refers to a mount but path2 doesn't, this is a cross-dev link.
    if (res) return EXDEV;
    if (!path2_remnant) return EXDEV;
    
    // If path1 and path2 both exist but refer to different mount points, this
    // is a cross-dev link.
    if (path1_ob != path2_ob) return EXDEV;
    
    // If the mount driver does not have a link call, return ENOSYS.
    if (!path1_ob->mount->link) return ENOSYS;
    
    // Forward to the driver.
    return path1_ob->mount->link(path1_ob->mount_ref, path1_remnant, path2_remnant);
    
  }
  
  // If path2 refers to a mount but path1 doesn't, this is a cross-dev link.
  if (!res && path2_remnant) return EXDEV;
  
  // With mounts now out of the way, we can make sure that path2 doesn't exist.
  if (!res) return res;
  
  // Linking directories is not allowed through the syscall, because it can
  // create loops.
  if (path1_ob->attrib & ROOTFS_ATTR_DIR) return EPERM;
  
  // We now satisfy sys_rootfs_link_ob()'s preconditions, so forward the call
  // there.
  return sys_rootfs_link_ob(path1_ob, path2);
}

