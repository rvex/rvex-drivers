
#include <sys/rootfs.h>

/**
 * Makes a directory. Returns errno or 0 on success.
 */
int sys_rootfs_mkdir(char *path, int mode) {
  
  // Check arguments.
  if (!path) return EINVAL;
  
  // Look up the file.
  char *remnant = path;
  rootfs_object_t *ob = NULL;
  int ret = sys_rootfs_resolve(&remnant, &ob, NULL, NULL, NULL);
  
  // If ret is zero, the file was found, or we encountered a directory mount
  // point.
  if (ret == 0) {
    
    // If we do not have a path remnant, i.e. the file actually exists (and is
    // not just a forward into the mounted filesystem), we must fail.
    if (!remnant) return EEXIST;
    
    // Handle forwarding to driver.
    if (!ob->mount) return EINVAL;
    if (!ob->mount->mkdir) return ENOSYS;
    return ob->mount->mkdir(ob->mount_ref, remnant, mode);
    
  }
  
  // Allocate a new file object.
  ob = (rootfs_object_t*)calloc(1, sizeof(rootfs_object_t));
  if (!ob) return ENOSPC;
  
  // Set the directory flag.
  ob->attrib = ROOTFS_ATTR_DIR;
  
  // Set the file mode.
  ob->mode = mode;
  
  // Call link to add a directory entry for the file.
  ret = sys_rootfs_link_ob(ob, path);
  if (ret) {
    free(ob);
    return ret;
  }
  
  // Add a fake link for the . entry.
  ob->links++;
  
  // The directory has been created.
  return 0;
}

