
#include <sys/rootfs.h>
#include <unistd.h>

/**
 * Writes to an open file.
 */
rootfs_write_ret_t sys_rootfs_write(rootfs_object_ref_t *ref, const void *buf, size_t amount) {
  rootfs_write_ret_t r = {};
  
  // Check arguments.
  if (!ref) { r.err_no = EBADF; return r; }
  if (!ref->ob) { r.err_no = EBADF; return r; }
  if (!buf) { r.err_no = EINVAL; return r; }
  
  // Check permissions.
  if (ref->flags & ROOTFS_O_DIRECTORY) { r.err_no = EBADF; return r; }
  if (!(ref->flags & ROOTFS_O_WRITE)) { r.err_no = EBADF; return r; }
  
  // Seek to end if we're appending.
  if (ref->flags & ROOTFS_O_APPEND) {
    rootfs_lseek_ret_t sr = sys_rootfs_lseek(ref, 0, SEEK_END);
    if (sr.err_no) { r.err_no = sr.err_no; return r; }
  }
  
  // Handle forwarding to driver.
  if (ref->ob->mount) {
    if (ref->ob->mount->write) {
      #pragma unroll(0)
      do {
        r = ref->ob->mount->write(ref->ob->mount_ref, ref->drv_ref, buf, amount);
      } while ((r.err_no == EWOULDBLOCK) && !(ref->flags & O_NONBLOCK));
      return r;
    } else {
      r.err_no = ENOSYS;
      return r;
    }
  }
  
  // Figure out how much we can write without increasing capacity.
  if (ref->offs >= ref->ob->capacity) {
    r.amount = 0;
  } else if (ref->offs + amount > ref->ob->capacity) {
    r.amount = ref->ob->capacity - ref->offs;
  } else {
    r.amount = amount;
  }
  
  // Write what we can without increasing capacity.
  if (r.amount) {
    
    // Write to the file.
    memcpy(ref->ob->contents + ref->offs, buf, r.amount);
    
    // Update the file pointer.
    ref->offs += r.amount;
    
    // Update the file size if necessary.
    if (ref->offs > ref->ob->size) {
      ref->ob->size = ref->offs;
    }
    
  }
  
  // If we've written all that the caller wanted, return here.
  if (r.amount == amount) {
    return r;
  }
  
  // If we can't increase capacity because this is a statically allocated file,
  // return here.
  if (ref->ob->attrib & ROOTFS_ATTR_CONTENTS_STATIC) {
    return r;
  }
  
  // We need to increase capacity now.
  size_t new_capacity = ref->ob->size + (amount - r.amount);
  void *new_contents = realloc(ref->ob->contents, new_capacity);
  
  // If we failed to realloc, return here.
  if (new_contents == NULL) {
    return r;
  }
  
  // Commit the new contents pointer.
  ref->ob->contents = new_contents;
  ref->ob->capacity = new_capacity;
  
  // Write to the file.
  memcpy(new_contents + ref->offs, buf + r.amount, amount - r.amount);
  
  // Update the file pointer.
  ref->offs += amount - r.amount;
  
  // Update the file size if necessary.
  if (ref->offs > ref->ob->size) {
    ref->ob->size = ref->offs;
  }
  
  // We now wrote everything the user requested.
  r.amount = amount;
  
  return r;
}

