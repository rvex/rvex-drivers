
#include <sys/rootfs.h>

/**
 * Closes a directory. Returns errno or 0 on success.
 */
int sys_rootfs_closedir(rootfs_object_ref_t *ref) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (!ref->ob) return EBADF;
  rootfs_object_t *ob = ref->ob;
  
  // Check permissions.
  if (!(ref->flags & ROOTFS_O_DIRECTORY)) return EBADF;
  
  // Handle forwarding to driver.
  if (ob->mount) {
    if (ob->mount->closedir) {
      int res = ob->mount->closedir(ob->mount_ref, ref->drv_ref);
      if (res) return res;
    }
  }
  
  // Forward to sys_rootfs_close_int().
  sys_rootfs_close_int(ref);
  return 0;
}

