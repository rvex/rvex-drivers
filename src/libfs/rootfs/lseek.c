
#include <sys/rootfs.h>
#include <unistd.h>

/**
 * Seeks within a file.
 */
rootfs_lseek_ret_t sys_rootfs_lseek(rootfs_object_ref_t *ref, off_t offset, int whence) {
  rootfs_lseek_ret_t r = {};
  
  // Check arguments.
  if (!ref) { r.err_no = EBADF; return r; }
  if (!ref->ob) { r.err_no = EBADF; return r; }
  
  // Check permissions.
  if (ref->flags & ROOTFS_O_DIRECTORY) { r.err_no = EBADF; return r; }
  if (!(ref->flags & (ROOTFS_O_READ | ROOTFS_O_WRITE))) { r.err_no = EBADF; return r; }
  
  // Handle forwarding to driver.
  if (ref->ob->mount) {
    if (ref->ob->mount->lseek) {
      return ref->ob->mount->lseek(ref->ob->mount_ref, ref->drv_ref, offset, whence);
    } else {
      r.err_no = ENOSYS;
      return r;
    }
  }
  
  // Figure out the seek reference.
  off_t reference;
  switch (whence) {
    case SEEK_SET: reference = 0; break;
    case SEEK_CUR: reference = ref->offs; break;
    case SEEK_END: reference = ref->ob->size; break;
    default: { r.err_no = EINVAL; return r; }
  }
  
  // Check range.
  if ((reference + offset < 0) || (reference + offset > ref->ob->size)) {
    r.err_no = EINVAL;
    return r;
  }
  
  // Perform the seek and return success.
  ref->offs = reference + offset;
  r.pos = reference + offset;
  return r;
}

