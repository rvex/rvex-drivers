
#include <sys/rootfs.h>

/**
 * Renames/moves a file. Returns errno or 0 on success.
 */
int sys_rootfs_rename(char *from, char *to) {
  
  // Check arguments.
  if (!from) return EINVAL;
  if (!to) return EINVAL;
  
  // Resolve the path that's supposed to already exist.
  char *from_remnant = from;
  rootfs_object_t *from_ob = NULL;
  rootfs_object_t *from_dir = NULL;
  rootfs_dirent_t *from_dirent = NULL;
  int res = sys_rootfs_resolve(&from_remnant, &from_ob, &from_dirent, &from_dir, NULL);
  if (res) return res;
  
  // If there is no directory entry, the application is trying to rename /.
  if (!from_dirent) return EINVAL;
  
  // Check that we have write permissions in the source directory.
  if (!(from_dir->mode & ROOTFS_MODE_WRITE)) return EPERM;
  
  // Save the dirent by offset, because the link that follows might realloc()
  // if the source and destination directory are the same.
  off_t from_dirent_offs = ((void*)from_dirent) - from_dir->contents;
  
  // Resolve the path that's supposed to not exist yet, excluding the object
  // we're renaming.
  char *to_remnant = to;
  rootfs_object_t *to_ob = NULL;
  res = sys_rootfs_resolve(&to_remnant, &to_ob, NULL, NULL, from_ob);
  if (res && (res != ENOENT)) return res;
  
  // If both paths exist and refer to the same mount point, forward to their
  // driver.
  if (from_ob->mount) {
    
    // If path1 refers to a mount but path2 doesn't, this is a cross-dev
    // rename.
    if (res) return EXDEV;
    
    // If path1 and path2 both exist but refer to different mount points, this
    // is a cross-dev rename.
    if (from_ob != to_ob) return EXDEV;
    
    // If the mount driver does not have a rename call, return ENOSYS.
    if (!from_ob->mount->rename) return ENOSYS;
    
    // Forward to the driver.
    return from_ob->mount->rename(from_ob->mount_ref, from_remnant, to_remnant);
    
  }
  
  // If path2 refers to a mount but path1 doesn't, this is a cross-dev rename.
  if (!res && to_ob->mount) return EXDEV;
  
  // Now we know both paths are not within a mount. That means that the dest
  // path must not exist, unless it is the exact same file as the source, in
  // which case rename must succeed with no-op.
  if (!res) {
    
    // Special case: same file.
    if (from_ob == to_ob) return 0;
    
    // Fail with EEXIST.
    return EEXIST;
    
  }
  
  // Store a pointer to the previous parent, so we can remove the .. link for
  // directories after we link.
  rootfs_object_t *parent_ob = from_ob->parent;
  
  // Perform a link for the new path.
  res = sys_rootfs_link_ob(from_ob, to);
  if (res) return res;
  
  // Restore the dirent after the link.
  from_dirent = (rootfs_dirent_t*)(from_dir->contents + from_dirent_offs);
  
  // Unlink the original path if the link succeeded.
  from_dirent->ob = NULL;
  if (from_ob->links > 0) from_ob->links--;
  
  // Also remove a link from the parent for the .. entry if this is a
  // directory.
  if (parent_ob && (from_ob->attrib & ROOTFS_ATTR_DIR)) parent_ob->links--;
  
  return 0;
}

