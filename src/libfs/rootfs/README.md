
RootFS supervisor functions
===========================

This folder contains the core files for libfs, known as the root filesystem
(rootfs).

The `_sys.c` file is special; it contains the syscall handlers that tie into
newlib's libgloss trap handler code. These handlers are a relatively thin shell;
the biggest thing they do is map from file descriptor numbers to
`rootfs_object_ref_t` structures. This file, along with `rootfs.h`, contains
some magic to ensure that it's included in the link, even though the linker
would otherwise not do this since it doesn't understand that the syscall
handlers are necessary (more info at the top of `_sys.c`).

The remaining files implement `sys_rootfs_*` functions, implementing some (part
of) a syscall. These functions have declarations in `sys/rootfs.h`, allowing
drivers to call these functions directly, thereby bypassing some syscall
overhead. This also means that they don't interfere with the application's file
descriptor pool, since they can manage the `rootfs_object_ref_t` directly.

