
/**
 * This file defines the syscall handlers for libfs. Whenever libfs is used,
 * this file MUST be linked. Since the syscall handlers themselves appear to
 * the linker as not being necessary (because weak default handlers are already
 * defined by crt0), we need to use trickery to require this file. This is done
 * using the rootfs_drivers_dummy symbol. This symbol is required by any source
 * file including rootfs.h by means of a static constant definition in that
 * header file, initialized to the address of rootfs_drivers_dummy. This
 * constant is stored in the .discard section so it doesn't waste any space in
 * the binary.
 */

#include <driver-config.h>
#include <sys/rootfs.h>
#include <machine/rvex.h>
#include <machine/rvex-sys.h>
#include <sys/errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * Plug-and-play definition for filesystem driver registration. This must be
 * defined in this file along with the syscalls, otherwise the syscalls will
 * not be linked as they appear to the linker to not be needed. The syscalls,
 * in turn, require insertion of all the requisite sys_rootfs_* functions.
 */
const long rootfs_drivers_dummy __attribute__ ((section (".pnp.rootfs_drivers"))) = 0;

/**
 * File descriptor structure. This is what file descriptor numbers refer to.
 * Because of dup, multiple fd numbers can refer to one of these.
 */
typedef struct fd_struct_t {
  
  // Reference for rootfs.
  rootfs_object_ref_t ref;
  
  // Number of file descriptors that refer to this structure. Set to one by
  // open(), incremented by dup() (through fcntl), decremented by close().
  // sys_rootfs_close() is only called when fd_count is decremented to zero.
  short fd_count;
  
  // If set, the system calls should forward to opendir/readdir_r/closedir
  // instead of the usual file calls.
  short dir;
  
} fd_struct_t;

/**
 * File descriptor number to structure mapping. Capacity indicates the size
 * of the realloc-managed array.
 */
static fd_struct_t **fd_map = NULL;
static int fd_map_capacity = 0;

/**
 * Returns an unused file descriptor index.
 */
static int sys_fd_find_free(int start) {
  int i;
  
  // Look for a free file descriptor within the current capacity.
  for (i = start; i < fd_map_capacity; i++) {
    if (fd_map[i] == NULL) {
      return i;
    }
  }
  
  // Try to increase capacitory of the file descriptor storage.
  int new_capacity = fd_map_capacity + 4;
  if (new_capacity <= start) new_capacity = start + 1;
  fd_struct_t **new = (fd_struct_t**)realloc(fd_map, sizeof(fd_struct_t*) * new_capacity);
  if (new == NULL) return -1;
  
  // Initialize the newly allocated memory to zero.
  memset(new + fd_map_capacity, 0, sizeof(fd_struct_t*) * (new_capacity - fd_map_capacity));
  
  // Successfully realloc'd.
  fd_map = new;
  fd_map_capacity = new_capacity;
  
  // i should contain the old fd_map_capacity after the loop, i.e. the first
  // entry in the newly allocated memory.
  return i;
}

/**
 * Returns the file descriptor structure belonging to the given number, or null
 * if the fd is not open.
 */
static fd_struct_t *sys_fd_resolve(int fd) {
  
  // Check range.
  if ((fd < 0) || (fd >= fd_map_capacity)) return NULL;
  
  // Look the number up in the table.
  return fd_map[fd];
  
}

#ifdef ENABLE_SYS_OPEN
/**
 * Implements the open syscall.
 */
rvex_sysreturn_t sys_open(const char *fname, int flags, int mode) {
  
  // Convert flags such that we get nice bitflags for read and write;
  //   O_RDONLY = 0 -> ROOTFS_O_READ = 1
  //   O_WRONLY = 1 -> ROOTFS_O_WRITE = 2
  //   O_RDWR   = 2 -> ROOTFS_O_READ | ROOTFS_O_WRITE = 3
  // flags & 3 == 3 is illegal, so we can just increment to do this.
  if ((flags & 3) == 3) rvex_sysreturn_err(EINVAL);
  flags++;
  
  // Get a free file descriptor.
  int fd = sys_fd_find_free(0);
  if (fd < 0) rvex_sysreturn_err(ENFILE);
  
  // calloc() a new structure.
  fd_struct_t *fds = (fd_struct_t*)calloc(1, sizeof(fd_struct_t));
  if (fds == NULL) rvex_sysreturn_err(ENOMEM);
  
  // Duplicate the supplied path so rootfs can manipulate it.
  char *fname_buf = strdup(fname);
  if (fname_buf == NULL) {
    free(fds);
    rvex_sysreturn_err(ENOMEM);
  }
  
  // Forward the open to rootfs.
  int res;
  if (flags & O_DIRECTORY) {
    
    // Directory open.
    res = sys_rootfs_opendir(&(fds->ref), fname_buf);
    fds->dir = 1;
    
  } else {
    
    // Normal open.
    res = sys_rootfs_open(&(fds->ref), fname_buf, flags, mode);
    
  }
  if (res) {
    free(fname_buf);
    free(fds);
    rvex_sysreturn_err(res);
  }
  
  // Update the map.
  fds->fd_count = 1;
  fd_map[fd] = fds;
  
  // Return the file descriptor index.
  rvex_sysreturn_ok(int, fd);
}
#endif

#ifdef ENABLE_SYS_CLOSE
/**
 * Implements the close syscall.
 */
rvex_sysreturn_t sys_close(int fd) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // If this is the last reference to this file descriptor structure, forward
  // the close to rootfs. This can fail, so we need to do this before anything
  // else.
  if (fds->fd_count == 1) {
    int res;
    if (fds->dir) {
      res = sys_rootfs_closedir(&(fds->ref));
    } else {
      res = sys_rootfs_close(&(fds->ref));
    }
    if (res) rvex_sysreturn_err(res);
  }
  
  // Remove the reference.
  fds->fd_count--;
  fd_map[fd] = NULL;
  
  // If we're out of references, free the structure.
  if (!fds->fd_count) {
    free(fds);
  }
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_READ
/**
 * Implements the read syscall.
 */
rvex_sysreturn_t sys_read(int fd, void *buf, size_t cnt) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // Forward to readdir if this is a directory file descriptor.
  if (fds->dir) {
    
    // For a readdir, cnt must be exactly the size of a sys_dirent_t
    // (which is a typedef for struct dirent).
    if (cnt != sizeof(sys_dirent_t)) rvex_sysreturn_err(EINVAL);
    
    // Forward the call.
    sys_dirent_t *result;
    int res = sys_rootfs_readdir_r(&(fds->ref), (sys_dirent_t*)buf, &result);
    if (res) rvex_sysreturn_err(res);
    
    // If result is NULL, we're at EOF.
    if (result == NULL) rvex_sysreturn_ok(ssize_t, 0);
    
    // If result is nonnull, but not equal to buf, we need to memmove.
    if ((void*)buf != (void*)result) memmove(buf, result, sizeof(sys_dirent_t));
    
    // Success.
    rvex_sysreturn_ok(ssize_t, sizeof(sys_dirent_t));
  }
  
  // Normal file, forward to read.
  rootfs_read_ret_t rr = sys_rootfs_read(&(fds->ref), buf, cnt);
  if (rr.err_no) rvex_sysreturn_err(rr.err_no);
  
  // Success.
  rvex_sysreturn_ok(ssize_t, rr.amount);
}
#endif

#ifdef ENABLE_SYS_WRITE
/**
 * Implements the write syscall.
 */
rvex_sysreturn_t sys_write(int fd, const void *buf, size_t cnt) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // Cannot write to directories.
  if (fds->dir) rvex_sysreturn_err(EBADF);
  
  // Normal file, forward to write.
  rootfs_write_ret_t wr = sys_rootfs_write(&(fds->ref), buf, cnt);
  if (wr.err_no) rvex_sysreturn_err(wr.err_no);
  
  // Success.
  rvex_sysreturn_ok(ssize_t, wr.amount);
}
#endif

#ifdef ENABLE_SYS_LSEEK
/**
 * Implements the lseek syscall.
 */
rvex_sysreturn_t sys_lseek(int fd, _off_t offset, int whence) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // Forward to seekdir if this is a directory file descriptor.
  if (fds->dir) {
    
    // SEEK_SET is used by rewinddir and seekdir. SEEK_CUR with zero is used by
    // telldir.
    if (whence == SEEK_SET) {
      
      // Offset -1 is reserved for telldir, so we need to check for that here.
      if (offset < 0) rvex_sysreturn_err(EINVAL);
      
    } else if (whence == SEEK_CUR) {
      
      // Offset must be zero.
      if (offset) rvex_sysreturn_err(EINVAL);
      
      // Set offset to -1 to request the current position from seekdir without
      // changing the pointer.
      offset = -1;
      
    } else {
      
      // Invalid directory seek.
      rvex_sysreturn_err(EINVAL);
      
    }
    
    // Forward the seek.
    rootfs_seekdir_ret_t sdr = sys_rootfs_seekdir(&(fds->ref), offset);
    if (sdr.err_no) rvex_sysreturn_err(sdr.err_no);
    
    // Success.
    rvex_sysreturn_ok(_off_t, sdr.pos);
    
  } else {
    
    // Normal file, forward to lseek.
    rootfs_lseek_ret_t sr = sys_rootfs_lseek(&(fds->ref), offset, whence);
    if (sr.err_no) rvex_sysreturn_err(sr.err_no);
    
    // Success.
    rvex_sysreturn_ok(_off_t, sr.pos);
    
  }
}
#endif

#ifdef ENABLE_SYS_FSTAT
/**
 * Implements the fstat syscall.
 */
rvex_sysreturn_t sys_fstat(int fd, struct stat *pstat) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // Clear the stat record.
  memset(pstat, 0, sizeof(struct stat));
  
  // Forward to stat.
  int res = sys_rootfs_stat(&(fds->ref), pstat);
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_IOCTL
/**
 * Implements the ioctl syscall.
 */
rvex_sysreturn_t sys_ioctl(int fd, int request, void *argp) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // Forward to ioctl.
  rootfs_ioctl_ret_t ir = sys_rootfs_ioctl(&(fds->ref), request, argp);
  if (ir.err_no) rvex_sysreturn_err(ir.err_no);
  
  // Success.
  rvex_sysreturn_ok(int, ir.result);
}
#endif

#ifdef ENABLE_SYS_FCNTL
/**
 * Implements the fcntl syscall.
 */
rvex_sysreturn_t sys_fcntl(int fd, int cmd, int arg) {
  
  // Resolve the file descriptor.
  fd_struct_t *fds = sys_fd_resolve(fd);
  if (fds == NULL) rvex_sysreturn_err(EBADF);
  
  // Handle the command.
  switch (cmd) {
    
    case F_DUPFD: {
      
      // Resolve the existing file descriptor.
      fd_struct_t *fds = sys_fd_resolve(fd);
      if (fds == NULL) rvex_sysreturn_err(EBADF);
      
      // Find a free file descriptor to dup into.
      int new_fd = sys_fd_find_free(arg);
      if (new_fd < 0) rvex_sysreturn_err(ENFILE);
      
      // Increment the reference count.
      fds->fd_count++;
      
      // Update the map.
      fd_map[new_fd] = fds;
      
      // Success.
      rvex_sysreturn_ok(int, new_fd);
      
    }
    
    case F_GETFD:
      rvex_sysreturn_ok(int, 0);
      break;
    
    case F_SETFD:
      if (arg) rvex_sysreturn_err(EINVAL);
      rvex_sysreturn_ok(int, 0);
      break;
    
    case F_GETFL:
      rvex_sysreturn_ok(int, fds->ref.flags - 1);
      break;
    
    case F_SETFL:
      arg &= O_APPEND | O_NONBLOCK;
      fds->ref.flags = (fds->ref.flags & ~(O_APPEND | O_NONBLOCK))
                     | (arg & (O_APPEND | O_NONBLOCK));
      rvex_sysreturn_ok(int, 0);
      break;
    
  }
  
  // Unknown command.
  rvex_sysreturn_err(EINVAL);
}
#endif

#ifdef ENABLE_SYS_STAT
/**
 * Implements the stat syscall.
 */
rvex_sysreturn_t sys_stat(const char *fname, struct stat *pstat) {
  
  // Open the file in a temporary buffer without read or write permissions.
  char *fname_buf = strdup(fname);
  if (fname_buf == NULL) rvex_sysreturn_err(ENOMEM);
  rootfs_object_ref_t ref = {};
  int res = sys_rootfs_open(&ref, fname_buf, 0, 0);
  free(fname_buf);
  if (res) rvex_sysreturn_err(res);
  
  // Clear the stat record.
  memset(pstat, 0, sizeof(struct stat));
  
  // Forward to stat.
  res = sys_rootfs_stat(&ref, pstat);
  
  // Close the file. There's nothing we can do about it if this fails, so we
  // just ignore the result.
  sys_rootfs_close(&ref);
  
  // Handle failed stat.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_LINK
/**
 * Implements the link syscall.
 */
rvex_sysreturn_t sys_link(const char *old, const char *new) {
  
  // Check input.
  if (old == NULL) rvex_sysreturn_err(EINVAL);
  if (new == NULL) rvex_sysreturn_err(EINVAL);
  
  // Make buffers for the paths.
  char *old_buf = strdup(old);
  if (old_buf == NULL) {
    rvex_sysreturn_err(ENOMEM);
  }
  char *new_buf = strdup(new);
  if (new_buf == NULL) {
    free(old_buf);
    rvex_sysreturn_err(ENOMEM);
  }
  
  // Forward the call.
  int res = sys_rootfs_link(old_buf, new_buf);
  
  // Free the buffers.
  free(old_buf);
  free(new_buf);
  
  // Handle failure.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_UNLINK
/**
 * Implements the unlink syscall.
 */
rvex_sysreturn_t sys_unlink(const char *fname) {
  
  // Check input.
  if (fname == NULL) rvex_sysreturn_err(EINVAL);
  
  // Make a buffer for the path.
  char *fname_buf = strdup(fname);
  if (fname_buf == NULL) {
    rvex_sysreturn_err(ENOMEM);
  }
  
  // Forward the call.
  int res = sys_rootfs_unlink(fname_buf);
  
  // Free the buffers.
  free(fname_buf);
  
  // Handle failure.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_MKDIR
/**
 * Implements the mkdir syscall.
 */
rvex_sysreturn_t sys_mkdir(const char *path, int mode) {
  
  // Check input.
  if (path == NULL) rvex_sysreturn_err(EINVAL);
  
  // Make a buffer for the path.
  char *path_buf = strdup(path);
  if (path_buf == NULL) {
    rvex_sysreturn_err(ENOMEM);
  }
  
  // Forward the call.
  int res = sys_rootfs_mkdir(path_buf, mode);
  
  // Free the buffers.
  free(path_buf);
  
  // Handle failure.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_RENAME
/**
 * Implements the rename syscall.
 */
rvex_sysreturn_t sys_rename(const char *old, const char *new) {
  
  // Check input.
  if (old == NULL) rvex_sysreturn_err(EINVAL);
  if (new == NULL) rvex_sysreturn_err(EINVAL);
  
  // Make buffers for the paths.
  char *old_buf = strdup(old);
  if (old_buf == NULL) {
    rvex_sysreturn_err(ENOMEM);
  }
  char *new_buf = strdup(new);
  if (new_buf == NULL) {
    free(old_buf);
    rvex_sysreturn_err(ENOMEM);
  }
  
  // Forward the call.
  int res = sys_rootfs_rename(old_buf, new_buf);
  
  // Free the buffers.
  free(old_buf);
  free(new_buf);
  
  // Handle failure.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_MOUNT
/**
 * Implements the mount syscall. Note that mount() is abused for creating
 * device files as well, in which case *target must be a file instead of a
 * directory.
 */
rvex_sysreturn_t sys_mount(
  const char *source,
  const char *target,
  const char *driver,
  unsigned long mountflags,
  const void *data
) {
  
  // Check input.
  if (target == NULL) rvex_sysreturn_err(EINVAL);
  if (driver == NULL) rvex_sysreturn_err(EINVAL);
  
  // Make buffers for the paths.
  char *source_buf = NULL;
  if (source != NULL) {
    source_buf = strdup(source);
    if (source_buf == NULL) {
      rvex_sysreturn_err(ENOMEM);
    }
  }
  char *target_buf = strdup(target);
  if (target_buf == NULL) {
    free(source_buf);
    rvex_sysreturn_err(ENOMEM);
  }
  
  // Forward the call.
  int res = sys_rootfs_mount(source_buf, target_buf, driver, mountflags, data);
  
  // Free the buffers.
  free(source_buf);
  free(target_buf);
  
  // Handle failure.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_UMOUNT
/**
 * Implements the umount syscall.
 */
rvex_sysreturn_t sys_umount(const char *target) {
  
  // Check input.
  if (target == NULL) rvex_sysreturn_err(EINVAL);
  
  // Make a buffer for the path.
  char *target_buf = strdup(target);
  if (target_buf == NULL) rvex_sysreturn_err(ENOMEM);
  
  // Forward the call.
  int res = sys_rootfs_umount(target_buf);
  
  // Free the buffers.
  free(target_buf);
  
  // Handle failure.
  if (res) rvex_sysreturn_err(res);
  
  // Success.
  rvex_sysreturn_ok(int, 0);
}
#endif

#ifdef ENABLE_SYS_CLOSE

#ifdef ENABLE_SYS_UMOUNT

/**
 * Weak putsk handler. This is called when umounting to print whether it is
 * safe to turn the system off.
 */
void __attribute__((weak)) putsk(const char *s);
void putsk(const char *s) {}

/**
 * Tries to unmount all mount points in the specified directory in arbitrary
 * order. Returns 1 if anything was unmounted. Returns 0 if there was nothing
 * to umount, or all umounts failed.
 */
static int libfs_shutdown_umount_dir(char *path, rootfs_object_t *ob, int force) {
  
  // Check that the current object is actually a directory.
  if (!(ob->attrib & ROOTFS_ATTR_DIR)) return 0;
  
  // If the capacity of the directory is zero, there is nothing to umount.
  if (!ob->capacity) return 0;
  
  // Get the end of the current path.
  char *pathpos = path + strlen(path);
  
  // Iterate over the directory entries.
  rootfs_dirent_t *ent = (rootfs_dirent_t*)ob->contents;
  unsigned long offs = 0;
  unsigned long size = ob->size;
  int anything = 0;
  #pragma unroll(0)
  for (; (offs += sizeof(rootfs_dirent_t)) <= size; ent++) {
    
    // Skip empty slots.
    if (!ent->ob) continue;
    
    // If this object is a directory, recurse into it. We don't care if it's
    // a mount point or not; a directory is a directory, and the mount may
    // technically be hiding other mounts, so we'll just ignore any mount when
    // recursing.
    if (ent->ob->attrib & ROOTFS_ATTR_DIR) {
      sprintf(pathpos, "%s/", ent->name);
      anything |= libfs_shutdown_umount_dir(path, ent->ob, force);
      *pathpos = 0;
    }
      
    // If this object is a mount point, call umount.
    if (ent->ob->mount) {
      int res = sys_rootfs_umount_ob(ent->ob, force);
      if (!res) {
        putsk(" - ");
        if (force) putsk("forcibly ");
        putsk("umounted ");
        putsk(path);
        putsk(ent->name);
        putsk(".\n");
      }
      anything |= !res;
    }
    
  }
  
  // Return whether we unmounted anything.
  return anything;
  
}

/**
 * Tries to unmount all mount points in arbitrary order. Returns 1 if anything
 * was unmounted. In that case, it should be called again. Returns 0 if nothing
 * new was unmounted. In that case, it should be called once with force set.
 */
static int libfs_shutdown_umount_all(int force) {
  
  // Get the root directory object.
  char path[256] = {'/', '\0'};
  char *pathp = path;
  rootfs_object_t *ob = NULL;
  if (sys_rootfs_resolve(&pathp, &ob, NULL, NULL, NULL)) return 0;
  
  // Unmount all in this directory.
  return libfs_shutdown_umount_dir(path, ob, force);
}

#endif

/**
 * Shutdown handler. This tries to close all open file descriptors, and then
 * tries to unmount all mount points.
 */
static void libfs_shutdown(int code) {
  
  putsk("libfs shutdown!\n");
  
  // Disable interrupts while we do this.
  CR_CCR = CR_CCR_IEN_C;
  
  // Iterate over the file descriptor list and close the open ones.
  int i;
  for (i = 0; i < fd_map_capacity; i++) {
    if (fd_map[i] != NULL) {
      sys_close(i);
    }
  }
  
#ifdef ENABLE_SYS_UMOUNT
  
  // We unmount in arbitrary order. Because mounts may depend on one another,
  // umounts may return EBUSY. To get all of them, we simply repeat the umount
  // process until we failed to umount anything new.
  while (libfs_shutdown_umount_all(0));
  
  // Forcibly umount any remaining mounts.
  libfs_shutdown_umount_all(1);
  
#endif

  putsk("libfs shutdown complete.\n");
  
}

PNP_REGISTER_EXIT(libfs_shutdown, libfs_shutdown);

#endif

