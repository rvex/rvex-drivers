
#include <sys/rootfs.h>

/**
 * Internal close function, which actually closes a non-mounted reference
 * for sys_rootfs_close() and sys_rootfs_closedir(),
 */
void sys_rootfs_close_int(rootfs_object_ref_t *ref) {
  rootfs_object_t *ob = ref->ob;
  
  // Update the reference counter.
  if (ob->anon_links > 0) {
    ob->anon_links--;
  } else {
    ob->anon_links = 0;
  }
  
  // Mark the object reference as closed.
  ref->ob = NULL;
  
  // Free the memory used by the file if all links are gone.
  sys_rootfs_free_ob(ob);
  
}

