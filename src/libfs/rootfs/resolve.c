
#include <sys/rootfs.h>

/**
 * Root directory filesystem object.
 */
static rootfs_object_t root = {
  .attrib = ROOTFS_ATTR_DIR | ROOTFS_ATTR_OBJECT_STATIC,
  .links = 2,
  .mode = 0777
};

/**
 * Resolves a path to an object file. Returns 0 or the appropriate errno.
 */
int sys_rootfs_resolve(
  char **path,                // **path is sanitized, *path is set to the path
                              //   remnant for the mount, or NULL for
                              //   non-mounts.
  rootfs_object_t **ob,       // *ob is set to point to the resolved object.
  rootfs_dirent_t **dirent,   // *dirent is set to point to the directory entry
                              //   for the resolved object, or NULL if *ob is
                              //   the root directory. Can be NULL.
  rootfs_object_t **dir,      // *dir is set to point to the directory
                              //   containing the resolved object, or NULL if
                              //   *ob is the root directory. Can be NULL.
  rootfs_object_t *exclude    // If this object is encountered while iterating,
                              //   EINVAL is returned (used by rename).
) {
  
  // Check parameters.
  if (!path) return EINVAL;
  if (!*path) return EINVAL;
  if (!ob) return EINVAL;
  char *p = *path;
  
  // Sanitize the path.
  int err = sys_rootfs_sanitize_path(p);
  if (err) return err;
  
  // Start in the root.
  rootfs_object_t *o = &root;
  if (dir) *dir = NULL;
  if (dirent) *dirent = NULL;
  
  // Iterate over the path components.
  #pragma unroll(0)
  while (1) {
    
    // Save the path before stripping off the / and path component, so we can
    // return it for the driver in case this turns out to be a mount point.
    char *remnant = p;
    
    // Strip the slash in the path.
    if (*p == '/') p++;
    
    // If we're now at the end of the path, we've found the requested object.
    if (!*p) {
      *path = NULL;
      *ob = o;
      return 0;
    }
    
    // Check the exclude.
    if (o == exclude) return EINVAL;
    
    // Check that the current object is actually a directory.
    if (!(o->attrib & ROOTFS_ATTR_DIR)) {
      return ENOTDIR;
    }
    
    // Check that we can enter this directory.
    if (!(o->mode & ROOTFS_MODE_EXEC)) return EPERM;
    
    // If the object is a mount point, we need to pass control to the driver.
    if (o->mount) {
      *path = remnant;
      *ob = o;
      return 0;
    }
    
    // Load the next path component. If this is longer than ROOTFS_NAME_LEN, we
    // can immediately say that the file does not exist.
    char fname[ROOTFS_NAME_LEN+1];
    int len = 0;
    while (1) {
      
      // If we reach the end of the current path entry, null terminate and
      // break.
      if (*p == 0 || *p == '/') {
        fname[len] = 0;
        break;
      }
      
      // If we're getting a new character after already getting ROOTFS_NAME_LEN
      // characters, the file cannot exist.
      if (len >= ROOTFS_NAME_LEN) {
        return ENOENT;
      }
      
      // Save the next character.
      fname[len] = *p++;
      len += 1;
      
    }
    
    // Handle . entry.
    if (!strcmp(".", fname)) {
      continue;
    }
    
    // Handle .. entry.
    if (!strcmp("..", fname)) {
      if (o->parent) o = o->parent;
      continue;
    }
    
    // If the directory is empty, the file obviously doesn't exist, and more
    // importantly, we shouldn't dereference o->contents.
    if (!o->capacity) {
      return ENOENT;
    }
    
    // Search the directory entries for the requested file.
    rootfs_dirent_t *ent = (rootfs_dirent_t*)o->contents;
    unsigned long offs = 0;
    unsigned long size = o->size;
    
    // Iterate over the directory entries.
    int found = 0;
    #pragma unroll(0)
    for (; (offs += sizeof(rootfs_dirent_t)) <= size; ent++) {
      
      // If there is no object associated with this entry, it represents an
      // empty slot, so skip it.
      if (!ent->ob) {
        continue;
      }
      
      // Compare the filenames.
      if (!strncmp(fname, ent->name, ROOTFS_NAME_LEN)) {
        found = 1;
        if (dir) *dir = o;
        if (dirent) *dirent = ent;
        o = ent->ob;
        break;
      }
      
    }
    if (!found) {
      return ENOENT;
    }
    
  }
  
}

