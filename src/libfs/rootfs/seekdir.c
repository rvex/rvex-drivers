
#include <sys/rootfs.h>

/**
 * Seeks within a directory or tells the current position. The following values
 * are allowed for offset:
 *  - -1: return the current position without manipulating the pointer.
 *  - 0: rewind to the start of the directory.
 *  - value previously returned by seekdir(ref, -1): seek back to that
 *    position.
 */
rootfs_seekdir_ret_t sys_rootfs_seekdir(rootfs_object_ref_t *ref, _off_t offset) {
  rootfs_seekdir_ret_t r = {};
  
  // Check arguments.
  if (!ref) { r.err_no = EBADF; return r; }
  if (offset < -1) { r.err_no = EINVAL; return r; }
  if (!ref->ob) { r.err_no = EBADF; return r; }
  if (!(ref->flags & ROOTFS_O_DIRECTORY)) { r.err_no = EBADF; return r; }
  rootfs_object_t *ob = ref->ob;
  
  // Handle forwarding to driver.
  if (ob->mount) {
    if (ob->mount->seekdir) {
      return ob->mount->seekdir(ob->mount_ref, ref->drv_ref, offset);
    } else {
      r.err_no = ENOSYS;
      return r;
    }
  }
  
  // The internal directories actually start at -2 to handle the . and ..
  // directories. Since negative offsets are reserved for seekdir, and 0 needs
  // to be the start of the directory, we add 2 to the returned positions and
  // subtract 2 from the received positions.
  
  // Update the offset if we receive a non-negative value.
  if (offset >= 0) {
    ref->offs = offset - 2;
  }
  
  // Return the current/new offset.
  r.pos = ref->offs + 2;
  
  // Success.
  return r;
}

