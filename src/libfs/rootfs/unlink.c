
#include <sys/rootfs.h>

/**
 * Removes the link specified by path. Returns errno or 0 on success.
 */
int sys_rootfs_unlink(char *path) {
  
  // Check arguments.
  if (!path) return EINVAL;
  
  // Resolve the path.
  rootfs_object_t *ob = NULL;
  rootfs_object_t *dir = NULL;
  rootfs_dirent_t *dirent = NULL;
  int res = sys_rootfs_resolve(&path, &ob, &dirent, &dir, NULL);
  if (res) return res;
  
  // Handle forwarding to driver.
  if (ob->mount) {
    if (path) {
      
      // Path refers to something within the mount point, so forward it.
      if (ob->mount->unlink) {
        return ob->mount->unlink(ob->mount_ref, path);
      } else {
        return ENOSYS;
      }
      
    } else {
      
      // Cannot delete the mount point itself without first unmounting it.
      return EBUSY;
      
    }
  }
  
  // If there is no directory entry for this file, the application is trying
  // to unlink the root directory.
  if (!dirent) return EPERM;
  
  // Check that we have write permissions in the directory.
  if (!(dir->mode & ROOTFS_MODE_WRITE)) return EPERM;
  
  // If this is a non-empty directory, ensure that it's empty. If it isn't,
  // fail with EISDIR.
  if (ob->attrib & ROOTFS_ATTR_DIR) {
    
    // Check alignment.
    if ((long)(ob->contents) & 3) return EIO;
    
    // Iterate over the directory.
    const rootfs_dirent_t *ent;
    off_t offs = 0;
    #pragma unroll(0)
    for (offs = 0; offs + sizeof(rootfs_dirent_t) <= ob->size; offs += sizeof(rootfs_dirent_t)) {
      
      // Get the pointer to the next directory entry.
      ent = (rootfs_dirent_t*)(ob->contents + offs);
      
      // If this is not a deleted entry, fail.
      if (ent->ob) return EISDIR;
      
    }
    
  }
  
  // Unlink the file.
  dirent->ob = NULL;
  if (ob->links > 0) ob->links--;
  
  // Free the memory used by the file if all links are gone.
  sys_rootfs_free_ob(ob);
  
  return 0;
}

