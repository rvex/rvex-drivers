
#include <sys/rootfs.h>

/**
 * Returns the file type of the specified object using the dirent.h constants
 * (DT_*). The stat constants (S_IF*) can be retrieved by leftshifting by 12
 * and masking with S_IFMT.
 */
int sys_rootfs_getftype(const rootfs_object_t *ob) {
  if (ob->attrib & ROOTFS_ATTR_DIR) {
    return DT_DIR;
  } else if (ob->mount) {
    return ob->mount->d_type;
  } else if (ob->attrib & ROOTFS_ATTR_CONTENTS_STATIC) {
    return DT_BLK;
  } else {
    return DT_REG;
  }
}

