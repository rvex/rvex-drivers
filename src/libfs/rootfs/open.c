
#include <sys/rootfs.h>

/**
 * Opens/creates a file by name. Returns errno or 0 on success. flags must
 * already have been incremented by 1 to get read/write access as a bitmask.
 * *path is undefined after the call.
 */
int sys_rootfs_open(rootfs_object_ref_t *ref, char *path, int flags, int mode) {
  
  // Check arguments.
  if (!ref) return EBADF;
  if (ref->ob) return EBADF;
  if (!path) return EINVAL;
  
  // Look up the file.
  char *remnant = path;
  rootfs_object_t *ob = NULL;
  int ret = sys_rootfs_resolve(&remnant, &ob, NULL, NULL, NULL);
  
  // If ret is zero, the file was found, or we encountered a directory mount
  // point.
  if (ret == 0) {
    
    // If we do not have a path remnant, i.e. the file actually exists (and is
    // not just a forward into the mounted filesystem), we must fail if O_CREAT
    // and O_EXCL are both specified.
    if (!remnant && (flags & O_EXCL) && (flags & O_CREAT)) return EEXIST;
    
    // If we do not have a path remnant and the object is a directory, we must
    // fail if any read/write permissions are requested. Use opendir() to read
    // directories instead. However, stat, ioctl, and fcntl are legal on
    // directories, and *should* be opened/closed with open().
    if (!remnant && (ob->attrib & ROOTFS_ATTR_DIR) && (flags & (ROOTFS_O_READ | ROOTFS_O_WRITE))) return EPERM;
    
    // Now we can just forward to sys_rootfs_open_ob.
    return sys_rootfs_open_ob(ref, ob, remnant, flags, mode);
    
  }
  
  // The file does not exist yet (and isn't in a directory mount), so we need
  // to create it if O_CREAT is specified. If O_CREAT is not specified, we
  // fail with ENOENT.
  if (!(flags & O_CREAT)) return ENOENT;
  
  // Allocate a new file object.
  ob = (rootfs_object_t*)calloc(1, sizeof(rootfs_object_t));
  if (!ob) return ENOSPC;
  
  // Set file mode.
  ob->mode = mode;
  
  // Call link to add a directory entry for the file.
  ret = sys_rootfs_link_ob(ob, path);
  
  // (*path is botched from here onwards)
  
  // Check for errors.
  if (ret) {
    free(ob);
    return ret;
  }
  
  // Now we leave it to sys_rootfs_open_ob to open the file we just created.
  return sys_rootfs_open_ob(ref, ob, NULL, flags, mode);
  
}

