
#include <sys/rootfs.h>

/**
 * Plug-and-play declaration for filesystem driver registration. This is
 * defined in syscalls.c, to ensure that syscalls.c is included.
 */
extern const long rootfs_drivers_dummy;
#define DRIVER_LIST ((const rootfs_driver_t*)(&rootfs_drivers_dummy + 1))

/**
 * Mounts a filesystem driver at the specified path. The driver is identified
 * by the "driver" string. arg is driver-specific. Returns errno or 0 on
 * success. The supplied path buffers must be considered to be invalid after
 * the call.
 */
int sys_rootfs_mount(char *source, char *target, const char *driver, unsigned long mountflags, const void *arg) {
  
  // Check arguments.
  if (!target) return EINVAL;
  if (!driver) return EINVAL;
  
  // Look up the file.
  rootfs_object_t *ob = NULL;
  int ret = sys_rootfs_resolve(&target, &ob, NULL, NULL, NULL);
  if (ret) return ret;
  
  // Make sure the file is not already mounted.
  if (ob->mount) return EBUSY;
  
  // Make sure the file is not open.
  if (ob->anon_links) return EBUSY;
  
  // If the driver is "block", we don't actually mount a driver. We instead
  // replace the file contents with the provided statically allocated block.
  if (!strncmp("block", driver, 16)) {
    
    // Cast the argument to the mount_block_arg_t structure.
    if (!arg) return EINVAL;
    const mount_block_arg_t *block_arg = (const mount_block_arg_t*)arg;
    
    // Free any existing contents if necessary.
    if (!(ob->attrib & ROOTFS_ATTR_CONTENTS_STATIC)) {
      free(ob->contents);
    }
    
    // Configure the file.
    ob->attrib |= ROOTFS_ATTR_CONTENTS_STATIC;
    ob->contents = block_arg->start;
    ob->size = block_arg->size;
    ob->capacity = block_arg->size;
    
    // Success.
    return 0;
  }
  
  // Search the driver list for the specified driver.
  const rootfs_driver_t *drv_info = NULL;
  int i;
  #pragma unroll(0)
  for (i = 0; DRIVER_LIST[i].mount; i++) {
    if (!strncmp(DRIVER_LIST[i].name, driver, 16)) {
      drv_info = DRIVER_LIST + i;
      break;
    }
  }
  
  // Check the driver record.
  if (!drv_info) return EINVAL;
  if (!drv_info->mount) return EINVAL;
  
  // Request the mount.
  rootfs_mount_ret_t mr = drv_info->mount(!!(ob->attrib & ROOTFS_ATTR_DIR), source, mountflags, arg);
  if (mr.err_no) return mr.err_no;
  
  // Mark the file as mounted.
  ob->mount = drv_info;
  ob->mount_ref = mr.drv_ref;
  
  // Success.
  return 0;
}

