
RootFS
======

This library provides a volatile in-memory filesystem, backed by newlib's
heap management. Quite a number of Linux-like things are supported, such as
functioning hardlinks, file descriptor duplication, and (single-user) file
and directory permissions. Furthermore, the filesystem supports rudimentary
mounts, which can be used by (proper) filesystem drivers and device file
handlers. Some of these drivers are included in the library, but the driver
list is completely extensible without needing to modify any of rootfs' sources.
Refer to the `README.md` file in the `drivers` subdirectory for more
information.

**NOTE:** rootfs files and directories are backed simply by realloc. Therefore,
growing a file or directory has quadratic complexity. You therefore probably
don't want to rely on rootfs files for any kind of write performance.

This library contains only a few optional utility functions that may be
called from userspace (for as far as there is such a thing on r-VEX). These
can be found in the `user` subdirectory. Pretty much the entire interface
with the user program happens via syscalls, which in turn is done by overriding
the weak syscall handler symbols supplied by newlib. Because of this, the
linker needs to be tricked to actually include the library. This can be done
simply by `#include <rootfs.h>`, which defines a static variable initialized
with the address of a dummy variable defined within the library, thereby
creating the library dependency. Both variables are placed in the `.discard`
section; this section can be thrown away at link time.

When this library is included without user initialization,
stdin/stdout/stderr are not opened as newlib would expect, and the filesystem
will be completely empty. The intended way to initialize the filesystem is as
follows:

    #include <machine/rvex-sys.h>
    #include <rootfs.h>
    #include <unistd.h>
    #include <stdlib.h>
    #include <fcntl.h>
    #include <rvudev.h>
    
    static void filesystem_setup(void) {
      
      // Create /dev directory for device files.
      if (mkdir("/dev", 0777)) exit(errno);
      
      // Create an r-VEX UART file for stdin/stdout/stderr.
      if (mount_rvudev("/dev/ttyS0", 0xD1000000)) exit(errno);
      
      // Open the UART. This should become fd 0 since no files are open yet,
      // which is stdin.
      if (open("/dev/ttyS0", O_RDWR, 0) != 0) exit(errno);
      
      // dup the file descriptor twice to initialize file descriptors 1 and 2
      // for stdout and stderr.
      if (fcntl(0, F_DUPFD, 1) != 1) exit(errno);
      if (fcntl(0, F_DUPFD, 2) != 2) exit(errno);
      
      // Configure the rest of the filesystem.
      ...
      
    }
    
    PNP_REGISTER_SETUP(filesystem_setup, filesystem_setup)

Of course, what you put in the `filesystem_setup` function is
platform-dependent, which is why this isn't done from within libfs. The
`mount_rvudev` call creates a serial port device file for the r-VEX debug UART
at address `0xD1000000`, which is subsequently opened in file descriptors 0
through 2 (libfs always gives you the lowest available file descriptor, so the
right file descriptors are used simply by ensuring that no files are open before
these calls).

Note that newlib buffers stdout. Unless you write a *lot* of data to stdout, the
data will only be written when `exit` closes the stdout file descriptor. If you
need line buffering, use stderr instead. You can also use `fflush(stdout);`
after writing to stdout.
