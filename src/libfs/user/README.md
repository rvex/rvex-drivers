
RootFS userspace functions
==========================

This folder contains some userspace functions that kind of belong in the C
runtime but currently aren't in there because I haven't worked out how to add a
new "system" to newlib. So, for the time being, they'll be in libfs.
