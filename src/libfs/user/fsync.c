
#include <sys/ioctl.h>
#include <stdlib.h>

/**
 * Syncs a file to the underlying storage medium.
 */
int fsync(int fd) {
  return ioctl(fd, IO_SYNC, NULL);
}

