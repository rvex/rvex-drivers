
#include <sys/dirent.h>
#include <unistd.h>

/**
 * Closes a directory, interface matches POSIX.
 */
int closedir(DIR *dirp) {
  return close((int)(long)dirp - 1);
}

