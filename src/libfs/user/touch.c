
#include <rootfs.h>
#include <unistd.h>
#include <fcntl.h>

/**
 * Implementation of touch, used by driver mount() functions a lot to ensure
 * file existence.
 */
int touch(const char *fname) {
  int fd = open(fname, O_RDONLY | O_CREAT, 0666);
  if (fd < 0) return -1;
  if (close(fd)) return -1;
  return 0;
}

