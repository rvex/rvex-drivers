
#include <errno.h>
#include <sys/dirent.h>
#include <unistd.h>
#include <reent.h>

/**
 * Reentrant read from a directory, interfaces match POSIX.
 */
int readdir_r(DIR *dirp, struct dirent *entry, struct dirent **result) {
  
  // Get the file descriptor number.
  int fd = (int)(long)dirp - 1;
  
  // Do a reentrant read from the file descriptor.
  struct _reent r;
  r._errno = 0;
  int count = _read_r(&r, fd, entry, sizeof(struct dirent));
  
  // Convert to readdir_r's interface.
  if (count >= sizeof(struct dirent)) {
    *result = entry;
  } else {
    *result = NULL;
    if (count < 0) {
      return r._errno;
    }
  }
  return 0;
}
