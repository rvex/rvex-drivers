
#include <errno.h>
#include <sys/dirent.h>
#include <unistd.h>

/**
 * Rewinds a directory, interface matches POSIX.
 */
void rewinddir(DIR *dirp) {
  
  // Get the file descriptor number.
  int fd = (int)(long)dirp - 1;
  
  // Perform a seek to 0.
  lseek(fd, 0, SEEK_SET);
  
}

