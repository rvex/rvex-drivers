
#include <errno.h>
#include <sys/dirent.h>
#include <string.h>

/**
 * Read from a directory, interfaces match POSIX.
 */
struct dirent *readdir(DIR *dirp) {
  
  // Prepare a struct dirent buffer.
  static struct dirent entry;
  memset(&entry, 0, sizeof(struct dirent));
  
  // Pass through to readdir_r.
  struct dirent *result;
  int res = readdir_r(dirp, &entry, &result);
  
  // Convert to readdir's interface.
  if (res) {
    errno = res;
    return NULL;
  } else {
    return result;
  }
}

