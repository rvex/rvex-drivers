
#include <rootfs.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/dirent.h>

/**
 * Opens a directory, interface matches POSIX.
 */
DIR *opendir(const char *path) {
  int fd = open(path, O_RDONLY | O_DIRECTORY, 0);
  if (fd < 0) return NULL;
  return (DIR*)(long)(fd + 1);
}

