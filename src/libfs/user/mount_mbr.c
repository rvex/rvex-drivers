
#include <rootfs.h>
#include <fileslice.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

/**
 * Reads the master boot record in dev and tries to mount its partitions as
 * dev1..4. Returns -1 if the disk does not appear to have a valid MBR, a
 * mount failed, or something else went wrong.
 */
int mount_mbr(const char *dev) {
  
  // Open the file.
  int fd = open(dev, O_RDONLY, 0);
  if (fd < 0) return -1;
  
  // Seek to offset 446, which is where the partition entries start.
  if (lseek(fd, 446, SEEK_SET) < 0) goto error;
  
  // Read the partition entries and trailing magic number.
  unsigned char buf[66];
  if (read(fd, buf, 66) != 66) goto error;
  
  // Check magic number.
  if ((buf[64] != 0x55) || (buf[65] != 0xAA)) goto error;
  
  // Make a buffer for the filename.
  int l = strlen(dev);
  char *namebuf = (char*)malloc(l+2);
  if (!namebuf) goto error;
  strcpy(namebuf, dev);
  namebuf[l+1] = 0;
  
  // Loop over the partitions.
  int i;
  unsigned char *part;
  for (i = 0, part = (unsigned char*)buf; i < 4; i++, part += 16) {
    
    // System ID must be nonzero.
    if (!part[i + 4]) continue;
    
    // Start sector.
    unsigned long start = ((unsigned long)part[8]  << 0)
                        | ((unsigned long)part[9]  << 8)
                        | ((unsigned long)part[10] << 16)
                        | ((unsigned long)part[11] << 24);
    if (!start) continue;
    
    // Sector count.
    unsigned long count = ((unsigned long)part[12] << 0)
                        | ((unsigned long)part[13] << 8)
                        | ((unsigned long)part[14] << 16)
                        | ((unsigned long)part[15] << 24);
    if (!count) continue;
    
    // Try to mount this partition.
    namebuf[l] = '1' + i;
    if (mount_fileslice(dev, namebuf, start << 9, count << 9)) goto error2;
    
  }
  
  // Close the file.
  free(namebuf);
  close(fd);
  return 0;
  
error2:
  free(namebuf);
error:
  close(fd);
  return -1;
}
