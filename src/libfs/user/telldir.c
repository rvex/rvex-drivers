
#include <errno.h>
#include <sys/dirent.h>
#include <unistd.h>

/**
 * Get the current directory pointer, interface matches POSIX.
 */
long int telldir(DIR *dirp) {
  
  // Get the file descriptor number.
  int fd = (int)(long)dirp - 1;
  
  // Use seek to get the current position.
  return lseek(fd, 0, SEEK_CUR);
  
}
