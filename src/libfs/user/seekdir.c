
#include <errno.h>
#include <sys/dirent.h>
#include <unistd.h>

/**
 * Seek within a directory, interface matches POSIX.
 */
void seekdir(DIR *dirp, long int pos) {
  
  // Get the file descriptor number.
  int fd = (int)(long)dirp - 1;
  
  // Perform a seek to the given position.
  lseek(fd, pos, SEEK_SET);
  
}

