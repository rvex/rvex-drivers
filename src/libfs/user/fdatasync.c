
#include <sys/ioctl.h>
#include <stdlib.h>

/**
 * Syncs a file's contents (not necessarily metadata as well) to the underlying
 * storage medium.
 */
int fdatasync(int fd) {
  return ioctl(fd, IO_SYNC, NULL);
}

