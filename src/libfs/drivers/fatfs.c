/**
 * Driver for mounting FAT12, FAT16 and FAT32 partitions. Uses elm chan's FatFS
 * library; this file is just the glue code between FatFS and libfs.
 */

#include "ffconf.h"
#include "diskio.h"
#include "ff.h"
#include <fatfs.h>
#include <sys/rootfs.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/mount.h>
#include <string.h>
#include <stdlib.h>
#include <sys/dirent.h>

#include <stdio.h>

#define DRIVER_NAME "fatfs"

/**
 * Tuple of block device file reference and FatFS information structure.
 */
typedef struct fatfs_info_t {
  
  // Backing block device.
  rootfs_object_ref_t ref;
  
  // FatFS data structure.
  FATFS ffs;
  
} fatfs_info_t;

/**
 * Open file structure.
 */
typedef struct fatfs_file_t {
  
  // FatFS file structure.
  FATFS_FIL f;

  // Path to this file. Unfortunately needed to implement stat.
  char path[];
  
} fatfs_file_t;

/**
 * Mapping from FatFS drive number to the backing block device reference and
 * FatFS information structure.
 */
static fatfs_info_t *volume_map[FF_VOLUMES];

/**
 * Called by FatFS to initialize the storage device.
 */
DSTATUS drv_ffs_disk_initialize(BYTE pdrv) {
  
  // No initialization code needed here. Simply return the status.
  return drv_ffs_disk_status(pdrv);
}

/**
 * Called by FatFS to retrieve device status.
 */
DSTATUS drv_ffs_disk_status(BYTE pdrv) {
  
  // Check drive number.
  if (pdrv > FF_VOLUMES) return RES_PARERR;
  
  // Return uninitialized if there is no block device open for this drive.
  if (volume_map[pdrv] == NULL) return STA_NOINIT;
  
  // Return write-protected if the block device is not writable.
  if (!(volume_map[pdrv]->ref.flags & ROOTFS_O_WRITE)) return STA_PROTECT;
  
  // No flags set.
  return 0;
}

/**
 * Called by FatFS to read from the disk.
 */
DRESULT drv_ffs_disk_read(BYTE pdrv, BYTE* buff, DWORD sector, UINT count) {
  
  //fprintf(stderr, "attempt read sector %d..%d\n", (int)sector, (int)(sector+count));
  
  // Load drive information.
  if (pdrv > FF_VOLUMES) return RES_PARERR;
  if (volume_map[pdrv] == NULL) return RES_NOTRDY;
  rootfs_object_ref_t *ref = &(volume_map[pdrv]->ref);
  
  // Seek to the desired sector.
  rootfs_lseek_ret_t sr = sys_rootfs_lseek(ref, sector << 9, SEEK_SET);
  if (sr.err_no) return RES_ERROR;
  if (sr.pos != sector << 9) return RES_ERROR;
  
  // Call read() until we have everything.
  int remain = count << 9;
  while (remain) {
    
    // Read data.
    rootfs_read_ret_t rr = sys_rootfs_read(ref, buff, remain);
    if (rr.err_no) return RES_ERROR;
    if (!rr.amount) return RES_ERROR;
    
    // Update the pointers.
    remain -= rr.amount;
    buff += rr.amount;
    
  }
  
  // Operation complete.
  //fprintf(stderr, "read sector %d..%d\n", (int)sector, (int)(sector+count));
  return RES_OK;
}

/**
 * Called by FatFS to write to the disk.
 */
DRESULT drv_ffs_disk_write(BYTE pdrv, const BYTE* buff, DWORD sector, UINT count) {
  
  //fprintf(stderr, "attempt write sector %d..%d\n", (int)sector, (int)(sector+count));
  
  // Load drive information.
  if (pdrv > FF_VOLUMES) return RES_PARERR;
  if (volume_map[pdrv] == NULL) return RES_NOTRDY;
  rootfs_object_ref_t *ref = &(volume_map[pdrv]->ref);
  
  // Seek to the desired sector.
  rootfs_lseek_ret_t sr = sys_rootfs_lseek(ref, sector << 9, SEEK_SET);
  if (sr.err_no) return RES_ERROR;
  if (sr.pos != sector << 9) return RES_ERROR;
  
  // Call write() until we have everything.
  int remain = count << 9;
  while (remain) {
    
    // Write data.
    rootfs_write_ret_t wr = sys_rootfs_write(ref, buff, remain);
    if (wr.err_no) return RES_ERROR;
    if (!wr.amount) return RES_ERROR;
    
    // Update the pointers.
    remain -= wr.amount;
    buff += wr.amount;
    
  }
  
  // Operation complete.
  //fprintf(stderr, "wrote sector %d..%d\n", (int)sector, (int)(sector+count));
  return RES_OK;
}

/**
 * Called by FatFS for various control tasks. With the current configuration,
 * only CTRL_SYNC is issued.
 */
DRESULT drv_ffs_disk_ioctl(BYTE pdrv, BYTE cmd, void* buff) {
  
  // Load drive information.
  if (pdrv > FF_VOLUMES) return RES_PARERR;
  if (volume_map[pdrv] == NULL) return RES_NOTRDY;
  rootfs_object_ref_t *ref = &(volume_map[pdrv]->ref);
  
  // Handle sync command.
  if (cmd == CTRL_SYNC) {
    rootfs_ioctl_ret_t ir = sys_rootfs_ioctl(ref, IO_SYNC, NULL);
    return ir.err_no ? RES_ERROR : RES_OK;
  }
  
  // All following commands need a buffer.
  if (!buff) return RES_PARERR;
  
  // Erase block size is always 1 sector.
  if (cmd == GET_BLOCK_SIZE) {
    *((DWORD*)buff) = 1;
    return RES_OK;
  }
  
  // Sector count and size need a stat.
  if ((cmd == GET_SECTOR_COUNT) || (cmd == GET_SECTOR_SIZE)) {
    struct stat sb;
    int res = sys_rootfs_stat(ref, &sb);
    if (res) return RES_ERROR;
    *((DWORD*)buff) = (DWORD)((cmd == GET_SECTOR_COUNT) ? sb.st_blocks : sb.st_blksize);
    return RES_OK;
  }
  
  // Unknown command, handle as no-op.
  return RES_OK;
}

/**
 * Returns the current time in the inane format FAT32 uses. That is:
 *   31..25: year-1980 (0..127, e.g. 37 for 2017)
 *   24..21: month (1..12)
 *   20..16: day of month (1..31)
 *   15..11: hour (0..23)
 *   10..5:  minute (0..59)
 *   4..0:   second / 2 (0..29) 
 */
DWORD drv_ffs_get_fattime(void) {
  
  // Get the current time with the gettimeofday syscall. Failing that, return
  // a fixed date.
  struct timeval tv;
  if (gettimeofday(&tv, NULL)) return 0x28210000; // midnight 2010-01-01
  
  // Work out the FAT date garbage from the UNIX timestamp in tv.
  unsigned int seconds2, minutes, hours, days;
  seconds2 = tv.tv_sec >> 1;
  minutes  = seconds2 / 30;
  seconds2 = seconds2 % 30;
  hours    = minutes  / 60;
  minutes  = minutes  % 60;
  days     = hours    / 24;
  hours    = hours    % 24;
  
  // Days since 1970-01-01 to day/month/year, from
  // https://stackoverflow.com/questions/7960318
  days += 719468;
  int era = (days >= 0 ? days : days - 146096) / 146097;
  unsigned int doe = (unsigned int)(days - era * 146097);              // [0, 146096]
  unsigned int yoe = (doe - doe/1460 + doe/36524 - doe/146096) / 365;  // [0, 399]
  int          y = (int)(yoe) + era * 400;
  unsigned int doy = doe - (365*yoe + yoe/4 - yoe/100);                // [0, 365]
  unsigned int mp = (5*doy + 2)/153;                                   // [0, 11]
  unsigned int d = doy - (153*mp+2)/5 + 1;                             // [1, 31]
  unsigned int m = mp + (mp < 10 ? 3 : -9);                            // [1, 12]
  
  // Shift everything together.
  return ((y - 1980) & 0x7F) << 25
       | (m          & 0x0F) << 21
       | (d          & 0x1F) << 16
       | (hours      & 0x1F) << 11
       | (minutes    & 0x3F) << 5
       | (seconds2   & 0x1F) << 0;
}

/**
 * Turns a FAT timestamp into a Unix timestamp.
 */
static unsigned int fat2unix(DWORD fattime) {
  
  // Unpack the fat time.
  unsigned int sec = ((fattime <<  1) & 0x3F);
  unsigned int min = ((fattime >>  5) & 0x3F);
  unsigned int hr  = ((fattime >> 11) & 0x1F);
  unsigned int d   = ((fattime >> 16) & 0x1F);
  unsigned int m   = ((fattime >> 21) & 0x0F);
  unsigned int y   = ((fattime >> 25) & 0x7F) + 1980;
  
  // Figure out days since 1970-01-01 from day/month/year, from
  // https://stackoverflow.com/questions/7960318
  y -= m <= 2;
  int          era = (y >= 0 ? y : y-399) / 400;
  unsigned int yoe = (unsigned int)(y - era * 400);             // [0, 399]
  unsigned int doy = (153*(m + (m > 2 ? -3 : 9)) + 2)/5 + d-1;  // [0, 365]
  unsigned int doe = yoe * 365 + yoe/4 - yoe/100 + doy;         // [0, 146096]
  int          dse = era * 146097 + (int)(doe) - 719468;
  
  // Combine days since epoch with time of day.
  return dse * (24 * 60 * 60)
       + hr  *      (60 * 60)
       + min *           (60)
       + sec;
}

/**
 * Best-effort mapping from FRESULT to POSIX errno.
 */
static const short drv_ffs_fres2errno[20] = {
  0,      /*FR_OK*/
  EIO,    /*FR_DISK_ERR*/
  EIO,    /*FR_INT_ERR*/
  EIO,    /*FR_NOT_READY*/
  ENOENT, /*FR_NO_FILE*/
  ENOENT, /*FR_NO_PATH*/
  ENOENT, /*FR_INVALID_NAME*/
  EPERM,  /*FR_DENIED*/
  EEXIST, /*FR_EXIST*/
  EBADF,  /*FR_INVALID_OBJECT*/
  EPERM,  /*FR_WRITE_PROTECTED*/
  EINVAL, /*FR_INVALID_DRIVE*/
  EINVAL, /*FR_NOT_ENABLED*/
  EIO,    /*FR_NO_FILESYSTEM*/
  EIO,    /*FR_MKFS_ABORTED*/
  EIO,    /*FR_TIMEOUT*/
  EIO,    /*FR_LOCKED*/
  ENOMEM, /*FR_NOT_ENOUGH_CORE*/
  ENFILE, /*FR_TOO_MANY_OPEN_FILES*/
  EINVAL, /*FR_INVALID_PARAMETER*/
};

/**
 * Converts a FatFS error code to the closest-matching errno.
 */
static int drv_ffs_conv_errno(FRESULT res) {
  if (res > 20) return EINVAL;
  return (int)drv_ffs_fres2errno[res];
}

/**
 * Mounts a block device with FatFS.
 */
static rootfs_mount_ret_t drv_ffs_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Can only mount to a directory.
  if (!isdir) { r.err_no = ENOTDIR; return r; }
  
  // We need a source file.
  if (!src) { r.err_no = EINVAL; return r; }
  
  // Look for an unused FatFS drive number.
  BYTE pdrv;
  for (pdrv = 0; pdrv < FF_VOLUMES; pdrv++) {
    if (volume_map[pdrv] == NULL) {
      break;
    }
  }
  
  // If there is no empty drive, return "out of file numbers".
  if (pdrv >= FF_VOLUMES) { r.err_no = ENFILE; return r; }
  
  // Figure out file open flags.
  int flags = ROOTFS_O_READ | (arg ? 0 : ROOTFS_O_WRITE);
  
  // Allocate the mount and filesystem information structure.
  fatfs_info_t *ffsi = (fatfs_info_t*)calloc(1, sizeof(fatfs_info_t));
  if (!ffsi) { r.err_no = ENOMEM; return r; }
  
  // Open the file.
  char *src_buf = strdup(src);
  if (!src_buf) { r.err_no = ENOMEM; goto cancel1; }
  int res = sys_rootfs_open(&(ffsi->ref), src_buf, flags, 0);
  free(src_buf);
  if (res) { r.err_no = res; goto cancel1; }
  
  // Now tell FatFS to mount the drive.
  volume_map[pdrv] = ffsi;
  char path[3] = { '0' + pdrv, ':', '\0' };
  res = drv_ffs_conv_errno(drv_ffs_f_mount(&(ffsi->ffs), path, 1));
  if (res) { r.err_no = res; goto cancel2; }
  
  // Save the FatFS drive number as the argument.
  r.drv_ref = (void*)(long)pdrv;
  
  // Success.
  return r;
  
cancel2:
  
  // Unmount FatFS.
  drv_ffs_f_mount(NULL, path, 1);
  
  // Revert the volume map.
  volume_map[pdrv] = NULL;
  
  // Close the backing file.
  sys_rootfs_close(&(ffsi->ref));
  
cancel1:
  // Free the data structure.
  free(ffsi);
  
  return r;
}

/**
 * Unmounts a FatFS device.
 */
static int drv_ffs_umount(void *drv_ffs_mount) {
  
  // Load drive information.
  BYTE pdrv = (BYTE)(long)drv_ffs_mount;
  if (pdrv > FF_VOLUMES) return EINVAL;
  if (volume_map[pdrv] == NULL) return EINVAL;
  fatfs_info_t *ffsi = volume_map[pdrv];
  
  // Unmount FatFS.
  char path[3] = { '0' + pdrv, ':', '\0' };
  drv_ffs_f_mount(NULL, path, 1);
  
  // Update the volume map.
  volume_map[pdrv] = NULL;
  
  // Close the backing file.
  sys_rootfs_close(&(ffsi->ref));
  
  // Free the data structure.
  free(ffsi);
  
  // Success.
  return 0;
}

/**
 * Converts a rootfs filename remnant to a fatfs path. Returns NULL if an error
 * occurs. Returns fname if no changes are needed. If the return value is
 * different from fname, it must be freed by the caller.
 */
static const char *drv_ffs_conv_fname(void *drv_ffs_mount, const char *fname) {
  
  // Load drive information.
  BYTE pdrv = (BYTE)(long)drv_ffs_mount;
  if (pdrv > FF_VOLUMES) return NULL;
  
  // If we're given a filename and pdrv is zero, the given filename will also
  // work right for FatFS, so we don't need to allocate a new buffer.
  if (!pdrv && fname && *fname) return fname;
  
  // If we're not given a path remnant or the remnant is empty, we need to
  // return the root directory.
  if (!fname || !*fname) fname = "/";
  
  // Create a buffer two bytes larger than the given filename.
  char *buf = (char*)malloc(strlen(fname) + 3);
  if (buf == NULL) return NULL;
  
  // Write the drive number.
  buf[0] = '0' + pdrv;
  buf[1] = ':';
  
  // Copy the rest of the path.
  strcpy(buf + 2, fname);
  
  // Return the allocated buffer.
  return buf;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_ffs_open(void *drv_ffs_mount, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Convert filename.
  const char *f_fname = drv_ffs_conv_fname(drv_ffs_mount, fname);
  if (!f_fname) { r.err_no = EINVAL; return r; }
  
  // Convert the flags. Note that we don't need to check for O_APPEND; rootfs
  // will seek to EOF before every write if O_APPEND is set.
  BYTE f_mode = 0;
  if (flags & ROOTFS_O_READ)  f_mode |= FA_READ;
  if (flags & ROOTFS_O_WRITE) f_mode |= FA_WRITE;
  if (flags & O_CREAT) f_mode |= (flags & O_EXCL) ? FA_CREATE_NEW : FA_OPEN_ALWAYS;
  
  // Create a file structure.
  fatfs_file_t *fil = (fatfs_file_t*)malloc(sizeof(fatfs_file_t) + strlen(f_fname) + 1);
  if (!fil) {
    if (f_fname != fname) free((char*)f_fname);
    r.err_no = ENOMEM;
    return r;
  }
  
  // Copy the filename into the structure.
  strcpy(fil->path, f_fname);
  
  // Free the filename object.
  if (f_fname != fname) free((char*)f_fname);
  
  // If this open is only intended for stat (no read/write flags given), don't
  // actually open the file and set obj to NULL so FatFS will return an error
  // if it tries to use the file.
  if (!(flags & (ROOTFS_O_READ | ROOTFS_O_WRITE))) {
    
    // Only for stat, don't open file.
    fil->f.obj.fs = NULL;
    
  } else {
    
    // Open the file.
    r.err_no = drv_ffs_conv_errno(drv_ffs_f_open(&(fil->f), fil->path, f_mode));
    
    // Check for open errors.
    if (r.err_no) {
      free(fil);
      return r;
    }
    
    // Truncate if desired.
    if (flags & O_TRUNC) {
      r.err_no = drv_ffs_conv_errno(drv_ffs_f_truncate(&(fil->f)));
      if (r.err_no) {
        drv_ffs_f_close(&(fil->f));
        free(fil);
        return r;
      }
    }
    
  }
  
  // Success. Return the FatFS file object as the driver reference.
  r.drv_ref = (void*)fil;
  return r;
}

/**
 * Driver read callback.
 */
static rootfs_read_ret_t drv_ffs_read(void *drv_ffs_mount, void *drv_file, void *buf, size_t count) {
  rootfs_read_ret_t r = {};
  
  // Check arguments.
  if (!drv_file) { r.err_no = EBADF; return r; }
  if (!buf) { r.err_no = EINVAL; return r; }
  fatfs_file_t *fil = (fatfs_file_t*)drv_file;
  if (!fil->f.obj.fs) { r.err_no = EBADF; return r; }
  
  // Forward to FatFS.
  UINT count_out = 0;
  r.err_no = drv_ffs_conv_errno(drv_ffs_f_read(&(fil->f), buf, (UINT)count, &count_out));
  r.amount = count_out;
  return r;
}

/**
 * Driver write callback.
 */
static rootfs_write_ret_t drv_ffs_write(void *drv_ffs_mount, void *drv_file, const void *buf, size_t count) {
  rootfs_write_ret_t r = {};
  
  // Check arguments.
  if (!drv_file) { r.err_no = EBADF; return r; }
  if (!buf) { r.err_no = EINVAL; return r; }
  fatfs_file_t *fil = (fatfs_file_t*)drv_file;
  if (!fil->f.obj.fs) { r.err_no = EBADF; return r; }
  
  // Forward to FatFS.
  UINT count_out = 0;
  r.err_no = drv_ffs_conv_errno(drv_ffs_f_write(&(fil->f), buf, (UINT)count, &count_out));
  r.amount = count_out;
  return r;
}

/**
 * Driver close callback.
 */
static int drv_ffs_close(void *drv_ffs_mount, void *drv_file) {
  
  // Check arguments.
  if (!drv_file) return EBADF;
  fatfs_file_t *fil = (fatfs_file_t*)drv_file;
  
  // Forward to FatFS, unless this is a fake file intended only for stat.
  if (fil->f.obj.fs) {
    int res = drv_ffs_conv_errno(drv_ffs_f_close(&(fil->f)));
    if (res) return res;
  }
  
  // Free the file structure.
  free(fil);
  
  // Success.
  return 0;
}

/**
 * Driver lseek callback.
 */
static rootfs_lseek_ret_t drv_ffs_lseek(void *drv_ffs_mount, void *drv_file, off_t offs, int whence) {
  rootfs_lseek_ret_t r = {};
  
  // Check arguments.
  if (!drv_file) { r.err_no = EBADF; return r; }
  fatfs_file_t *fil = (fatfs_file_t*)drv_file;
  if (!fil->f.obj.fs) { r.err_no = EBADF; return r; }
  
  // Figure out the desired position.
  off_t new_pos = offs;
  switch (whence) {
    case SEEK_SET: break;
    case SEEK_CUR: new_pos += (off_t)drv_ffs_f_tell(&(fil->f)); break;
    case SEEK_END: new_pos += (off_t)drv_ffs_f_size(&(fil->f)); break;
    default: { r.err_no = EINVAL; return r; }
  }
  
  // Check range.
  if ((new_pos < 0) || (new_pos > (off_t)drv_ffs_f_size(&(fil->f)))) {
    r.err_no = EINVAL;
    return r;
  }
  
  // If the desired offset equals the current one, don't do anything.
  if (new_pos == (off_t)drv_ffs_f_tell(&(fil->f))) {
    r.pos = new_pos;
    return r;
  }
  
  // Forward to FatFS.
  r.err_no = drv_ffs_conv_errno(drv_ffs_f_lseek(&(fil->f), new_pos));
  if (r.err_no) return r;
  
  // Success.
  r.pos = (off_t)drv_ffs_f_tell(&(fil->f));
  return r;
}

/**
 * Driver stat callback.
 */
static int drv_ffs_stat(void *drv_ffs_mount, void *drv_file, struct stat *sb) {
  
  // Check arguments.
  if (!drv_file) return EBADF;
  if (!sb) return EINVAL;
  fatfs_file_t *fil = (fatfs_file_t*)drv_file;
  
  // Perform a stat on the filename carried within the file structure.
  FATFS_FILINFO info;
  int res = drv_ffs_conv_errno(drv_ffs_f_stat(fil->path, &info));
  if (res) return res;
  
  // Convert permissions.
  mode_t mode = (info.fattrib & AM_RDO) ? 0444 : 0666;
  
  // Convert file/directory.
  if (info.fattrib & AM_DIR) {
    
    // This is a directory.
    mode |= S_IFDIR;
    sb->st_nlink = 2;
    
  } else {
    
    // This is a regular file.
    mode |= S_IFREG;
    sb->st_nlink = 1;
    
  }
  
  // Set the filesize.
  sb->st_size  = (size_t)info.fsize;
  
  // Set the times to whatever time we get from FatFS.
  time_t t = fat2unix(((DWORD)(info.fdate) << 16) | (DWORD)(info.ftime));
  sb->st_atime = t;
  sb->st_mtime = t;
  sb->st_ctime = t;
  
  // Success.
  return 0;
};

/**
 * Driver ioctl callback.
 */
static rootfs_ioctl_ret_t drv_ffs_ioctl(void *drv_ffs_mount, void *drv_file, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!drv_file) { r.err_no = EBADF; return r; }
  fatfs_file_t *fil = (fatfs_file_t*)drv_file;
  if (!fil->f.obj.fs) { r.err_no = EBADF; return r; }
  
  // Forward sync request to FatFS.
  if (request == IO_SYNC) {
    r.err_no = drv_ffs_conv_errno(drv_ffs_f_sync(&(fil->f)));
    return r;
  }
  
  // Unknown command.
  r.err_no = EINVAL;
  return r;
}

/**
 * Driver unlink callback.
 */
static int drv_ffs_unlink(void *drv_ffs_mount, const char *path) {
  
  // Convert filename.
  const char *f_path = drv_ffs_conv_fname(drv_ffs_mount, path);
  if (!f_path) return EINVAL;
  
  // Forward to FatFS.
  int res = drv_ffs_conv_errno(drv_ffs_f_unlink(f_path));
  
  // Free the filename buffer.
  if (f_path != path) free((char*)f_path);
  
  // Return success or errno.
  return res;
}

/**
 * Driver rename callback definition.
 */
static int drv_ffs_rename(void *drv_ffs_mount, const char *old, const char *new) {
  
  // Convert filenames.
  const char *f_old = drv_ffs_conv_fname(drv_ffs_mount, old);
  const char *f_new = drv_ffs_conv_fname(drv_ffs_mount, new);
  
  int res;
  if (f_new && f_new) {
    
    // Forward to FatFS.
    res = drv_ffs_conv_errno(drv_ffs_f_rename(f_old, f_new));
    
  } else {
    
    // Failed to convert one or both of the paths.
    res = EINVAL;
    
  }
  
  // Free the filename buffers if they were allocated.
  if (f_old != old) free((char*)f_old);
  if (f_new != new) free((char*)f_new);
  
  // Return success or errno.
  return res;
}

/**
 * Driver mkdir callback definition.
 */
static int drv_ffs_mkdir(void *drv_ffs_mount, const char *path, int mode) {
  
  // Convert filename.
  const char *f_path = drv_ffs_conv_fname(drv_ffs_mount, path);
  if (!f_path) return EINVAL;
  
  // Forward to FatFS.
  int res = drv_ffs_conv_errno(drv_ffs_f_mkdir(f_path));
  
  // Free the filename buffer.
  if (f_path != path) free((char*)f_path);
  
  // Return success or errno.
  return res;
}

/**
 * Driver opendir callback.
 */
static rootfs_opendir_ret_t drv_ffs_opendir(void *drv_ffs_mount, const char *path) {
  rootfs_opendir_ret_t r = {};
  
  // Convert filename.
  const char *f_path = drv_ffs_conv_fname(drv_ffs_mount, path);
  if (!f_path) { r.err_no = EINVAL; return r; }
  
  // Allocate a directory information structure for FatFS.
  FATFS_DIR *dp = (FATFS_DIR*)malloc(sizeof(FATFS_DIR));
  if (!dp) {
    if (f_path != path) free((char*)f_path);
    r.err_no = ENOMEM;
    return r;
  }
  
  // Forward to FatFS.
  r.err_no = drv_ffs_conv_errno(drv_ffs_f_opendir(dp, f_path));
  
  // Free the filename buffer.
  if (f_path != path) free((char*)f_path);
  
  // Check for errors.
  if (r.err_no) {
    free(dp);
    return r;
  }
  
  // Save the structure as the driver reference.
  r.drv_ref = (void*)dp;
  return r;
}

/**
 * Driver readdir callback.
 */
static int drv_ffs_readdir_r(void *drv_ffs_mount, void *drv_dir, sys_dirent_t *entry, sys_dirent_t **result) {
  
  // Initialize the "result" return value to NULL.
  *result = NULL;
  
  // Check arguments.
  if (!drv_dir) return EBADF;
  if (!entry || !result) return EINVAL;
  FATFS_DIR *dp = (FATFS_DIR*)drv_dir;
  
  // Perform the readdir.
  FATFS_FILINFO info;
  int res = drv_ffs_conv_errno(drv_ffs_f_readdir(dp, &info));
  if (res) return res;
  
  // Handle EOF.
  if (!info.fname[0]) return 0;
  
  // Convert the FILINFO structure to sys_dirent_t.
  entry->d_ino = 0;
  entry->d_size = (size_t)info.fsize;
  entry->d_mode = (info.fattrib & AM_RDO) ? 0444 : 0666;
  
  // Convert file/directory.
  if (info.fattrib & AM_DIR) {
    
    // This is a directory.
    entry->d_type = DT_DIR;
    entry->d_nlinks = 2;
    
  } else {
    
    // This is a regular file.
    entry->d_type = DT_REG;
    entry->d_nlinks = 1;
    
  }
  
  // Filename.
  strncpy(entry->d_name, info.fname, 255);
  entry->d_name[255] = 0;
  
  // Success.
  *result = entry;
  return 0;
}

/**
 * Driver closedir callback.
 */
static int drv_ffs_closedir(void *drv_ffs_mount, void *drv_dir) {
  
  // Check arguments.
  if (!drv_dir) return EBADF;
  FATFS_DIR *dp = (FATFS_DIR*)drv_dir;
  
  // Forward to FatFS.
  int res = drv_ffs_conv_errno(drv_ffs_f_closedir(dp));
  if (res) return res;
  
  // Free the file structure.
  free(dp);
  
  // Success.
  return 0;
}

/**
 * Driver seekdir callback.
 */
static rootfs_seekdir_ret_t drv_ffs_seekdir(void *drv_ffs_mount, void *drv_dir, off_t offs) {
  rootfs_seekdir_ret_t r = {};
  
  // Check arguments.
  if (!drv_dir) { r.err_no = EBADF; return r; }
  FATFS_DIR *dp = (FATFS_DIR*)drv_dir;
  
  // Handle rewinding.
  if (offs == 0) {
    r.err_no = drv_ffs_conv_errno(drv_ffs_f_readdir(dp, NULL));
    return r;
  }
  
  // Only rewinding is supported by FatFS.
  r.pos = -1;
  r.err_no = EINVAL;
  return r;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(fatfs) {
  .name      = DRIVER_NAME,
  .d_type    = DT_REG,
  .mount     = drv_ffs_mount,
  .umount    = drv_ffs_umount,
  .open      = drv_ffs_open,
  .read      = drv_ffs_read,
  .write     = drv_ffs_write,
  .close     = drv_ffs_close,
  .lseek     = drv_ffs_lseek,
  .stat      = drv_ffs_stat,
  .ioctl     = drv_ffs_ioctl,
  .unlink    = drv_ffs_unlink,
  .rename    = drv_ffs_rename,
  .mkdir     = drv_ffs_mkdir,
  .opendir   = drv_ffs_opendir,
  .readdir_r = drv_ffs_readdir_r,
  .closedir  = drv_ffs_closedir,
  .seekdir   = drv_ffs_seekdir
};

/**
 * Mount function for this driver. This is pretty much just "mount".
 */
int mount_fatfs(const char *source, const char *target, int readonly) {
  return mount(source, target, DRIVER_NAME, 0, (void*)readonly);
}

