/**
 * Driver for mounting blocks of memory to a file directly. This isn't really
 * a driver though, since libfs can already do this built-in. So we just supply
 * the mount function and that's it.
 */

#include <rootfs.h>
#include <sys/mount.h>

/**
 * Creates a memory-mapped block file.
 */
int mount_block(const char *path, void *start, size_t size) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Try to do the mount.
  mount_block_arg_t arg = {.start = start, .size = size};
  return mount(NULL, path, "block", 0, (const void*)&arg);
}

