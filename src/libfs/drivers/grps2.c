
/**
 * Driver for APBPS2 from grlib.
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <machine/rvex.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <input.h>
#include <irq.h>
#include <grps2.h>
#include <sys/mount.h>

#define DRIVER_NAME "grps2"

/**
 * Declaration of the gettimeofday syscall implementation, which we call
 * directly from the interrupt handlers.
 */
rvex_sysreturn_t sys_gettimeofday(struct timeval *ptimeval, void *ptimezone);

/**
 * PS2 peripheral structure.
 */
typedef struct {
  unsigned int data;
  unsigned int stat;
  unsigned int ctrl;
  unsigned int timer;
} apbps2_t;

// Status masks.
#define STAT_DR   0x00000001
#define STAT_PE   0x00000002
#define STAT_FE   0x00000004
#define STAT_KI   0x00000008
#define STAT_RF   0x00000010
#define STAT_TF   0x00000020
#define STAT_TCNT 0x07C00000
#define STAT_RCNT 0xF8000000

// Control masks.
#define CTRL_RE   0x00000001
#define CTRL_TE   0x00000002
#define CTRL_RI   0x00000004
#define CTRL_TI   0x00000008

/**
 * Reads a single byte from a PS/2 device, with the given timeout in PS/2
 * clock cycles. Returns the received byte on success or -1 on failure.
 */
static int drv_ps2_dev_read(volatile apbps2_t *dev, int timeout) {
  timeout = CR_CNT + dev->timer * timeout;
  while (!(dev->stat & STAT_DR)) if ((int)(CR_CNT - timeout) > 0) return -1;
  return dev->data & 0xFF;
}

/**
 * Writes a command buffer to a PS/2 device, using a timeout of 500 clock
 * cycles for transmitting the whole command. Returns the number of transmitted
 * bytes on success or -1 on failure.
 */
static int drv_ps2_dev_write(volatile apbps2_t *dev, char *buf, int count) {
  int timeout = CR_CNT + dev->timer * 500; // 50ms timeout
  int remain = count;
  while (remain) {
    while (dev->stat & STAT_TF) if ((int)(CR_CNT - timeout) > 0) return -1;
    dev->data = *buf++;
    remain--;
  }
  while (dev->stat & STAT_TCNT) if ((int)(CR_CNT - timeout) > 0) return -1;
  return count;
}

/**
 * Sends a PS/2 command followed by a single-byte read. If the read value is
 * 0xFE, the entire command is retransmitted (this is incorrect for commands
 * longer than 1 byte I think, but all commands used here are 1 byte long
 * anyway so sue me) up to 3 times. The byte last received is returned, or -1
 * is returned on error or if no response was received.
 */
static int drv_ps2_dev_cmd(volatile apbps2_t *dev, char *buf, int count, int timeout) {
  int retries = 3;
  while (1) {
    if (drv_ps2_dev_write(dev, buf, 1) < 0) return -1;
    int x = drv_ps2_dev_read(dev, timeout);
    if (x < 0) return -1;
    if (x == 0xFE && retries) {
      retries--;
      int delay = CR_CNT + dev->timer * 10;
      while ((int)(delay - CR_CNT) > 0);
      continue;
    }
    return x;
  }
}

/**
 * Number of events stored in the event FIFO.
 */
#define EBUFSIZE 256

/**
 * PS/2 type definitions.
 */
#define PS2_NONE  0
#define PS2_KEYBD 1
#define PS2_MOUSE 2

/**
 * PS/2 status record, belonging to a mount.
 */
typedef struct ps2_mount_ref_t {
  
  // Pointer to the peripheral.
  volatile apbps2_t *dev;
  
  // Interrupt number.
  int irq;
  
  // Event buffer write position. This loops constantly without regard for
  // whether data is actually read by the file descriptors, since multiple
  // file descriptors can be open at a time, and if they don't read in time
  // we're screwed anyway.
  volatile short ebufpos;
  
  // Event buffer. Interrupt handlers write to this at position ebufpos when
  // they post an event, followed by a wrapping increment of ebufpos.
  volatile struct input_event ebuf[EBUFSIZE];
  
  // Device type.
  short type;
  
  // State information depending on device.
  union {
    
    // Keyboard state information.
    struct {
      
      // Indicates that the next byte is an extended code.
      unsigned char keybd_ext;
      
      // Indicates that the next byte means key up instead of key down.
      unsigned char keybd_up;
      
      // Whether shift is currently pressed.
      unsigned char keybd_shift;
      
      // Each bit in this record corresponds to a linux key code. The bit is
      // set when the key is down. This is used to distinguish between a
      // key-down and typematic event (because a normal /dev/input/event*
      // distinguishes between those for some reason). The bit within the byte
      // is the minor index, the array index is major.
      unsigned char keybd_dn[128/8];
      
    };
    
    // Mouse state information.
    struct {
      
      // Mouse buffer position, i.e. the number of bytes already received for
      // the current packet.
      unsigned char mouse_bufpos;
      
      // The first two bytes of the 3-byte update packet.
      unsigned char mouse_buf[2];
      
      // Previous button state.
      unsigned char mouse_btns;
      
    };
  };
  
} ps2_mount_ref_t;

/**
 * PS/2 object reference.
 */
typedef struct ps2_object_ref_t {
  
  // The next index within ebuf that should be returned to the user.
  short ebufpos;
  
} ps2_object_ref_t;

/**
 * Mapping from keyboard scan code to Linux key code. Extended key codes should
 * be or'd with 0x80 when indexing the table.
 */
static const unsigned char drv_ps2_scan2key[256] = {
  0,
  /* 0x01 */ KEY_F9               /* =  67 */,
  0,
  /* 0x03 */ KEY_F5               /* =  63 */,
  /* 0x04 */ KEY_F3               /* =  61 */,
  /* 0x05 */ KEY_F1               /* =  59 */,
  /* 0x06 */ KEY_F2               /* =  60 */,
  /* 0x07 */ KEY_F12              /* =  88 */,
  0,
  /* 0x09 */ KEY_F10              /* =  68 */,
  /* 0x0A */ KEY_F8               /* =  66 */,
  /* 0x0B */ KEY_F6               /* =  64 */,
  /* 0x0C */ KEY_F4               /* =  62 */,
  /* 0x0D */ KEY_TAB              /* =  15 */,
  /* 0x0E */ KEY_GRAVE            /* =  41 */,
  0, 0,
  /* 0x11 */ KEY_LEFTALT          /* =  56 */,
  /* 0x12 */ KEY_LEFTSHIFT        /* =  42 */,
  0,
  /* 0x14 */ KEY_RIGHTCTRL        /* =  97 */,
  /* 0x15 */ KEY_Q                /* =  16 */,
  /* 0x16 */ KEY_1                /* =   2 */,
  0, 0, 0,
  /* 0x1A */ KEY_Z                /* =  44 */,
  /* 0x1B */ KEY_S                /* =  31 */,
  /* 0x1C */ KEY_A                /* =  30 */,
  /* 0x1D */ KEY_W                /* =  17 */,
  /* 0x1E */ KEY_2                /* =   3 */,
  0, 0,
  /* 0x21 */ KEY_C                /* =  46 */,
  /* 0x22 */ KEY_X                /* =  45 */,
  /* 0x23 */ KEY_D                /* =  32 */,
  /* 0x24 */ KEY_E                /* =  18 */,
  /* 0x25 */ KEY_4                /* =   5 */,
  /* 0x26 */ KEY_3                /* =   4 */,
  0, 0,
  /* 0x29 */ KEY_SPACE            /* =  57 */,
  /* 0x2A */ KEY_V                /* =  47 */,
  /* 0x2B */ KEY_F                /* =  33 */,
  /* 0x2C */ KEY_T                /* =  20 */,
  /* 0x2D */ KEY_R                /* =  19 */,
  /* 0x2E */ KEY_5                /* =   6 */,
  0, 0,
  /* 0x31 */ KEY_N                /* =  49 */,
  /* 0x32 */ KEY_B                /* =  48 */,
  /* 0x33 */ KEY_H                /* =  35 */,
  /* 0x34 */ KEY_G                /* =  34 */,
  /* 0x35 */ KEY_Y                /* =  21 */,
  /* 0x36 */ KEY_6                /* =   7 */,
  0, 0, 0,
  /* 0x3A */ KEY_M                /* =  50 */,
  /* 0x3B */ KEY_J                /* =  36 */,
  /* 0x3C */ KEY_U                /* =  22 */,
  /* 0x3D */ KEY_7                /* =   8 */,
  /* 0x3E */ KEY_8                /* =   9 */,
  0, 0,
  /* 0x41 */ KEY_COMMA            /* =  51 */,
  /* 0x42 */ KEY_K                /* =  37 */,
  /* 0x43 */ KEY_I                /* =  23 */,
  /* 0x44 */ KEY_O                /* =  24 */,
  /* 0x45 */ KEY_0                /* =  11 */,
  /* 0x46 */ KEY_9                /* =  10 */,
  0, 0,
  /* 0x49 */ KEY_DOT              /* =  52 */,
  /* 0x4A */ KEY_SLASH            /* =  53 */,
  /* 0x4B */ KEY_L                /* =  38 */,
  /* 0x4C */ KEY_SEMICOLON        /* =  39 */,
  /* 0x4D */ KEY_P                /* =  25 */,
  /* 0x4E */ KEY_MINUS            /* =  12 */,
  0, 0, 0,
  /* 0x52 */ KEY_APOSTROPHE       /* =  40 */,
  0,
  /* 0x54 */ KEY_LEFTBRACE        /* =  26 */,
  /* 0x55 */ KEY_EQUAL            /* =  13 */,
  0, 0,
  /* 0x58 */ KEY_CAPSLOCK         /* =  58 */,
  /* 0x59 */ KEY_RIGHTSHIFT       /* =  54 */,
  /* 0x5A */ KEY_ENTER            /* =  28 */,
  /* 0x5B */ KEY_RIGHTBRACE       /* =  27 */,
  0,
  /* 0x5D */ KEY_BACKSLASH        /* =  43 */,
  0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x66 */ KEY_BACKSPACE        /* =  14 */,
  0, 0,
  /* 0x69 */ KEY_KP1              /* =  79 */,
  0,
  /* 0x6B */ KEY_KP4              /* =  75 */,
  /* 0x6C */ KEY_KP7              /* =  71 */,
  0, 0, 0,
  /* 0x70 */ KEY_KP0              /* =  82 */,
  /* 0x71 */ KEY_KPDOT            /* =  83 */,
  /* 0x72 */ KEY_KP2              /* =  80 */,
  /* 0x73 */ KEY_KP5              /* =  76 */,
  /* 0x74 */ KEY_KP6              /* =  77 */,
  /* 0x75 */ KEY_KP8              /* =  72 */,
  /* 0x76 */ KEY_ESC              /* =   1 */,
  /* 0x77 */ KEY_NUMLOCK          /* =  69 */,
  /* 0x78 */ KEY_F11              /* =  87 */,
  /* 0x79 */ KEY_KPPLUS           /* =  78 */,
  /* 0x7A */ KEY_KP3              /* =  81 */,
  /* 0x7B */ KEY_KPMINUS          /* =  74 */,
  /* 0x7C */ KEY_KPASTERISK       /* =  55 */,
  /* 0x7D */ KEY_KP9              /* =  73 */,
  /* 0x7E */ KEY_SCROLLLOCK       /* =  70 */,
  0, 0, 0, 0,
  /* 0x83 */ KEY_F7               /* =  65 */,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x91 */ KEY_RIGHTALT         /* = 100 */,
  0, 0,
  /* 0x94 */ KEY_RIGHTCTRL        /* =  97 */,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0x9F */ KEY_LEFTMETA         /* = 125 */,
  0, 0, 0, 0, 0, 0, 0,
  /* 0xA7 */ KEY_RIGHTMETA        /* = 126 */,
  0, 0, 0, 0, 0, 0, 0,
  /* 0xAF */ KEY_COMPOSE          /* = 127 */,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xCA */ KEY_KPSLASH          /* =  98 */,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xDA */ KEY_KPENTER          /* =  96 */,
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  /* 0xE9 */ KEY_END              /* = 107 */,
  0,
  /* 0xEB */ KEY_LEFT             /* = 105 */,
  /* 0xEC */ KEY_HOME             /* = 102 */,
  0, 0, 0,
  /* 0xF0 */ KEY_INSERT           /* = 110 */,
  /* 0xF1 */ KEY_DELETE           /* = 111 */,
  /* 0xF2 */ KEY_DOWN             /* = 108 */,
  0,
  /* 0xF4 */ KEY_RIGHT            /* = 106 */,
  /* 0xF5 */ KEY_UP               /* = 103 */,
  0,
  /* 0xF7 */ KEY_PAUSE            /* = 119 */,
  0, 0,
  /* 0xFA */ KEY_PAGEDOWN         /* = 109 */,
  0,
  /* 0xFC */ KEY_SYSRQ            /* =  99 */,
  /* 0xFD */ KEY_PAGEUP           /* = 104 */,
};

/**
 * Mapping from Linux key code to ASCII. The first set is without shift, the
 * second is with shift. They are interleaved for readability (since it doesn't
 * matter jack for decoding performance).
 */
static const char drv_ps2_key2ascii[256] = {
  /* 0 = KEY_RESERVED           */ 0,    0,
  /* 1 = KEY_ESC                */ 0,    0,
  /* 2 = KEY_1                  */ '1',  '!',
  /* 3 = KEY_2                  */ '2',  '@',
  /* 4 = KEY_3                  */ '3',  '#',
  /* 5 = KEY_4                  */ '4',  '$',
  /* 6 = KEY_5                  */ '5',  '%',
  /* 7 = KEY_6                  */ '6',  '^',
  /* 8 = KEY_7                  */ '7',  '&',
  /* 9 = KEY_8                  */ '8',  '*',
  /* 10 = KEY_9                 */ '9',  '(',
  /* 11 = KEY_0                 */ '0',  ')',
  /* 12 = KEY_MINUS             */ '-',  '_',
  /* 13 = KEY_EQUAL             */ '=',  '+',
  /* 14 = KEY_BACKSPACE         */ 0,    0,
  /* 15 = KEY_TAB               */ '\t', 0,
  /* 16 = KEY_Q                 */ 'q',  'Q',
  /* 17 = KEY_W                 */ 'w',  'W',
  /* 18 = KEY_E                 */ 'e',  'E',
  /* 19 = KEY_R                 */ 'r',  'R',
  /* 20 = KEY_T                 */ 't',  'T',
  /* 21 = KEY_Y                 */ 'y',  'Y',
  /* 22 = KEY_U                 */ 'u',  'U',
  /* 23 = KEY_I                 */ 'i',  'I',
  /* 24 = KEY_O                 */ 'o',  'O',
  /* 25 = KEY_P                 */ 'p',  'P',
  /* 26 = KEY_LEFTBRACE         */ '[',  '{',
  /* 27 = KEY_RIGHTBRACE        */ ']',  '}',
  /* 28 = KEY_ENTER             */ '\n', '\n',
  /* 29 = KEY_LEFTCTRL          */ 0,    0,
  /* 30 = KEY_A                 */ 'a',  'A',
  /* 31 = KEY_S                 */ 's',  'S',
  /* 32 = KEY_D                 */ 'd',  'D',
  /* 33 = KEY_F                 */ 'f',  'F',
  /* 34 = KEY_G                 */ 'g',  'G',
  /* 35 = KEY_H                 */ 'h',  'H',
  /* 36 = KEY_J                 */ 'j',  'J',
  /* 37 = KEY_K                 */ 'k',  'K',
  /* 38 = KEY_L                 */ 'l',  'L',
  /* 39 = KEY_SEMICOLON         */ ';',  ':',
  /* 40 = KEY_APOSTROPHE        */ '\'', '"',
  /* 41 = KEY_GRAVE             */ '`',  '~',
  /* 42 = KEY_LEFTSHIFT         */ 0,    0,
  /* 43 = KEY_BACKSLASH         */ 0,    0,
  /* 44 = KEY_Z                 */ 'z',  'Z',
  /* 45 = KEY_X                 */ 'x',  'X',
  /* 46 = KEY_C                 */ 'c',  'C',
  /* 47 = KEY_V                 */ 'v',  'V',
  /* 48 = KEY_B                 */ 'b',  'B',
  /* 49 = KEY_N                 */ 'n',  'N',
  /* 50 = KEY_M                 */ 'm',  'M',
  /* 51 = KEY_COMMA             */ ',',  '<',
  /* 52 = KEY_DOT               */ '.',  '>',
  /* 53 = KEY_SLASH             */ '/',  '?',
  /* 54 = KEY_RIGHTSHIFT        */ 0,    0,
  /* 55 = KEY_KPASTERISK        */ '*',  '*',
  /* 56 = KEY_LEFTALT           */ 0,    0,
  /* 57 = KEY_SPACE             */ ' ',  ' ',
  /* 58 = KEY_CAPSLOCK          */ 0,    0,
  /* 59 = KEY_F1                */ 0,    0,
  /* 60 = KEY_F2                */ 0,    0,
  /* 61 = KEY_F3                */ 0,    0,
  /* 62 = KEY_F4                */ 0,    0,
  /* 63 = KEY_F5                */ 0,    0,
  /* 64 = KEY_F6                */ 0,    0,
  /* 65 = KEY_F7                */ 0,    0,
  /* 66 = KEY_F8                */ 0,    0,
  /* 67 = KEY_F9                */ 0,    0,
  /* 68 = KEY_F10               */ 0,    0,
  /* 69 = KEY_NUMLOCK           */ 0,    0,
  /* 70 = KEY_SCROLLLOCK        */ 0,    0,
  /* 71 = KEY_KP7               */ '7',  '7',
  /* 72 = KEY_KP8               */ '8',  '8',
  /* 73 = KEY_KP9               */ '9',  '9',
  /* 74 = KEY_KPMINUS           */ '-',  '-',
  /* 75 = KEY_KP4               */ '4',  '4',
  /* 76 = KEY_KP5               */ '5',  '5',
  /* 77 = KEY_KP6               */ '6',  '6',
  /* 78 = KEY_KPPLUS            */ '+',  '+',
  /* 79 = KEY_KP1               */ '1',  '1',
  /* 80 = KEY_KP2               */ '2',  '2',
  /* 81 = KEY_KP3               */ '3',  '3',
  /* 82 = KEY_KP0               */ '0',  '0',
  /* 83 = KEY_KPDOT             */ '.',  '.',
  /* 84 = undefined             */ 0,    0,
  /* 85 = KEY_ZENKAKUHANKAKU    */ 0,    0,
  /* 86 = KEY_102ND             */ 0,    0,
  /* 87 = KEY_F11               */ 0,    0,
  /* 88 = KEY_F12               */ 0,    0,
  /* 89 = KEY_RO                */ 0,    0,
  /* 90 = KEY_KATAKANA          */ 0,    0,
  /* 91 = KEY_HIRAGANA          */ 0,    0,
  /* 92 = KEY_HENKAN            */ 0,    0,
  /* 93 = KEY_KATAKANAHIRAGANA  */ 0,    0,
  /* 94 = KEY_MUHENKAN          */ 0,    0,
  /* 95 = KEY_KPJPCOMMA         */ 0,    0,
  /* 96 = KEY_KPENTER           */ '\n', '\n',
  /* 97 = KEY_RIGHTCTRL         */ 0,    0,
  /* 98 = KEY_KPSLASH           */ '/',  '/',
  /* 99 = KEY_SYSRQ             */ 0,    0,
  /* 100 = KEY_RIGHTALT         */ 0,    0,
  /* 101 = KEY_LINEFEED         */ '\n', '\n',
  /* 102 = KEY_HOME             */ 0,    0,
  /* 103 = KEY_UP               */ 0,    0,
  /* 104 = KEY_PAGEUP           */ 0,    0,
  /* 105 = KEY_LEFT             */ 0,    0,
  /* 106 = KEY_RIGHT            */ 0,    0,
  /* 107 = KEY_END              */ 0,    0,
  /* 108 = KEY_DOWN             */ 0,    0,
  /* 109 = KEY_PAGEDOWN         */ 0,    0,
  /* 110 = KEY_INSERT           */ 0,    0,
  /* 111 = KEY_DELETE           */ 0,    0,
  /* 112 = KEY_MACRO            */ 0,    0,
  /* 113 = KEY_MUTE             */ 0,    0,
  /* 114 = KEY_VOLUMEDOWN       */ 0,    0,
  /* 115 = KEY_VOLUMEUP         */ 0,    0,
  /* 116 = KEY_POWER            */ 0,    0,
  /* 117 = KEY_KPEQUAL          */ '=',  '=',
  /* 118 = KEY_KPPLUSMINUS      */ '+',  '-',
  /* 119 = KEY_PAUSE            */ 0,    0,
  /* 120 = KEY_SCALE            */ 0,    0,
  /* 121 = KEY_KPCOMMA          */ ',',  ',',
  /* 122 = KEY_HANGEUL          */ 0,    0,
  /* 123 = KEY_HANJA            */ 0,    0,
  /* 124 = KEY_YEN              */ 0,    0,
  /* 125 = KEY_LEFTMETA         */ 0,    0,
  /* 126 = KEY_RIGHTMETA        */ 0,    0,
  /* 127 = KEY_COMPOSE          */ 0,    0
};

/**
 * Posts an event to the event buffer.
 */
static void drv_ps2_post(ps2_mount_ref_t *ps2, struct input_event ev) {
  int pos = ps2->ebufpos;
  ps2->ebuf[pos] = ev;
  pos += 1;
  pos &= EBUFSIZE - 1;
  ps2->ebufpos = pos;
}

/**
 * Mouse interrupt handler.
 */
static void ps2_mouse_irq(unsigned long arg) {
  ps2_mount_ref_t *ps2 = (ps2_mount_ref_t*)arg;
  
  // Read until we're out of data.
  while (ps2->dev->stat & STAT_DR) {
    unsigned char frame = (unsigned char)ps2->dev->data;
    
    // If this is supposed to be the first byte in the packet but bit 3 is not
    // set, we're not synced properly. Ignore this byte in that case.
    if (!ps2->mouse_bufpos && !(frame & 0x08)) continue;
    
    // If this isn't the last byte of a packet, simply save it in the buffer
    // and continue. We'll parse the packet when we have everything.
    if (ps2->mouse_bufpos < 2) {
      ps2->mouse_buf[ps2->mouse_bufpos] = frame;
      ps2->mouse_bufpos++;
      continue;
    }
    
    // Load the packet.
    unsigned char ph = ps2->mouse_buf[0];
    unsigned char px = ps2->mouse_buf[1];
    unsigned char py = frame;
    
    // Load the current time for the timestamp field.
    struct input_event ev;
    sys_gettimeofday(&(ev.time), NULL);
    
    // Handle X delta.
    ev.value = px;
    if (ph & 0x10) ev.value -= 256;
    if (ev.value) {
      ev.type = EV_REL;
      ev.code = REL_X;
      drv_ps2_post(ps2, ev);
    }
    
    // Handle Y delta.
    ev.value = py;
    if (ph & 0x20) ev.value -= 256;
    if (ev.value) {
      ev.type = EV_REL;
      ev.code = REL_Y;
      drv_ps2_post(ps2, ev);
    }
    
    // Handle buttons.
    ph &= 0x07;
    unsigned char x = ph ^ ps2->mouse_btns;
    if (x) {
      ev.type = EV_KEY;
      int i;
      for (i = 0; i < 3; i++) {
        if (x & (1 << i)) {
          ev.code = BTN_LEFT + i;
          ev.value = (ph & (1 << i)) ? 1 : 0;
          drv_ps2_post(ps2, ev);
        }
      }
      ps2->mouse_btns = ph;
    }
    
    // Reset the buffer position to prepare for the next packet.
    ps2->mouse_bufpos = 0;
    
  }
  
}

/**
 * Keyboard interrupt handler.
 */
static void ps2_keybd_irq(unsigned long arg) {
  ps2_mount_ref_t *ps2 = (ps2_mount_ref_t*)arg;
  
  // Read until we're out of data.
  while (ps2->dev->stat & STAT_DR) {
    unsigned char frame = (unsigned char)ps2->dev->data;
    
    // Handle extended flag.
    if (frame == 0xE0) {
      ps2->keybd_ext = 1;
      continue;
    }
    
    // Handle break (key up instead of down) flag.
    if (frame == 0xF0) {
      ps2->keybd_up = 1;
      continue;
    }
    
    // Initialize the event structure.
    struct input_event ev;
    sys_gettimeofday(&(ev.time), NULL);
    ev.type = EV_KEY;
    
    // Determine the key code. If no key code is known for this scan code,
    // ignore it. Otherwise we'll get spurious events for the weird keys that
    // have compound and 0xE1 scancodes.
    ev.code = frame;
    if (ps2->keybd_ext) ev.code |= 0x80;
    ev.code = drv_ps2_scan2key[ev.code];
    if (ev.code) {
      
      // Update the key state table, and while we do so, determine if the key was
      // already down to detect typematic events.
      unsigned char msk = 1 << (ev.code & 7);
      int idx = ev.code >> 3;
      unsigned char old = ps2->keybd_dn[idx];
      unsigned char new = old;
      if (ps2->keybd_up) {
        ev.value = 0; // Up.
        new &= ~msk;
      } else if (old & msk) {
        ev.value = 2; // Typematic.
      } else {
        ev.value = 1; // Down.
        new |= msk;
      }
      ps2->keybd_dn[idx] = new;
      
      // Post the event.
      drv_ps2_post(ps2, ev);
      
      // Update keybd_shift state.
      if (ev.code == KEY_LEFTSHIFT || ev.code == KEY_RIGHTSHIFT) {
        ps2->keybd_shift = !ps2->keybd_up;
      }
      
      // If this is not a release event, see if this key has an ASCII
      // equivalent for the EV_CHR event.
      if (ev.value) {
        
        // Translate to ASCII.
        ev.code = drv_ps2_key2ascii[(ev.code << 1) | !!ps2->keybd_shift];
        if (ev.code) {
          
          // Post the EV_CHR event.
          ev.type = EV_CHR;
          ev.value = 0;
          drv_ps2_post(ps2, ev);
          
        }
        
      }
      
    }
    
    // Reset flags for the next event.
    ps2->keybd_ext = 0;
    ps2->keybd_up = 0;
    
  }
  
}

/**
 * Driver mount callback.
 */
static rootfs_mount_ret_t drv_ps2_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  char buf[1];
  int x;
  
  // Check arguments.
  if (isdir) { r.err_no = EISDIR; return r; }
  if (src || !arg) { r.err_no = EINVAL; return r; }
  mount_ps2_arg_t *ps2_arg = (mount_ps2_arg_t*)arg;
  
  // Allocate the state structure.
  ps2_mount_ref_t *ps2 = (ps2_mount_ref_t*)calloc(1, sizeof(ps2_mount_ref_t));
  if (!ps2) { r.err_no = ENOMEM; return r; }
  
  // Save the address and IRQ.
  ps2->dev = (volatile apbps2_t*)ps2_arg->address;
  ps2->irq = ps2_arg->irq;
  
  // Initialize the peripheral.
  ps2->dev->ctrl = CTRL_RE | CTRL_TE;
  
  // Disable scanning.
  buf[0] = 0xF5;
  drv_ps2_dev_cmd(ps2->dev, buf, 1, 2000);
  
  // Flush input FIFO.
  while (ps2->dev->stat & STAT_DR) {
    volatile int __attribute__((unused)) dummy = ps2->dev->data;
  }
  
  // Send reset command. If we don't get a response for this, no device is
  // connected to the port and we should fail mounting
  buf[0] = 0xFF;
  x = drv_ps2_dev_cmd(ps2->dev, buf, 1, 10000);
  if (x == -1) { r.err_no = EIO; return r; }
  
  // Set mouse to stream mode. Keyboards don't have this command and will thus
  // not respond with ACK.
  buf[0] = 0xEA;
  x = drv_ps2_dev_cmd(ps2->dev, buf, 1, 10000);
  if (x == 0xFA) {
    ps2->type = PS2_MOUSE;
  } else {
    ps2->type = PS2_KEYBD;
  }
  
  // Enable scanning.
  buf[0] = 0xF4;
  drv_ps2_dev_cmd(ps2->dev, buf, 1, 2000);
  
  // Enable and set up the interrupt.
  irq_register(ps2->irq, (ps2->type == PS2_MOUSE) ? ps2_mouse_irq : ps2_keybd_irq, (unsigned long)ps2);
  irq_clear(ps2->irq);
  irq_enable(ps2->irq, 1);
  ps2->dev->ctrl = CTRL_RE | CTRL_TE | CTRL_RI;
  
  // Save the state structure as mount reference.
  r.drv_ref = (void*)ps2;
  
  // Success.
  return r;
}

/**
 * Driver umount callback.
 */
static int drv_ps2_umount(void *mount_ref) {
  
  // Check arguments.
  if (!mount_ref) return EINVAL;
  ps2_mount_ref_t *ps2 = (ps2_mount_ref_t*)mount_ref;
  
  // Disable the peripheral.
  ps2->dev->ctrl = 0;
  
  // Disable interrupts.
  irq_enable(ps2->irq, 0);
  irq_clear(ps2->irq);
  
  // Unregister the interrupt handler.
  irq_register(ps2->irq, NULL, 0);
  
  // Free the data structure.
  free(ps2);
  
  // Success.
  return 0;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_ps2_open(void *mount_ref, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref) { r.err_no = EINVAL; return r; }
  ps2_mount_ref_t *ps2 = (ps2_mount_ref_t*)mount_ref;
  
  // Create a object reference structure.
  ps2_object_ref_t *ref = (ps2_object_ref_t*)calloc(1, sizeof(ps2_object_ref_t));
  if (!ref) { r.err_no = ENOMEM; return r; }
  
  // Initialize the object reference.
  ref->ebufpos = ps2->ebufpos;
  
  // Success.
  r.drv_ref = (void*)ref;
  return r;
}

/**
 * Driver read implementation.
 */
static rootfs_read_ret_t drv_ps2_read(void *mount_ref, void *file_ref, void *buf, size_t cnt) {
  rootfs_read_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  ps2_mount_ref_t *ps2 = (ps2_mount_ref_t*)mount_ref;
  ps2_object_ref_t *ref = (ps2_object_ref_t*)file_ref;
  
  // Read until we run out of buffer space.
  r.amount = 0;
  while (r.amount + sizeof(struct input_event) <= cnt) {
    struct input_event *ev = (struct input_event*)buf;
    
    // Return if no event is pending.
    if (ref->ebufpos == ps2->ebufpos) {
      if (r.amount == 0) r.err_no = EWOULDBLOCK;
      return r;
    }
    
    // Copy the next record.
    int pos = ref->ebufpos;
    *ev = ps2->ebuf[pos];
    pos += 1;
    pos &= EBUFSIZE - 1;
    ref->ebufpos = pos;
    
    // Prepare for next record.
    r.amount += sizeof(struct input_event);
    buf += sizeof(struct input_event);
  }
  
  // Success.
  return r;
}

/**
 * Driver close callback.
 */
static int drv_ps2_close(void *mount_ref, void *file_ref) {
  
  // Free the file structure.
  free(file_ref);
  
  // Success.
  return 0;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(grps2) {
  .name   = DRIVER_NAME,
  .d_type = DT_CHR,
  .mount  = drv_ps2_mount,
  .umount = drv_ps2_umount,
  .open   = drv_ps2_open,
  .read   = drv_ps2_read,
  .close  = drv_ps2_close
};

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created. irq must be the interrupt number associated
 * with the peripheral, as passed to irq_register(), irq_enable(), and
 * irq_clear(), which must be available through some other library. While we're
 * listing requirements, sys_gettimeofday() is also used.
 */
int mount_grps2(const char *path, long address, int irq) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Try to do the mount.
  mount_ps2_arg_t arg;
  arg.address = address;
  arg.irq = irq;
  return mount(NULL, path, DRIVER_NAME, 0, (const void*)&arg);
}

