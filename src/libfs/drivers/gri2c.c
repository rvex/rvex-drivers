/**
 * Driver for GRI2C.
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <gri2c.h>
#include <sys/mount.h>

#define DRIVER_NAME "gri2c"

// I2C master peripheral structure.
typedef struct i2cmst_t {
  unsigned int prescale;
  unsigned int ctrl;
  unsigned int data;
  unsigned int cmdstat;
} i2cmst_t;

// Command masks.
#define STA   0x80
#define STO   0x40
#define RD    0x20
#define WR    0x10
#define ACK   0x08

// Status masks.
#define RXACK 0x80
#define TIP   0x02

// Wait for transfer complete.
#define WAIT  while (p->cmdstat & TIP)

/**
 * Starts an I2C transfer.
 */
static int drv_i2c_start(volatile i2cmst_t *p, int addr) {
  
  // Send slave address.
  p->data = addr;
  p->cmdstat = STA | WR;
  WAIT;
  
  // Read acknowledgement.
  if (p->cmdstat & RXACK) {
    // This writes a dummy byte to a non-responsive slave. Don't know what else
    // to do, the manual doesn't specify.
    p->data = 0;
    p->cmdstat = STO | WR;
    WAIT;
    return -1;
  }
  
  return 0;
}

/**
 * Writes a byte. If last is nonzero, this will be the last transfer.
 */
static int drv_i2c_write_byte(volatile i2cmst_t *p, int data, int last) {
  
  // Send data.
  p->data = data;
  if (last) {
    p->cmdstat = WR | STO;
  } else {
    p->cmdstat = WR;
  }
  WAIT;
  
  // Read acknowledgement.
  if (p->cmdstat & RXACK) {
    if (!last) {
      // This writes a dummy byte to a non-responsive slave. Don't know what else
      // to do, the manual doesn't specify.
      p->data = 0;
      p->cmdstat = STO | WR;
      WAIT;
    }
    return -1;
  }
  
  return 0;
}

/**
 * Reads a byte. If last is nonzero, this will be the last transfer.
 */
static int drv_i2c_read_byte(volatile i2cmst_t *p, int last) {
  
  // Read data.
  if (last) {
    p->cmdstat = RD | ACK | STO;
  } else {
    p->cmdstat = RD;
  }
  WAIT;
  return p->data;
  
}

/**
 * I2C object reference.
 */
typedef struct i2c_object_ref_t {
  
  // Selected address. -1 if not set yet.
  signed char addr;
  
  // Currently selected register (file pointer).
  unsigned char reg;
  
} i2c_object_ref_t;

/**
 * Driver mount callback.
 */
static rootfs_mount_ret_t drv_i2c_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Can only mount to a file.
  if (isdir) { r.err_no = EISDIR; return r; }
  
  // Source file makes no sense for this driver.
  if (src != NULL) { r.err_no = EINVAL; return r; }
  
  // Save the peripheral address.
  r.drv_ref = (void*)arg;
  
  // Configure the peripheral clock.
  volatile i2cmst_t *p = (volatile i2cmst_t*)arg;
  p->ctrl = 0x00;
  p->prescale = (unsigned int)mountflags;
  
  return r;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_i2c_open(void *mount_ref, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref) { r.err_no = EINVAL; return r; }
  
  // Create a file reference structure.
  i2c_object_ref_t *ref = (i2c_object_ref_t*)calloc(1, sizeof(i2c_object_ref_t));
  if (!ref) { r.err_no = ENOMEM; return r; }
  
  // Initialize the object.
  ref->addr = -1;
  ref->reg = 0;
  
  // Save the backing file reference as the driver reference.
  r.drv_ref = (void*)ref;
  return r;
}

/**
 * Driver read implementation.
 */
static rootfs_read_ret_t drv_i2c_read(void *mount_ref, void *file_ref, void *buf, size_t cnt) {
  rootfs_read_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  i2c_object_ref_t *ref = (i2c_object_ref_t*)file_ref;
  volatile i2cmst_t *p = (volatile i2cmst_t*)mount_ref;
  if (ref->addr < 0) { r.err_no = EINVAL; return r; }
  
  // Reset the peripheral.
  p->ctrl = 0x00;
  p->ctrl = 0x80;
  
  // Start the transfer.
  if (drv_i2c_start(p, ref->addr << 1)) return r;
  
  // Send the register address.
  if (drv_i2c_write_byte(p, ref->reg, 0)) return r;
  
  // Repeated start for the read.
  if (drv_i2c_start(p, (ref->addr << 1) | 1)) return r;
  
  // Read the data.
  char *data = (char*)buf;
  while (cnt--) {
    *data++ = (char)drv_i2c_read_byte(p, cnt==0);
    r.amount++;
    ref->reg++;
  }
  
  // Success.
  return r;
}

/**
 * Driver write implementation.
 */
static rootfs_write_ret_t drv_i2c_write(void *mount_ref, void *file_ref, const void *buf, size_t cnt) {
  rootfs_write_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  i2c_object_ref_t *ref = (i2c_object_ref_t*)file_ref;
  volatile i2cmst_t *p = (volatile i2cmst_t*)mount_ref;
  if (ref->addr < 0) { r.err_no = EINVAL; return r; }
  
  // Reset the peripheral.
  p->ctrl = 0x00;
  p->ctrl = 0x80;
  
  // Start the transfer.
  if (drv_i2c_start(p, ref->addr << 1)) return r;
  
  // Send the register address.
  if (drv_i2c_write_byte(p, ref->reg, cnt==0)) return r;
  
  // Send the data.
  const char *data = (const char*)buf;
  while (cnt--) {
    if (drv_i2c_write_byte(p, *data++, cnt==0)) return r;
    r.amount++;
    ref->reg++;
  }
  
  // Success.
  return r;
}

/**
 * Driver close callback.
 */
static int drv_i2c_close(void *mount_ref, void *file_ref) {
  
  // Free the file structure.
  free(file_ref);
  
  // Success.
  return 0;
}

/**
 * Driver lseek callback.
 */
static rootfs_lseek_ret_t drv_i2c_lseek(void *mount_ref, void *file_ref, off_t offs, int whence) {
  rootfs_lseek_ret_t r = {};
  
  // Check arguments.
  if (!file_ref) { r.err_no = EBADF; return r; }
  i2c_object_ref_t *ref = (i2c_object_ref_t*)file_ref;
  
  // Set the new offset.
  if (whence == SEEK_CUR) offs += ref->reg;
  ref->reg = (unsigned char)offs;
  
  // Success.
  r.pos = (off_t)ref->reg;
  return r;
}

/**
 * Driver stat callback.
 */
static int drv_i2c_stat(void *mount_ref, void *file_ref, struct stat *sb) {
  
  // Override the size.
  sb->st_size = 256;
  
  // Success.
  return 0;
};

/**
 * Driver ioctl callback.
 */
static rootfs_ioctl_ret_t drv_i2c_ioctl(void *mount_ref, void *file_ref, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!file_ref) { r.err_no = EBADF; return r; }
  i2c_object_ref_t *ref = (i2c_object_ref_t*)file_ref;
  
  // Check for I2C address configuration request.
  if (request == I2C_SLAVE) {
    long addr = (long)param;
    if ((addr < 0) || (addr > 127)) { r.err_no = EINVAL; return r; }
    ref->addr = (signed char)addr;
    return r;
  }
  
  // Unknown ioctl.
  r.err_no = EINVAL;
  return r;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(gri2c) {
  .name   = DRIVER_NAME,
  .d_type = DT_BLK,
  .mount  = drv_i2c_mount,
  .open   = drv_i2c_open,
  .read   = drv_i2c_read,
  .write  = drv_i2c_write,
  .close  = drv_i2c_close,
  .lseek  = drv_i2c_lseek,
  .stat   = drv_i2c_stat,
  .ioctl  = drv_i2c_ioctl
};

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created. ambaclk must be the AMBA clock frequency in
 * kHz. This also creates the file if it doesn't already exist. Most
 * importantly though, using this function to do the mount ensures that the
 * driver static library is actually loaded.
 */
int mount_gri2c(const char *path, long ambaclk, long address) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Calculate the prescaler value. This is for a 100kHz I2C clock.
  long prescale = ambaclk / 500 - 1;
  if (prescale < 0) prescale = 0;
  
  // Try to do the mount.
  return mount(NULL, path, DRIVER_NAME, prescale, (const void*)address);
}

