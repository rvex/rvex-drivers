/**
 * Driver for the r-VEX debug UART.
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <rvudev.h>
#include <sys/mount.h>

#define DRIVER_NAME "rvex-uart"

/**
 * Mounts a UART to a rootfs file.
 */
static rootfs_mount_ret_t drv_rvu_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Can only mount to a file.
  if (isdir) { r.err_no = EISDIR; return r; }
  
  // Source file makes no sense for this driver.
  if (src != NULL) { r.err_no = EINVAL; return r; }
  
  // Save the peripheral address.
  r.drv_ref = (void*)arg;
  
  return r;
}

/**
 * Driver read implementation.
 */
static rootfs_read_ret_t drv_rvu_read(void *mount_ref, void *file_ref, void *buf, size_t cnt) {
  rootfs_read_ret_t r = {};
  char *cbuf = (char*)buf;
  int remain = cnt;
  volatile rvex_uart_regs_t *uart = (volatile rvex_uart_regs_t*)mount_ref;
  
  // If there is no data ready, return EWOULDBLOCK.
  if (!(uart->stat & RVU_RXDR)) {
    r.err_no = EWOULDBLOCK;
    return r;
  }
  
  // Read whatever we have into the buffer.
  #pragma unroll(0)
  while (remain--) {
    
    // Read a byte from the buffer.
    *cbuf++ = uart->data;
    
    // Increment amount accordingly.
    r.amount++;
    
    // Stop if the buffer is now empty.
    if (!(uart->stat & RVU_RXDR)) {
      break;
    }
    
  }
  
  return r;
}

/**
 * Driver write implementation.
 */
static rootfs_write_ret_t drv_rvu_write(void *mount_ref, void *file_ref, const void *buf, size_t cnt) {
  rootfs_write_ret_t r = {};
  const char *cbuf = (const char*)buf;
  int remain = cnt;
  volatile rvex_uart_regs_t *uart = (volatile rvex_uart_regs_t*)mount_ref;
  
  // If the buffer is full, return EWOULDBLOCK.
  if (!(uart->stat & RVU_TXDR)) {
    r.err_no = EWOULDBLOCK;
    return r;
  }
  
  // Write whatever we can into the buffer.
  #pragma unroll(0)
  while (remain--) {
    
    // Write a byte.
    uart->data = *cbuf++;
    
    // Increment amount accordingly.
    r.amount++;
    
    // Stop if the buffer is now full.
    if (!(uart->stat & RVU_TXDR)) {
      break;
    }
    
  }
  
  return r;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(rvex_uart) {
  .name   = DRIVER_NAME,
  .d_type = DT_CHR,
  .mount  = drv_rvu_mount,
  .read   = drv_rvu_read,
  .write  = drv_rvu_write
};

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created.
 */
int mount_rvudev(const char *path, long address) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Try to do the mount.
  return mount(NULL, path, DRIVER_NAME, 0, (const void*)address);
}

