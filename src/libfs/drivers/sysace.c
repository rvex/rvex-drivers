/**
 * Driver for the ml605-demo system ACE compactflash card.
 */

#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <sysace.h>
#include <sys/mount.h>
#include <sys/ioctl.h>

#define ace_debug(...)
//#define ace_debug(...) fprintf(stderr, __VA_ARGS__)

#define DRIVER_NAME "sysace"

/**
 * LRU lookup table.
 */
static const int drv_ace_lru_lookup[24] = {
  00001031100, // state 0
  00001051300, // state 1
  00201031700, // state 2
  00204032100, // state 3
  00004052500, // state 4
  00204052700, // state 5
  00607031101, // state 6
  00607051301, // state 7
  01007151101, // state 8
  01012201101, // state 9
  00612231301, // state 10
  01012261301, // state 11
  01401151702, // state 12
  01404152102, // state 13
  01607151702, // state 14
  01612201702, // state 15
  01422202102, // state 16
  01624202102, // state 17
  00022232503, // state 18
  00222232703, // state 19
  00624232503, // state 20
  01024262503, // state 21
  01422262703, // state 22
  01624262703  // state 23
// \/\/\/\/\/
//  \ \ \ \ `- least recently used entry
//   \ \ \ `-- next state if entry 0 is used
//    \ \ `--- next state if entry 1 is used
//     \ `---- next state if entry 2 is used
//      `----- next state if entry 3 is used
};

/**
 * Number of lines in the cache. Must be a power of two, but can otherwise be
 * modified without breaking things.
 */
#define NUM_LINES 8

/**
 * Number of sector entries per line. Must be 4, otherwise the LRU lookup table
 * will be wrong.
 */
#define NUM_ENTS 4

/**
 * Cache "line". Each line contains 4 entries and least-recently-used
 * information.
 */
typedef struct sysace_entry_t {
  
  // Loaded sector + 1, or 0 if invalid.
  unsigned int sector;
  
  // Dirty flag (needs to be written back).
  char dirty;
  
  // Sector buffer. This is an int buffer to ensure 32-bit alignment, so we can
  // do 32-bit memcpys between sysace and cache.
  int buf[512/4];
  
} sysace_entry_t;

/**
 * Cache "line". Each line contains 4 entries and least-recently-used
 * information.
 */
typedef struct sysace_line_t {
  
  // Cache entries.
  sysace_entry_t e[NUM_ENTS];
  
  // LRU information.
  int lru;
  
} sysace_line_t;

/**
 * Driver information structure. This contains a 8 kiB cache for hopefully
 * speeding up accesses a little.
 */
typedef struct sysace_t {
  
  // sysace base address.
  void *base;
  
  // Cache.
  sysace_line_t c[NUM_LINES];
  
} sysace_t;

/**
 * Open file information record.
 */
typedef struct sysace_file_t {
  
  // Current file offset.
  off_t offs;
  
} sysace_file_t;

/**
 * Macro for setting the sysace control register.
 */
#define SYSACE_CTRL(sa, writable, page) \
  *((volatile int*)(sa->base + 0x3FFFFFFC)) \
    = 0x12355AA0            /* magic number, checked by hardware */ \
    | ((page) & 3)          /* page index */ \
    | ((writable) ? 4 : 0); /* write-enable flag */

/**
 * Reads the given sector from the card into the supplied cache entry.
 */
static int drv_ace_read_entry(sysace_t *sa, sysace_entry_t *ent, unsigned int sector) {
  
  ace_debug("ace rd sector 0x%06X\n", sector);
  
  // Figure out the bank from the sector number. A bank is 512 MiB in size,
  // a sector is 512 bytes, so we need to shift by 20 bits to get it.
  int page = sector >> 20;
  sector &= 0xFFFFF;
  SYSACE_CTRL(sa, 0, page);
  
  // Read.
  const int *src = (const int*)(sa->base + ((sector & 0xFFFFF) << 9));
  int *dest = ent->buf;
  int i;
  for (i = 0; i < 512/4; i++) *dest++ = *src++;
  
  // Reset the control register.
  SYSACE_CTRL(sa, 0, 0);
  
  // Update the tag.
  ent->sector = sector + 1;
  ent->dirty = 0;
  return 0;
}

/**
 * Write the given entry back to the card if it is dirty. The contents are
 * verified after the write, and up to 5 retries are done. If the data is still
 * invalid after the fifth write, EIO is returned. Otherwise, the dirty flag is
 * cleared and 0 is returned.
 */
static int drv_ace_write_entry(sysace_t *sa, sysace_entry_t *ent) {
  
  // Figure out the sector.
  unsigned int sector = ent->sector;
  if (sector == 0) return -1;
  sector--;
  
  ace_debug("ace wr sector 0x%06X: ", sector);
  
  // Figure out the bank from the sector number. A bank is 512 MiB in size,
  // a sector is 512 bytes, so we need to shift by 20 bits to get it. We also
  // set the write-enable flag.
  int page = sector >> 20;
  sector &= 0xFFFFF;
  SYSACE_CTRL(sa, 1, page);
  
  // Try to write up to 5 times.
  int retries;
  int *output = (int*)(sa->base + ((sector & 0xFFFFF) << 9));
  for (retries = 0; retries < 5; retries++) {
    
    // Write.
    const int *src = ent->buf;
    int *dest = output;
    int i;
    for (i = 0; i < 512/4; i++) *dest++ = *src++;
    
    // Read from the next sector to invalidate the buffer.
    volatile int dummy __attribute__((unused)) = *dest;
    
    // Verify.
    src = ent->buf;
    const int *check = output;
    for (i = 0; i < 512/4; i++) {
      if (*check++ != *src++) break;
    }
    if (i == 512/4) {
      break;
    }
    
  }
  
  // Reset the control register.
  SYSACE_CTRL(sa, 0, 0);
  
  // If we failed the fifth retry, return error.
  if (retries == 5) {
    ace_debug("fail!\n");
    return -1;
  }
  
  // Write successful, clear dirty flag.
  ace_debug("OK\n");
  ent->dirty = 0;
  return 0;
}

/**
 * This function acquires and prepares a cache entry for the given sector.
 * The returned entry is either valid for the requested sector or it is
 * invalid. NULL is returned if an IO error occurs.
 */
static sysace_entry_t *drv_ace_get_entry(sysace_t *sa, unsigned int sector) {
  
  // Get the right line.
  sysace_line_t *line = &(sa->c[sector & (NUM_LINES-1)]);
  
  // See if the requested sector is already cached. If it is, update the LRU
  // and return it.
  sysace_entry_t *ent;
  int i;
  for (i = 0; i < NUM_ENTS; i++) {
    ent = &(line->e[i]);
    if (ent->sector == sector + 1) {
      ace_debug("ace cache hit sector 0x%06X\n", sector);
      goto done;
    }
  }
  
  // Figure out the least recently used entry.
  i = drv_ace_lru_lookup[line->lru] & 3;
  ent = &(line->e[i]);
  
  // If this entry is dirty, write it back.
  if (ent->sector) {
    if (ent->dirty) {
      int res = drv_ace_write_entry(sa, ent);
      if (res) return NULL;
    }
    ace_debug("ace cache evict sector 0x%06X for 0x%06X\n", ent->sector-1, sector);
  }
  
  // Invalidate the entry.
  ent->sector = 0;
  
done:
  // Update the LRU state.
  line->lru = (drv_ace_lru_lookup[line->lru] >> ((6 * i) + 6)) & 0x3F;
  
  // Return the entry.
  return ent;
}

/**
 * Does a read drv_ace_transfer (write=0) or write drv_ace_transfer (write!=0). Returns amount
 * read/written or errno.
 */
static rootfs_read_ret_t drv_ace_transfer(sysace_t *sa, sysace_file_t *file, void *buf, size_t cnt, int write) {
  rootfs_read_ret_t r = {};
  
  // Check input.
  if (!sa || !file) { r.err_no = EBADF; return r; }
  if (!buf) { r.err_no = EINVAL; return r; }
  
  // Access sectors until we complete the request.
  size_t remain = cnt;
  while (remain) {
    
    // Figure out metrics for the next sector.
    unsigned long sector = file->offs >> 9;
    int start = file->offs & 0x1FF;
    int amount = 512 - start;
    if (amount > remain) amount = remain;
    
    // Find a suitable entry.
    sysace_entry_t *ent = drv_ace_get_entry(sa, sector);
    if (!ent) { r.err_no = EIO; break; }
    
    // If the sector is not already cached, and we either don't have a full
    // sector remaining or are reading, first read the sector from the card.
    // Otherwise (writing a full sector) we don't need to read first.
    if ((!ent->sector) && (!write || (amount < 512))) {
      r.err_no = drv_ace_read_entry(sa, ent, sector);
      if (r.err_no) break;
    }
    
    // Transfer between the supplied buffer and the cache entry.
    char *cache = ((char*)ent->buf) + start;
    if (write) {
      memcpy(cache, buf, amount);
    } else {
      memcpy(buf, cache, amount);
    }
    
    // If we're writing, set the dirty flag.
    if (write) {
      ent->dirty = 1;
    }
    
    // Bookkeeping.
    buf += amount;
    file->offs += amount;
    r.amount += amount;
    remain -= amount;
    
  }
  
  // Ignore errors if we wrote at least something.
  if (r.amount) r.err_no = 0;
  
  // Complete or partial success.
  return r;
}

/**
 * Purges dirty cache pages to the card. Returns 0 for success or errno
 * otherwise.
 */
static int drv_ace_sync(sysace_t *sa) {
  int ret = 0;
  
  // Handle the sync as well as we can. That is, write all the dirty sections,
  // and continue on error. We eventually return the first error or success.
  ace_debug("ace sync start\n");
  int l, i;
  for (l = 0; l < NUM_LINES; l++) {
    for (i = 0; i < NUM_ENTS; i++) {
      sysace_entry_t *ent = &(sa->c[l].e[i]);
      if (ent->dirty) {
        int res = drv_ace_write_entry(sa, ent);
        if (res && !ret) {
          ret = res;
        }
      }
    }
  }
  ace_debug("ace sync end: %s\n", strerror(ret));
  
  return ret;
}

/**
 * Driver mount implementation.
 */
static rootfs_mount_ret_t drv_ace_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Can only mount to a file.
  if (isdir) { r.err_no = EISDIR; return r; }
  
  // Source file makes no sense for this driver.
  if (src != NULL) { r.err_no = EINVAL; return r; }
  
  // Allocate the driver information structure.
  sysace_t *sa = (sysace_t*)malloc(sizeof(sysace_t));
  if (!sa) { r.err_no = ENOMEM; return r; }
  
  // Save the base address.
  sa->base = (void*)arg;
  
  // Reset the cache.
  int l, i;
  for (l = 0; l < NUM_LINES; l++) {
    for (i = 0; i < NUM_ENTS; i++) {
      sa->c[l].e[i].sector = 0;
      sa->c[l].e[i].dirty = 0;
    }
    sa->c[l].lru = 0;
  }
  
  // Save the driver structure pointer.
  r.drv_ref = (void*)sa;
  return r;
}

/**
 * Driver umount implementation.
 */
static int drv_ace_umount(void *mount_ref) {
  
  // Check arguments.
  if (!mount_ref) return EINVAL;
  sysace_t *sa = (sysace_t*)mount_ref;
  
  // Perform a sync.
  
  
  // Free the data structure.
  free(sa);
  
  // Success.
  return 0;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_ace_open(void *mount_ref, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref) { r.err_no = EINVAL; return r; }
  if (fname) { r.err_no = EINVAL; return r; }
  
  // Create a file structure.
  sysace_file_t *file = (sysace_file_t*)malloc(sizeof(sysace_file_t));
  if (!file) { r.err_no = ENOMEM; return r; }
  
  // Set the file offset to 0.
  file->offs = 0;
  
  // Success. Return the file object as the driver reference.
  r.drv_ref = (void*)file;
  return r;
}

/**
 * Driver read implementation.
 */
static rootfs_read_ret_t drv_ace_read(void *mount_ref, void *file_ref, void *buf, size_t cnt) {
  return drv_ace_transfer((sysace_t*)mount_ref, (sysace_file_t*)file_ref, buf, cnt, 0);
}

/**
 * Driver write implementation.
 */
static rootfs_write_ret_t drv_ace_write(void *mount_ref, void *file_ref, const void *buf, size_t cnt) {
  rootfs_write_ret_t r = {};
  rootfs_read_ret_t rr = drv_ace_transfer((sysace_t*)mount_ref, (sysace_file_t*)file_ref, (void*)buf, cnt, 1);
  r.err_no = rr.err_no;
  r.amount = rr.amount;
  return r;
}

/**
 * Driver close callback.
 */
static int drv_ace_close(void *mount_ref, void *file_ref) {
  
  // Check arguments.
  if (!file_ref) return EBADF;
  sysace_file_t *file = (sysace_file_t*)file_ref;
  
  // Free the file structure.
  free(file);
  
  // Success.
  return 0;
}

/**
 * Driver lseek callback.
 */
static rootfs_lseek_ret_t drv_ace_lseek(void *mount_ref, void *file_ref, off_t offs, int whence) {
  rootfs_lseek_ret_t r = {};
  
  // Check arguments.
  if (!file_ref) { r.err_no = EBADF; return r; }
  sysace_file_t *file = (sysace_file_t*)file_ref;
  
  // Figure out the desired offset.
  long long new_offs = offs;
  switch (whence) {
    case SEEK_SET: break;
    case SEEK_CUR: new_offs += file->offs; break;
    case SEEK_END: new_offs += 0x80000000; break;
    default: { r.err_no = EINVAL; return r; }
  }
  
  // Check range.
  if ((new_offs < 0) || (new_offs > 0x80000000)) {
    r.err_no = EINVAL;
    return r;
  }
  
  // Set the new offset.
  file->offs = (off_t)new_offs;
  
  // Success.
  r.pos = (off_t)new_offs;
  return r;
}

/**
 * Driver ioctl callback.
 */
static rootfs_ioctl_ret_t drv_ace_ioctl(void *mount_ref, void *file_ref, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref) { r.err_no = EINVAL; return r; }
  sysace_t *sa = (sysace_t*)mount_ref;
  
  // If the request isn't sync, it's not a command we know about.
  if (request != IO_SYNC) { r.err_no = EINVAL; return r; }
  
  // Pass control to sync.
  r.err_no = drv_ace_sync(sa);
  return r;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(sysace) {
  .name   = DRIVER_NAME,
  .d_type = DT_BLK,
  .mount  = drv_ace_mount,
  .umount = drv_ace_umount,
  .open   = drv_ace_open,
  .read   = drv_ace_read,
  .write  = drv_ace_write,
  .close  = drv_ace_close,
  .lseek  = drv_ace_lseek,
  .ioctl  = drv_ace_ioctl
};

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created.
 */
int mount_sysace(const char *path, long address) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Try to do the mount.
  return mount(NULL, path, DRIVER_NAME, 0, (const void*)address);
}

