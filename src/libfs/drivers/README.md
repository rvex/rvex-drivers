
RootFS drivers
==============

This directory contains drivers that extend the functionality of rootfs. These
can be equated to Linux kernel filesystem modules or device file drivers
depending on the driver. They are registered from userspace using the respective
`mount_<driver>` call; the only global function defined by a driver. This
function is just a wrapper around the `mount` syscall, but it is usually
necessary to use the function to force inclusion of the driver object file when
linking the application.

The single other global that a driver should define is the entry in rootfs'
driver table, done using the `ROOTFS_REGISTER_DRIVER` macro from `sys/rootfs.h`.
The `mount` handler in rootfs searches this table for the driver specified in
the `mount` arguments.

rvudev
------

This driver allows creation of character device files that map to an r-VEX debug
UART. This driver is usually connected to stdin/stdout/stderr in the platform's
rootfs setup method. Mount function:

    #include <rvudev.h>
    int mount_rvudev(const char *path, long address);

block
-----

This driver allows creation of block device files that simply map directly to
physical memory. It's useful especially as a backing device for mounting a
filesystem with another driver. Mount function:

    #include <rootfs.h>
    int mount_block(const char *path, void *start, size_t size);

fileslice
---------

This driver allows creation of a block device backed by a contiguous slice of
another file. This is especially useful for making partition block devices from
a volume block device. As such, this is used by the `mount_mbr` function in the
user directory. Mount function:

    #include <fileslice.h>
    int mount_fileslice(const char *src, const char *target, off_t start, off_t size)

fatfs
-----

This driver uses elm chan's FatFS driver to allow mounting of FAT12, FAT16 and
FAT32 partitions. The block data is taken from a (block device) file (which
could technically be a file in another FatFS mount for FATception) supplied at
mount time, so you'll need some a device driver in addition to this. Mount
function:

    #include <fatfs.h>
    int mount_fatfs(const char *source, const char *target, int readonly);

See also: http://elm-chan.org/fsw/ff/00index_e.html

sysace
------

This driver is (currently) rather specific to the ml605-demo board. It
interfaces with its sysace instance and accompanying bus wrapper for paging and
write-protection. More specifically, it expects the following behavior for the
memory-mapped region supplied at mount time:

 - The region is 1 GiB in size. It must be at least sector-aligned (512 bytes).
 - The first 512 MiB maps directly to the currently selected page of the at
   most 2 GiB compactflash card.
 - The hardware must write-buffer exactly one sector. More specifically, if an
   address outside the previously-written sector is accessed, the sector must be
   flushed to the card. If the sector is subsequently accessed again, it must be
   reread from the card to support the verification logic in the driver.
 - The page and write-protection lock is controlled by a write-only 32-bit
   register at the end of the 1 GiB region (that is, `base + 0x3FFFFFFC`).
   Writes to this register that do not match the `0x12355AA*` should be rejected
   to prevent accidental disabling of the write-protect lock. Bit 1:0 select the
   current page (essentially forming address bits 30:29). Bit 2 must be set to
   allow writing, and cleared to activate write-protection.
 - While the region is write-protected, writes must be ignored. The access may
   result in a bus fault.
 - Reading outside the first 512 MiB region must at least flush the last sector
   within the region. Thus, the first 512 MiB may either be mirrored, or the
   page address may be added to the requested address (the latter is the
   behavior of the ml605-demo platform).

Mount function:

    #include <sysace.h>
    int mount_sysace(const char *path, long address);

gri2c
-----

This driver provides rudimentary access to I2C devices using the GRlib I2C
peripheral.

Mount function:

    #include <gri2c.h>
    int mount_i2c(const char *path, long ambaclk, long address);

`ambaclk` must be set to the AMBA bus frequency in kHz. It is used to configure
the I2C peripheral prescaler to get a 100kHz I2C clock (standard mode).

Usage:

    int file = open(path, O_RDWR, 0);
    if (file < 0) exit(1);

When you have opened the device, you must specify with what device address you
want to communicate. The address ranges from 0 to 127, 0 being general call.

    int addr = 0x40;
    if (ioctl(file, I2C_SLAVE, (void*)addr) < 0) exit(1);

Now you can use lseek/read/write to communicate with the device. This works a
little different from Linux's implementation, where reads and writes are
straight I2C without the register abstraction level; this implementation uses
the file pointer *modulo* 256 as the register number to access. The file pointer
auto-increments by whatever number read/write returns. When you read/write
beyond register 255, the read/write wraps back to register 0. `SEEK_END` and
`SEEK_SET` both reference register 0.

For example, here's a read-modify-write for register 32.

    // Seek to register 32.
    if (lseek(file, 32, SEEK_SET) != 32) exit(1);

    // Read data from register 32.
    char data;
    if (read(file, &data, 1) != 1) exit(1);

    // Do some stuff with the contents.
    data ^= 0x55;

    // Write the value back to register 32.
    if (lseek(file, 32, SEEK_SET) != 32) exit(1);
    if (write(file, &data, 1) != 1) exit(1);

grps2
-----

This driver exposes PS/2 devices as /dev/input/event*-like files using the
GRlib APBPS2 peripheral. This driver requires a `gettimeofday()`
implementation, as well as an interrupt controller exposing the interface
specified in the `irq.h` header.

Mount function:

    #include <grps2.h>
    int mount_grps2(const char *path, long address, int irq);

`irq` must be set to the interrupt associated with the APBPS2 peripheral at
`address`, as accepted by the functions specified in `irq.h`. `gettimeofday()`
will be called from the interrupt service routine.

Usage:

    int file = open(path, O_RDONLY | O_NONBLOCK, 0);
    if (file < 0) exit(1);
    
    struct input_event ev;
    if (read(file, &ev, sizeof(ev)) == sizeof(ev)) {
      // Parse event here.
    } else if (errno == EWOULDBLOCK) {
      // No event available right now.
    } else {
      // An error occurred.
    }

The mount function fails with `EIO` if no PS/2 device is responding. Otherwise,
it should autodetect whether the device is a mouse or a keyboard and initialize
accordingly. Hotplugging is NOT supported.

Keyboards generate the following events:

| `type`   | `code`    | `value`  | Significance           |
| -------- | --------- | -------- | ---------------------- |
| `EV_KEY` | `KEY_*`   | 0        | Key released           |
| `EV_KEY` | `KEY_*`   | 1        | Key pressed            |
| `EV_KEY` | `KEY_*`   | 2        | Typematic (autorepeat) |
| `EV_CHR` | character | 0        | Decoded character      |

`EV_CHR` is nonstandard. It provides ASCII characters for keys where that makes
sense, triggering in addition to the `EV_KEY` event for both press and
typematic events. This decoded character takes shift into consideration, but
caps lock is not supported. The keypad behaves as if num lock is on for this
event.

Note that `EV_SCAN` is not reported.

Mice generate the following events:

| `type`   | `code`       | `value`  | Significance    |
| -------- | ------------ | -------- | --------------- |
| `EV_KEY` | `BTN_LEFT`   | 0        | LMB released    |
| `EV_KEY` | `BTN_LEFT`   | 1        | LMB pressed     |
| `EV_KEY` | `BTN_RIGHT`  | 0        | RMB released    |
| `EV_KEY` | `BTN_RIGHT`  | 1        | RMB pressed     |
| `EV_KEY` | `BTN_MIDDLE` | 0        | MMB released    |
| `EV_KEY` | `BTN_MIDDLE` | 1        | MMB pressed     |
| `EV_REL` | `REL_X`      | offset   | X axis movement |
| `EV_REL` | `REL_Y`      | offset   | Y axis movement |

The scroll wheel and extra buttons are not supported.

The driver has a built-in FIFO 256 events in size. If this overflows for an
open file descriptor, it will behave as if the oldest multiple of 256 events
was lost. Overflow is also not detected in any way. It is therefore advisable
to poll often enough...

dsp
---

This driver exposes an audio output as a /dev/dsp-like device file. The driver
is pretty universal, configurable at mount time.

Mount function:

    #include <dsp.h>
    int mount_dsp(const char *path, const mount_dsp_arg_t *arg);

`arg` must point to a fully initialized structure that describes the audio
device. The structure is as follows:

 - `int flags`: a bitmask with various options. Refer to the list below this
   one.

 - `float frequency`: specifies the platform frequency, or if the samplerate is
   fixed, this specifies the samplerate. Both are in Hz.

 - `volatile unsigned int *scale1`: specifies the address of the samplerate
   scaler register, or NULL if the samplerate is fixed. If specified, setting
   the samplerate will cause a write to this register. The value written is
   the value in `frequency` divided by the given samplerate.

 - `volatile unsigned int *scale2`: if non-null, this register is also written
   whenever `scale1` is written. It is set to the same value. This is useful
   for crappy timers which behave strangely when their scaler is set to a value
   below the current counter value. In that case, point this to the counter
   register.

 - `volatile unsigned int *data`: must point to the data output register. This
   register is always written using one 32-bit access per sample. If the sample
   format is less than 4 bytes per sample, the value is repeated. For example,
   for an 8-bit mono sample of `0x12`, `0x12121212` is written.

 - `volatile unsigned int *status`: must point to the status register, which
   indicates the state of the hardware FIFO. Depending on `flags`, this must be
   either the amount of free bytes in the FIFO or the amount of currently
   queued bytes.

 - `const volatile unsigned int *size_reg` OR `unsigned int size_direct`:
   specifies either an address to a buffer size register, or its immediate
   value if the size is fixed and known at mount-time. The value in `flags`
   determines which is used. The unit is samples.

The following flags are available:

 - `DSP_MOUNT_FMT_8BIT` and `*_16BIT`: specifies the number of bits per sample.
   Exactly one of these must be specified.

 - `DSP_MOUNT_FMT_MONO` and `*_STEREO`: specifies the number of channels.
   Exactly one of these must be specified.

 - `DSP_MOUNT_FMT_UNSIGNED` and `*_SIGNED`: specifies whether the samples are
   signed or unsigned. Exactly one of these must be specified.

 - `DSP_MOUNT_FMT_BIG` and `*_LITTLE`: specifies the endianness of the samples.
   Exactly one of these must be specified when DSP_MOUNT_FMT_16BIT is
   specified.

 - `DSP_MOUNT_PRES_DIM_ONE`: when added to the mask, indicates that the scaler
   register uses diminished-one encoding.

 - `DSP_MOUNT_STAT_REMAIN` and `*_AVAIL`: specifies whether the `status`
   register value signifies the amount of samples currently queued (`*_REMAIN`)
   or free (`*_AVAIL`). Exactly one of these must be specified.

 - `DSP_MOUNT_SIZE_DIRECT` and `*_REG`: specifies whether `size_reg` or
   `size_direct` is used to determine the FIFO size.

Usage:

    int file = open(path, O_WRONLY | O_NONBLOCK, 0);
    if (file < 0) exit(1);
    
    int val;
    
    val = 44100; // or some other samplerate
    if (ioctl(file, SNDCTL_DSP_SPEED, &val) < 0) exit(1);
    // val now contains the actual samplerate, which may be different due to
    // roundoff error or peripheral limitations
    
    val = AFMT_U16_LE; // or some other format (see soundcard.h)
    if (ioctl(file, SNDCTL_DSP_SETFMT, &val) < 0) exit(1);
    
    val = 1; // 1 for stereo, 0 for mono
    if (ioctl(file, SNDCTL_DSP_STEREO, &val) < 0) exit(1);
    
    // write to the peripheral
    write(file, buffer, bufsize);

The driver will automatically convert from the selected sample format to the
native one specified at mount time, including mixing two incoming stereo
channels to mono. You can also query the native format like this:

    val = AFMT_QUERY;
    if (ioctl(file, SNDCTL_DSP_SETFMT, &val) < 0) exit(1);
    // val now contains AFMT_U8, AFMT_U16_LE, AFMT_U16_BE, AFMT_S8,
    // AFMT_S16_LE, or AFMT_S16_BE

    val = 0;
    if (ioctl(file, SNDCTL_DSP_CHANNELS, &val) < 0) exit(1);
    // val now contains 1 for mono, 2 for stereo

Using the native format should give you somewhat better performance. The driver
can't upsample or downsample, so all supported samplerates have identical
buffer write performance.

Some more ioctls are available for compatibility with `/dev/dsp`. The most
relevant are `SNDCTL_DSP_SYNC` and `SNDCTL_DSP_GETOSPACE`. `SNDCTL_DSP_SYNC`
blocks until the hardware FIFO runs dry, `SNDCTL_DSP_GETOSPACE` allows you to
retrieve the buffer status. This driver has no concept of fragments, so the
fragment size for the latter is simply set to the size of a single sample.

grsvga
------

This driver implements the GRlib SVGACTRL peripheral as a /dev/fb0-like device.
It supports the following modes:

 - 320x240 8bpp (upscaled to 640x480 in software)
 - 400x300 8bpp (upscaled to 800x600 in software)
 - 640x480 8bpp
 - 640x480 16bpp
 - 640x480 32bpp
 - 800x600 8bpp
 - 800x600 16bpp
 - 800x600 32bpp

Mount function:

    #include <grsvga.h>
    int mount_grsvga(const char *path, long address);

`address` should be set to the base address of the svgactrl peripheral. The
mount function sets up a white screen at 640x480 16bpp at mount time.

To change the mode:

    struct fb_var_screeninfo info;
    int fd;
    
    // Open file descriptor.
    fd = open("/dev/fb0", O_WRONLY | O_NONBLOCK, 0);
    
    // Get the current display mode.
    ioctl(fd, FBIOGET_VSCREENINFO, &info);
    
    // Change the resolution.
    info.xres = xres;
    info.yres = yres;
    info.bits_per_pixel = bpp;
    
    // Request the changed display mode.
    ioctl(fd, FBIOPUT_VSCREENINFO, &info);

The `FBIOPUT_VSCREENINFO` only looks at the `xres` and `bits_per_pixel` fields
of the request, so you can actually omit the `FBIOGET_VSCREENINFO` `ioctl` and
write to `yres` if you don't care about compatibility. Changing the mode always
makes a white screen, or whatever is in palette entry 255 for 8 bits per pixel.

To upload a new color map:

    unsigned char cmap[256*3];
    
    cmap[0] = /* index 0 red channel */;
    cmap[1] = /* index 0 red channel */;
    cmap[2] = /* index 0 red channel */;
    cmap[3] = /* index 1 red channel */;
    /* and so on */
    
    ioctl(fd, FBIOPUTCMAP, cmap);

An `ioctl` to request the current color map is available but not implemented,
as the peripheral color lookup table is write-only and it's hardly worth the
additional overhead.

The framebuffer behaves like a write-only block device, so to write pixels to
it just use `lseek` and `write`. (It would be straightforward to implement
`read` as well, but who has time?) You can also request the framebuffer pointer
directly like this:

    struct fb_fix_screeninfo fixed_info;
    
    // Get the framebuffer pointer.
    ioctl(fd, FBIOGET_VSCREENINFO, &fixed_info);
    void *fb = (void*)fixed_info.smem_start;

... but if you're in 320x240 or 400x300 you'll have to handle upscaling by
yourself.
