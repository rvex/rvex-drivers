/**
 * Driver for GRlib SVGACTRL, exposing it as something that looks like Linux
 * /dev/fb0. It supports 640x480 and 800x600 with direct access to the
 * framebuffer, as well as emulated 320x240 and 400x300 modes, but you have
 * to use write() for those. 8-bit palette and 16-bit 5:6:5 color modes are
 * supported.
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <grsvga.h>
#include <fb.h>
#include <sys/mount.h>

#define DRIVER_NAME "grsvga"


/**
 * Peripheral structure.
 */
typedef struct svgactrl_t {
  unsigned int status;
  unsigned int vidlen;
  unsigned int fplen;
  unsigned int synclen;
  unsigned int linelen;
  const void *framebuf;
  const unsigned int clocks[4];
  unsigned int clut;
} svgactrl_t;

/**
 * Resolution information.
 */
typedef struct res_info_t {
  
  // Resolution for fb_var_screeninfo.
  unsigned short xres;
  unsigned short yres;
  
  // Resolution for peripheral.
  unsigned short xres_phys;
  unsigned short yres_phys;
  
  // Whether we need to scale x2 from userspace to framebuffer.
  unsigned char scale;
  
  // Clock selection mux for peripheral.
  unsigned char clksel;
  
  // Pixel clock info for fb_var_screeninfo.
  unsigned short pixclk;
  
  // Timing info for peripheral and fb_var_screeninfo.
  unsigned short left_margin;
  unsigned short right_margin;
  unsigned short upper_margin;
  unsigned short lower_margin;
  unsigned short hsync_len;
  unsigned short vsync_len;
  
} res_info_t;

/**
 * List of supported resolutions.
 */
#define NUM_RES_SUPPORTED 4
static const res_info_t drv_svga_res[NUM_RES_SUPPORTED] = {
  {
    .xres =          320,
    .yres =          240,
    .xres_phys =     640,
    .yres_phys =     480,
    .scale =           1,
    .clksel =          0, /* pixclock = 25MHz */
    .pixclk =      40000, /* ps */
    .left_margin =    48,
    .right_margin =   16,
    .upper_margin =   31,
    .lower_margin =   11,
    .hsync_len =      96,
    .vsync_len =       2
  },
  {
    .xres =          400,
    .yres =          300,
    .xres_phys =     800,
    .yres_phys =     600,
    .scale =           1,
    .clksel =          3, /* pixclock = 40MHz */
    .pixclk =      25000, /* ps */
    .left_margin =    88,
    .right_margin =   40,
    .upper_margin =   23,
    .lower_margin =    1,
    .hsync_len =     128,
    .vsync_len =       4
  },
  {
    .xres =          640,
    .yres =          480,
    .xres_phys =     640,
    .yres_phys =     480,
    .scale =           0,
    .clksel =          0, /* pixclock = 25MHz */
    .pixclk =      40000, /* ps */
    .left_margin =    48,
    .right_margin =   16,
    .upper_margin =   31,
    .lower_margin =   11,
    .hsync_len =      96,
    .vsync_len =       2
  },
  {
    .xres =          800,
    .yres =          600,
    .xres_phys =     800,
    .yres_phys =     600,
    .scale =           0,
    .clksel =          3, /* pixclock = 40MHz */
    .pixclk =      25000, /* ps */
    .left_margin =    88,
    .right_margin =   40,
    .upper_margin =   23,
    .lower_margin =    1,
    .hsync_len =     128,
    .vsync_len =       4
  }
};

/**
 * Color depth information.
 */
typedef struct bpp_info_t {
  
  // Bits per pixel for this mode.
  unsigned char bpp;
  
  // Bits depth register value.
  unsigned char bdsel;
  
  // Whether scaling is supported in this mode.
  unsigned char scale_support;
  
  // Whether this mode uses a palette.
  unsigned char palette;
  
  // Bitfield information.
  unsigned char r_off;
  unsigned char r_len;
  unsigned char g_off;
  unsigned char g_len;
  unsigned char b_off;
  unsigned char b_len;
  
} bpp_info_t;

/**
 * List of supported color depths.
 */
#define NUM_BPP_SUPPORTED 3
static const bpp_info_t drv_svga_bpp[NUM_BPP_SUPPORTED] = {
  {
    .bpp =             8,
    .bdsel =           1,
    .scale_support =   1,
    .palette =         1,
    .r_off =          16,
    .r_len =           8,
    .g_off =           8,
    .g_len =           8,
    .b_off =           0,
    .b_len =           8
  },
  {
    .bpp =            16,
    .bdsel =           2,
    .scale_support =   0,
    .palette =         0,
    .r_off =          11,
    .r_len =           5,
    .g_off =           5,
    .g_len =           6,
    .b_off =           0,
    .b_len =           5
  },
  {
    .bpp =            32,
    .bdsel =           3,
    .scale_support =   0,
    .palette =         0,
    .r_off =          16,
    .r_len =           8,
    .g_off =           8,
    .g_len =           8,
    .b_off =           0,
    .b_len =           8
  }
};

/**
 * SVGA mount structure.
 */
typedef struct svga_mount_ref_t {
  
  // Peripheral access.
  volatile svgactrl_t *regs;
  
  // Framebuffer address.
  void *fb_malloc;
  void *fb;
  
  // Amount of space allocated for the framebuffer.
  int fb_size;
  
  // Visible size of the framebuffer.
  int fb_vis_size_phys;
  
  // Visible size of the framebuffer as presented to the application (before
  // upscaling).
  int fb_vis_size;
  
  // Framebuffer configuration.
  const res_info_t *res;
  const bpp_info_t *bpp;
  
} svga_mount_ref_t;

/**
 * SVGA object reference structure.
 */
typedef struct svga_object_ref_t {
  
  // Current offset within the framebuffer in bytes.
  int offset;
  
} svga_object_ref_t;

/**
 * Configures the SVGA to the given xres and bpp, rounded up and clamped to
 * capabilities.
 */
static void drv_svga_config(svga_mount_ref_t *svga, int xres, int bpp) {
  int i;
  
  // Find the appropriate color depth info.
  const bpp_info_t *bi = drv_svga_bpp;
  i = 0;
  while (1) {
    if (bi->bpp >= bpp) break;
    i++;
    if (i >= NUM_BPP_SUPPORTED) break;
    bi++;
  }
  
  // Find the appropriate resolution info.
  const res_info_t *ri = drv_svga_res;
  i = 0;
  while (1) {
    if (!ri->scale || bi->scale_support) {
      if (ri->xres >= xres) break;
    }
    i++;
    if (i >= NUM_RES_SUPPORTED) break;
    ri++;
  }
  
  // Save in mount reference.
  svga->res = ri;
  svga->bpp = bi;
  
  // Reset the SVGA controller.
  svga->regs->status = 2;
  
  // Configure the SVGA controller.
  svga->regs->vidlen  = ((ri->yres_phys-1) << 16) + (ri->xres_phys-1);
  svga->regs->fplen   = (ri->lower_margin << 16) + ri->right_margin;
  svga->regs->synclen = (ri->vsync_len << 16) + ri->hsync_len;
  svga->regs->linelen = ((ri->yres_phys + ri->lower_margin + ri->upper_margin + ri->vsync_len - 1) << 16) +
                         (ri->xres_phys + ri->right_margin + ri->left_margin + ri->hsync_len - 1);
  
  // Set the framebuffer pointer.
  svga->regs->framebuf = svga->fb;
  
  // Figure out the size of the visible area of the framebuffer.
  svga->fb_vis_size_phys = (svga->res->xres_phys * svga->res->yres_phys) << (svga->bpp->bdsel - 1);
  svga->fb_vis_size      = (svga->res->xres      * svga->res->yres     ) << (svga->bpp->bdsel - 1);
  
  // Clear the framebuffer.
  memset(svga->fb, 255, svga->fb_vis_size_phys);
  
  // Start the SVGA controller.
  svga->regs->status = 1 | (ri->clksel << 6) | (bi->bdsel << 4);
  
}

/**
 * Driver mount callback.
 */
static rootfs_mount_ret_t drv_svga_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Check arguments.
  if (isdir) { r.err_no = EISDIR; return r; }
  if (src || !arg) { r.err_no = EINVAL; return r; }
  
  // Allocate the mount data structure.
  svga_mount_ref_t *svga = (svga_mount_ref_t*)malloc(sizeof(svga_mount_ref_t));
  if (!svga) { r.err_no = ENOMEM; return r; }
  
  // Set the register access pointer.
  svga->regs = (volatile svgactrl_t*)arg;
  
  // Allocate the framebuffer. This must be 1kiB aligned, so we allocated
  // 1kiB-1 extra so we can choose a 1kiB-aligned area within the malloc.
  int size = 800*600*4;
  void *fb = malloc(size + 1023);
  if (!fb) { free(svga); r.err_no = ENOMEM; return r; }
  svga->fb_malloc = fb;
  fb = (void*)(((long)fb + 1023) & (~1023));
  svga->fb = fb;
  svga->fb_size = size;
  
  // Configure the SVGA.
  drv_svga_config(svga, 320, 240);
  
  // Save the mount reference.
  r.drv_ref = (void*)svga;
  
  // Success.
  return r;
}

/**
 * Driver umount callback.
 */
static int drv_svga_umount(void *mount_ref) {
  
  // Check arguments.
  if (!mount_ref) return EINVAL;
  svga_mount_ref_t *svga = (svga_mount_ref_t*)mount_ref;
  
  // Disable the SVGA controller.
  svga->regs->status = 2;
  
  // Free the framebuffer.
  free(svga->fb_malloc);
  
  // Free the data structure.
  free(svga);
  
  // Success.
  return 0;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_svga_open(void *mount_ref, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Create a object reference structure.
  svga_object_ref_t *ref = (svga_object_ref_t*)calloc(1, sizeof(svga_object_ref_t));
  if (!ref) { r.err_no = ENOMEM; return r; }
  
  // Clear the offset.
  ref->offset = 0;
  
  // Success.
  r.drv_ref = (void*)ref;
  return r;
}

/**
 * Driver write implementation.
 */
static rootfs_write_ret_t drv_svga_write(void *mount_ref, void *file_ref, const void *buf, size_t cnt) {
  rootfs_write_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  svga_mount_ref_t *svga = (svga_mount_ref_t*)mount_ref;
  svga_object_ref_t *ref = (svga_object_ref_t*)file_ref;
  
  // Figure out offset and count stuff.
  int offs = ref->offset;
  int max_cnt = svga->fb_vis_size - offs;
  if (cnt > max_cnt) cnt = max_cnt;
  ref->offset = offs + cnt;
  r.amount = cnt;
  
  // Switch between 2x upscaling mode and normal mode.
  if (svga->res->scale) {
    
    // Figure out stride.
    int s = svga->res->xres;
    
    // Figure out the coordinate of the first offset.
    int x = offs % s;
    int y = offs / s;
    
    // Figure out the two write addresses.
    unsigned long w1 = (unsigned long)svga->fb + (x << 1) + ((y * s) << 2);
    unsigned long w2 = w1 + (s << 1);
    
    // Negative x; is 0 at the end of a line.
    int t = x - s;
    
    // Handle preamble.
    while (cnt && (t & 3)) {
      
      // Write the pixel.
      unsigned short pix = *((unsigned char*)buf);
      pix *= 0x101;
      *((unsigned short*)w1) = pix;
      *((unsigned short*)w2) = pix;
      
      // Next pixel.
      buf += 1;
      cnt -= 1;
      w1  += 2;
      w2  += 2;
      t   -= 1;
      if (!t) {
        w1 += s << 2;
        w2 += s << 2;
        t = s;
      }
      
    }
    
    // Blit four pixels at a time.
    while (cnt > 3) {
      
      // Write the pixel.
      unsigned short pix1 = ((unsigned char*)buf)[0];
      unsigned short pix2 = ((unsigned char*)buf)[1];
      unsigned short pix3 = ((unsigned char*)buf)[2];
      unsigned short pix4 = ((unsigned char*)buf)[3];
      pix1 *= 0x101;
      pix2 *= 0x101;
      pix3 *= 0x101;
      pix4 *= 0x101;
      unsigned int pixa = (pix1 << 16) | pix2;
      unsigned int pixb = (pix3 << 16) | pix4;
      ((unsigned int*)w1)[0] = pixa;
      ((unsigned int*)w1)[1] = pixb;
      ((unsigned int*)w2)[0] = pixa;
      ((unsigned int*)w2)[1] = pixb;
      
      // Next pixel quad.
      buf += 4;
      cnt -= 4;
      w1  += 8;
      w2  += 8;
      t   -= 4;
      if (!t) {
        w1 += s << 2;
        w2 += s << 2;
        t = s;
      }
      
    }
    
    // Handle epilog.
    while (cnt) {
      
      // Write the pixel.
      unsigned short pix = *((unsigned char*)buf);
      pix *= 0x101;
      *((unsigned short*)w1) = pix;
      *((unsigned short*)w2) = pix;
      
      // Next pixel.
      buf += 1;
      cnt -= 1;
      w1  += 2;
      w2  += 2;
      t   -= 1;
      if (!t) {
        w1 += s << 2;
        w2 += s << 2;
        t = s;
      }
      
    }
    
  } else {
    
    // Defer to memcpy's expertise.
    memcpy(svga->fb + offs, buf, cnt);
    
  }
  
  // Success.
  return r;
}

/**
 * Driver close callback.
 */
static int drv_svga_close(void *mount_ref, void *file_ref) {
  
  // Free the file structure.
  free(file_ref);
  
  // Success.
  return 0;
}

/**
 * Driver lseek callback.
 */
static rootfs_lseek_ret_t drv_svga_lseek(void *mount_ref, void *file_ref, off_t offs, int whence) {
  rootfs_lseek_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  svga_mount_ref_t *svga = (svga_mount_ref_t*)mount_ref;
  svga_object_ref_t *ref = (svga_object_ref_t*)file_ref;
  
  // Set the new offset.
  int size = svga->fb_vis_size;
  switch (whence) {
    case SEEK_SET:                      break;
    case SEEK_CUR: offs += ref->offset; break;
    case SEEK_END: offs += size;        break;
    default:       r.err_no = EINVAL;   return r;
  }
  
  // Check range.
  if ((offs < 0) || (offs > size)) { r.err_no = EINVAL; return r; }
  
  // Save the new offset.
  ref->offset = offs;
  
  // Success.
  r.pos = offs;
  return r;
}

/**
 * Driver stat callback.
 */
static int drv_svga_stat(void *mount_ref, void *file_ref, struct stat *sb) {
  
  // Check arguments.
  if (!mount_ref) return EBADF;
  svga_mount_ref_t *svga = (svga_mount_ref_t*)mount_ref;
  
  // Override the size.
  sb->st_size = svga->fb_vis_size;
  
  // Success.
  return 0;
};

/**
 * Driver ioctl callback.
 */
static rootfs_ioctl_ret_t drv_svga_ioctl(void *mount_ref, void *file_ref, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  svga_mount_ref_t *svga = (svga_mount_ref_t*)mount_ref;
  
  // Check argument existance.
  if (!param && (request != FBIO_WAITFORVSYNC)) { r.err_no = EINVAL; return r; }
  
  // Handle requests.
  struct fb_var_screeninfo *vpar = (struct fb_var_screeninfo*)param;
  struct fb_fix_screeninfo *fpar = (struct fb_fix_screeninfo*)param;
  unsigned char *cpar = (unsigned char*)param;
  int i;
  switch (request) {
    
    case FBIOPUT_VSCREENINFO: // Put fb_var_screeninfo.
      drv_svga_config(svga, vpar->xres, vpar->bits_per_pixel);
      // (fallthrough)
      
    case FBIOGET_VSCREENINFO: // Get fb_var_screeninfo.
      vpar->xres              = svga->res->xres;
      vpar->yres              = svga->res->yres;
      vpar->xres_virtual      = svga->res->xres;
      vpar->yres_virtual      = svga->res->yres;
      vpar->xoffset           = 0;
      vpar->yoffset           = 0;
      
      vpar->bits_per_pixel    = svga->bpp->bpp;
      vpar->grayscale         = 0;
      
      vpar->red.offset        = svga->bpp->r_off;
      vpar->red.length        = svga->bpp->r_len;
      vpar->red.msb_right     = 0;
      vpar->green.offset      = svga->bpp->g_off;
      vpar->green.length      = svga->bpp->g_len;
      vpar->green.msb_right   = 0;
      vpar->blue.offset       = svga->bpp->b_off;
      vpar->blue.length       = svga->bpp->b_len;
      vpar->blue.msb_right    = 0;
      vpar->transp.offset     = 0;
      vpar->transp.length     = 0;
      vpar->transp.msb_right  = 0;
      
      vpar->nonstd            = 0;
      vpar->activate          = 0;
      vpar->height            = 320;
      vpar->width             = 427;
      vpar->accel_flags       = 0;
      
      vpar->pixclock          = svga->res->pixclk;
      vpar->left_margin       = svga->res->left_margin;
      vpar->right_margin      = svga->res->right_margin;
      vpar->upper_margin      = svga->res->upper_margin;
      vpar->lower_margin      = svga->res->lower_margin;
      vpar->hsync_len         = svga->res->hsync_len;
      vpar->vsync_len         = svga->res->vsync_len;
      vpar->sync              = 0;
      vpar->vmode             = 0;
      return r;
    
    case FBIOGET_FSCREENINFO: // Get fb_fix_screeninfo.
      memset(fpar, 0, sizeof(struct fb_fix_screeninfo));
      strncpy(fpar->id, "grlib svgactrl", 16);
      fpar->smem_start = (unsigned long)svga->fb;
      fpar->smem_len   = svga->fb_size;
      fpar->mmio_start = (unsigned long)svga->regs;
      fpar->mmio_len   = sizeof(svgactrl_t);
      return r;
    
    case FBIOGETCMAP: // Get 256*3 char (rgb) colormap.
      // Not supported by hardware, not worth making a copy for.
      r.err_no = EINVAL;
      return r;
    
    case FBIOPUTCMAP: // Put 256*3 char (rgb) colormap.
      for (i = 0; i < 256; i++) {
        unsigned char r = *cpar++;
        unsigned char g = *cpar++;
        unsigned char b = *cpar++;
        svga->regs->clut = (i << 24) | (r << 16) | (g << 8) | b;
      }
      return r;
    
    case FBIO_WAITFORVSYNC: // Block until vsync.
      while (!(svga->regs->status & 8));
      return r;
    
  }
  
  // Unknown ioctl.
  r.err_no = EINVAL;
  return r;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(svga) {
  .name   = DRIVER_NAME,
  .d_type = DT_BLK,
  .mount  = drv_svga_mount,
  .umount = drv_svga_umount,
  .open   = drv_svga_open,
  .write  = drv_svga_write,
  .close  = drv_svga_close,
  .lseek  = drv_svga_lseek,
  .stat   = drv_svga_stat,
  .ioctl  = drv_svga_ioctl
};


/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created. address must be set to the peripheral address
 * of the svgactrl peripheral.
 */
int mount_grsvga(const char *path, long address) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Try to do the mount.
  return mount(NULL, path, DRIVER_NAME, 0, (const void*)address);
}

