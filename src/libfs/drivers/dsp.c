/**
 * Universal driver for a basic audio output peripheral, described using the
 * following information (supplied at mount time):
 *  - input clock frequency in Hz
 *  - scaler register address (dividing down from the input clock frequency)
 *  - whether the scaler is diminished-one encoded
 *  - data register address
 *  - data register format
 *  - address for getting the total number of bytes currently in the hardware
 *    buffer, or amount of space remaining in the hardware buffer
 *  - address for getting the total size of the hardware buffer
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <dsp.h>
#include <soundcard.h>
#include <sys/mount.h>

#define DRIVER_NAME "dsp"

/**
 * DSP mount argument structure.
 */
typedef struct dsp_object_ref_t {
  
  // Format selected by the file descriptor. If it's different from the native
  // format, the driver will convert. This uses the DSP_MOUNT_FMT_* defs.
  int format;
  
} dsp_object_ref_t;

/**
 * Sets/retrieves the samplerate.
 */
static void drv_dsp_samplerate(mount_dsp_arg_t *dsp, int *srate) {
  
  // Check for fixed samplerate.
  if (!dsp->scale1) {
    *srate = dsp->frequency;
    return;
  }
  
  // Figure out the scalar.
  int scale = (int)(dsp->frequency / (float)(*srate) + 0.5f);
  
  // Return the actual samplerate.
  *srate = (int)(dsp->frequency / (float)scale + 0.5f);
  
  // If the scalar is stored as diminished-one, subtract one.
  if ((dsp->flags & DSP_MOUNT_PRES_DIM_ONE) && scale) scale--;
  
  // Write to the control register(s).
  *(dsp->scale1) = scale;
  if (dsp->scale2) *(dsp->scale2) = scale;
  
}

/**
 * Driver mount callback.
 */
static rootfs_mount_ret_t drv_dsp_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Check arguments.
  if (isdir) { r.err_no = EISDIR; return r; }
  if (src || !arg) { r.err_no = EINVAL; return r; }
  mount_dsp_arg_t *dsp_arg = (mount_dsp_arg_t*)arg;
  
  // Make a copy of the argument (this driver doesn't need additional data).
  mount_dsp_arg_t *dsp = (mount_dsp_arg_t*)malloc(sizeof(mount_dsp_arg_t));
  if (!dsp) { r.err_no = ENOMEM; return r; }
  memcpy(dsp, dsp_arg, sizeof(mount_dsp_arg_t));
  
  // Default samplerate for /dev/dsp is a measly 8kHz.
  int srate = 8000;
  drv_dsp_samplerate(dsp, &srate);
  
  // Save the state structure as mount reference.
  r.drv_ref = (void*)dsp;
  
  // Success.
  return r;
}

/**
 * Driver umount callback.
 */
static int drv_dsp_umount(void *mount_ref) {
  
  // Check arguments.
  if (!mount_ref) return EINVAL;
  mount_dsp_arg_t *dsp = (mount_dsp_arg_t*)mount_ref;
  
  // Free the data structure.
  free(dsp);
  
  // Success.
  return 0;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_dsp_open(void *mount_ref, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref) { r.err_no = EINVAL; return r; }
  mount_dsp_arg_t *dsp = (mount_dsp_arg_t*)mount_ref;
  
  // Create a object reference structure.
  dsp_object_ref_t *ref = (dsp_object_ref_t*)calloc(1, sizeof(dsp_object_ref_t));
  if (!ref) { r.err_no = ENOMEM; return r; }
  
  // Initialize the format to the native one.
  ref->format = dsp->flags & DSP_MOUNT_FMT_MASK;
  
  // Success.
  r.drv_ref = (void*)ref;
  return r;
}

/**
 * Driver write implementation.
 */
static rootfs_write_ret_t drv_dsp_write(void *mount_ref, void *file_ref, const void *buf, size_t cnt) {
  rootfs_write_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  mount_dsp_arg_t *dsp = (mount_dsp_arg_t*)mount_ref;
  dsp_object_ref_t *ref = (dsp_object_ref_t*)file_ref;
  
  // Cache some format stuff in registers (if the compiler is not stupid).
  const int fmt = ref->format;
  const int flags = dsp->flags;
  int in_shift = 0;
  if (fmt & DSP_MOUNT_FMT_16BIT)  { in_shift++; }
  if (fmt & DSP_MOUNT_FMT_STEREO) { in_shift++; }
  
  // Convert cnt to samples.
  cnt >>= in_shift;
  
  // Return if the application isn't writing enough bytes.
  if (!cnt) return r;
  
  // Figure out how many samples we can write before we fill the buffer.
  unsigned int max_cnt = *(dsp->status);
  if (!(flags & DSP_MOUNT_STAT_AVAIL)) {
    if (flags & DSP_MOUNT_SIZE_REG) {
      max_cnt = *(dsp->size_reg) - max_cnt;
    } else {
      max_cnt = dsp->size_direct - max_cnt;
    }
  }
  
  // Return if there is no room in the buffer at all.
  if (max_cnt == 0) { r.err_no = EWOULDBLOCK; return r; }
  
  // Clamp cnt by the space available in the buffer.
  if (cnt > max_cnt) cnt = max_cnt;
  r.amount = cnt << in_shift;
  
  // Handle special cases where the supplied format matches the native one.
  // This prevents a lot of in-loop format conversion stuff.
  if (fmt == (flags & DSP_MOUNT_FMT_MASK)) {
    
    if (in_shift == 0) {
      
      // 8-bit samples (8-bit mono).
      const unsigned char *dbuf = (const unsigned char*)buf;
      while (cnt) {
        unsigned int d = *dbuf++;
        d |= d << 8;
        d |= d << 16;
        *(dsp->data) = d;
        cnt--;
      }
      return r;
      
    } else if (in_shift == 1) {
      
      // 16-bit samples (8-bit stereo or 16-bit mono).
      const unsigned short *dbuf = (const unsigned short*)buf;
      while (cnt) {
        unsigned int d = *dbuf++;
        d |= d << 16;
        *(dsp->data) = d;
        cnt--;
      }
      return r;
      
    } else if (in_shift == 2) {
      
      // 32-bit samples (16-bit stereo).
      const unsigned int *dbuf = (const unsigned int*)buf;
      while (cnt) {
        *(dsp->data) = *dbuf++;
        cnt--;
      }
      return r;
      
    }
    
  }
  
  // Iterate over the samples.
  const unsigned char *cbuf = (const unsigned char*)buf;
  while (cnt) {
    unsigned int ch1, ch2;
    
    // Read and convert first channel.
    ch1 = *cbuf++;
    if (fmt & DSP_MOUNT_FMT_16BIT) {
      if (fmt & DSP_MOUNT_FMT_LITTLE) {
        ch1 |= (*cbuf++) << 8;
      } else {
        ch1 <<= 8;
        ch1 |= *cbuf++;
      }
    } else {
      ch1 |= ch1 << 8;
    }
    
    // Read and convert second channel.
    if (fmt & DSP_MOUNT_FMT_STEREO) {
      ch2 = *cbuf++;
      if (fmt & DSP_MOUNT_FMT_16BIT) {
        if (fmt & DSP_MOUNT_FMT_LITTLE) {
          ch2 |= (*cbuf++) << 8;
        } else {
          ch2 <<= 8;
          ch2 |= *cbuf++;
        }
      } else {
        ch2 |= ch2 << 8;
      }
    } else {
      ch2 = ch1;
    }
    
    // If the output format is mono, mix the two channels.
    if (!(flags & DSP_MOUNT_FMT_STEREO)) {
      ch1 = (ch1 + ch2) >> 1;
      ch2 = ch1;
    }
    
    // Correct for difference in signedness.
    if ((flags ^ fmt) & DSP_MOUNT_FMT_SIGNED) {
      ch1 = (ch1 + 0x8000) & 0xFFFF;
      ch2 = (ch2 + 0x8000) & 0xFFFF;
    }
    
    // If the output format is 8-bit mono, intermix the channels to make the
    // final output work right.
    if (!(flags & DSP_MOUNT_FMT_16BIT) && (flags & DSP_MOUNT_FMT_STEREO)) {
      ch1 = (ch1 & 0xFF) | (ch2 & 0xFF00);
      ch2 = ch1;
    }
    
    // If the output format is little endian, swap everything.
    if (flags & DSP_MOUNT_FMT_LITTLE) {
      unsigned int x = ((ch1 >> 8) | (ch1 << 8)) & 0xFFFF;
      ch1 = ((ch2 >> 8) | (ch2 << 8)) & 0xFFFF;
      ch2 = x;
    }
    
    // Combine the channels and write to the register.
    *(dsp->data) = (ch1 << 16) | ch2;
    
    // Update the count variable.
    cnt--;
    
  }
  
  // Success.
  return r;
}

/**
 * Driver close callback.
 */
static int drv_dsp_close(void *mount_ref, void *file_ref) {
  
  // Free the file structure.
  free(file_ref);
  
  // Success.
  return 0;
}

/**
 * Driver ioctl callback.
 */
static rootfs_ioctl_ret_t drv_dsp_ioctl(void *mount_ref, void *file_ref, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!file_ref || !mount_ref) { r.err_no = EBADF; return r; }
  mount_dsp_arg_t *dsp = (mount_dsp_arg_t*)mount_ref;
  dsp_object_ref_t *ref = (dsp_object_ref_t*)file_ref;
  
  // Check argument existance.
  if ((request & 0xFFFF) && !param) { r.err_no = EINVAL; return r; }
  
  // Handle requests.
  int *iparam = (int*)param;
  int i;
  switch (request) {
    
    // No-op ioctls.
    case SNDCTL_DSP_RESET:
    case SNDCTL_DSP_POST:
      return r;
    
    // Wait for the buffer to run dry.
    case SNDCTL_DSP_SYNC: {
      
      // Determine what the empty value is for the status register.
      unsigned int empty_val;
      if (dsp->flags & DSP_MOUNT_STAT_AVAIL) {
        if (dsp->flags & DSP_MOUNT_SIZE_REG) {
          empty_val = *(dsp->size_reg);
        } else {
          empty_val = dsp->size_direct;
        }
      } else {
        empty_val = 0;
      }
      
      // Block until the status register contains the empty value.
      while (*(dsp->status) != empty_val);
      
      return r;
    }
    
    // Set samplerate.
    case SNDCTL_DSP_SPEED:
      drv_dsp_samplerate(dsp, iparam);
      return r;
    
    // Set stereo/mono.
    case SNDCTL_DSP_STEREO:
      
      // Set stereo flag.
      if (*iparam) {
        ref->format |= DSP_MOUNT_FMT_STEREO;
      } else {
        ref->format &= ~DSP_MOUNT_FMT_STEREO;
      }
      
      return r;
    
    // Return bogus block size.
    case SNDCTL_DSP_GETBLKSIZE:
      *iparam = 1024;
      return r;
    
    // Set sample format.
    case SNDCTL_DSP_SETFMT:
      if (*iparam == AFMT_QUERY) switch (ref->format & (DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_LITTLE)) {
        
        case DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_8BIT | DSP_MOUNT_FMT_BIG:
        case DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_8BIT | DSP_MOUNT_FMT_LITTLE:
          *iparam = AFMT_U8;
          return r;
        
        case DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_BIG:
          *iparam = AFMT_U16_BE;
          return r;
        
        case DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_LITTLE:
          *iparam = AFMT_U16_LE;
          return r;
        
        case DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_8BIT | DSP_MOUNT_FMT_BIG:
        case DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_8BIT | DSP_MOUNT_FMT_LITTLE:
          *iparam = AFMT_S8;
          return r;
        
        case DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_BIG:
          *iparam = AFMT_S16_BE;
          return r;
        
        case DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_LITTLE:
          *iparam = AFMT_S16_LE;
          return r;
        
      }
      
      ref->format &= ~(DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_LITTLE);
      switch (*iparam) {
        
        case AFMT_U8:
          ref->format |= DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_8BIT;
          return r;
        
        case AFMT_S16_LE:
          ref->format |= DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_LITTLE;
          return r;
        
        case AFMT_S16_BE:
          ref->format |= DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_BIG;
          return r;
        
        case AFMT_S8:
          ref->format |= DSP_MOUNT_FMT_SIGNED | DSP_MOUNT_FMT_8BIT;
          return r;
        
        case AFMT_U16_LE:
          ref->format |= DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_LITTLE;
          return r;
        
        default: *iparam = AFMT_U16_BE;
          ref->format |= DSP_MOUNT_FMT_UNSIGNED | DSP_MOUNT_FMT_16BIT | DSP_MOUNT_FMT_BIG;
          return r;
      }
    
    // Set number of channels (mono/stereo, different API).
    case SNDCTL_DSP_CHANNELS:
      
      i = *iparam;
      if (!i) {
        
        // Query.
        if (ref->format & DSP_MOUNT_FMT_STEREO) {
          *iparam = 2;
        } else {
          *iparam = 1;
        }
        return r;
        
      }
      
      // Clamp to boundaries.
      if (i < 1) i = 1;
      if (i > 2) i = 2;
      *iparam = i;
      
      // Set stereo flag.
      if (i == 2) {
        ref->format |= DSP_MOUNT_FMT_STEREO;
      } else {
        ref->format &= ~DSP_MOUNT_FMT_STEREO;
      }
      
      return r;
    
    // Return mask of supported formats.
    case SNDCTL_DSP_GETFMTS:
      *iparam = AFMT_U8 | AFMT_U16_LE | AFMT_U16_BE
              | AFMT_S8 | AFMT_S16_LE | AFMT_S16_BE;
      return r;
    
    // Return output buffer space information.
    case SNDCTL_DSP_GETOSPACE: {
      
      // We don't have a concept of fragments that we DMA to some DSP. Instead,
      // we just report the fragment size as one sample.
      
      // Log2 of the size of a sample.
      int samplesizesh = 0;
      if (ref->format & DSP_MOUNT_FMT_16BIT) samplesizesh++;
      if (ref->format & DSP_MOUNT_FMT_STEREO) samplesizesh++;
      
      // Total buffer size in samples.
      unsigned int size;
      if (dsp->flags & DSP_MOUNT_SIZE_REG) {
        size = *(dsp->size_reg);
      } else {
        size = dsp->size_direct;
      }
      
      // Amount of samples available for writing.
      unsigned int avail = *(dsp->status);
      if (!(dsp->flags & DSP_MOUNT_STAT_AVAIL)) {
        avail = size - avail;
      }
      
      // Populate the buffer.
      audio_buf_info *aparam = (audio_buf_info*)param;
      aparam->fragments = avail;
      aparam->fragstotal = size;
      aparam->fragsize = 1 << samplesizesh;
      aparam->bytes = avail << samplesizesh;
      
      return r;
    }
    
    // Return capability mask. Just a revision level here, we can't do much.
    case SNDCTL_DSP_GETCAPS:
      *iparam = 1;
      return r;
    
  }
  
  // Unknown ioctl.
  r.err_no = EINVAL;
  return r;
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(dsp) {
  .name   = DRIVER_NAME,
  .d_type = DT_CHR,
  .mount  = drv_dsp_mount,
  .umount = drv_dsp_umount,
  .open   = drv_dsp_open,
  .write  = drv_dsp_write,
  .close  = drv_dsp_close,
  .ioctl  = drv_dsp_ioctl
};

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created.
 */
int mount_dsp(const char *path, const mount_dsp_arg_t *arg) {
  
  // Make sure the file exists.
  if (touch(path)) return -1;
  
  // Try to do the mount.
  return mount(NULL, path, DRIVER_NAME, 0, (const void*)arg);
}

