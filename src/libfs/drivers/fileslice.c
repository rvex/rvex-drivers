/**
 * File slice driver. This creates a device file that is offset by some amount
 * of bytes and has a shorter length. It can be used to implement partition
 * files.
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <sys/rootfs.h>
#include <sys/dirent.h>
#include <fileslice.h>
#include <sys/mount.h>

#define DRIVER_NAME "fileslice"

/**
 * Slice mount information structure.
 */
typedef struct slice_info_t {
  
  // Backing file reference.
  rootfs_object_ref_t bf;
  
  // Start offset.
  off_t start;
  
  // Size.
  off_t size;
  
} slice_info_t;

/**
 * Slice object reference.
 */
typedef struct slice_object_ref_t {
  
  // Backing file reference.
  rootfs_object_ref_t bf;
  
  // Current file pointer.
  off_t pos;
  
} slice_object_ref_t;

/**
 * Driver mount implementation.
 */
static rootfs_mount_ret_t drv_fsl_mount(int isdir, char *src, unsigned long mountflags, const void *arg) {
  rootfs_mount_ret_t r = {};
  
  // Check arguments.
  if (isdir) { r.err_no = EISDIR; return r; }
  if (!src || !arg) { r.err_no = EINVAL; return r; }
  mount_fileslice_arg_t *slice_arg = (mount_fileslice_arg_t*)arg;
  
  // The start and size must be nonnegative.
  if ((slice_arg->start < 0) || (slice_arg->size < 0)) { r.err_no = EINVAL; return r; }
  
  // Allocate a rootfs object reference for the backed file. We keep this open
  // while mounted, so the source object cannot be freed.
  slice_info_t *slice = (slice_info_t*)calloc(1, sizeof(slice_info_t));
  if (!slice) { r.err_no = ENOMEM; return r; }
  
  // Save the start offset and size.
  slice->start = slice_arg->start;
  slice->size = slice_arg->size;
  
  // Open the source file. Note that we don't need read/write permission in
  // this file, we just keep it for the reference counter and to get the object
  // pointer.
  r.err_no = sys_rootfs_open(&(slice->bf), src, 0, 0);
  if (r.err_no) {
    free(slice);
    return r;
  }
  
  // Save the source file reference as the mount reference.
  r.drv_ref = (void*)slice;
  
  // Success.
  return r;
}

/**
 * Driver umount implementation.
 */
static int drv_fsl_umount(void *mount_ref) {
  
  // Check arguments.
  if (!mount_ref) return EINVAL;
  slice_info_t *slice = (slice_info_t*)mount_ref;
  
  // Close the backing file.
  sys_rootfs_close(&(slice->bf));
  
  // Free the data structure.
  free(slice);
  
  // Success.
  return 0;
}

/**
 * Driver open callback.
 */
static rootfs_open_ret_t drv_fsl_open(void *mount_ref, const char *fname, int flags, int mode) {
  rootfs_open_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref) { r.err_no = EINVAL; return r; }
  slice_info_t *slice = (slice_info_t*)mount_ref;
  
  // Create a file reference structure.
  slice_object_ref_t *ref = (slice_object_ref_t*)calloc(1, sizeof(slice_object_ref_t));
  if (!ref) { r.err_no = ENOMEM; return r; }
  
  // Forward the open to the source file.
  r.err_no = sys_rootfs_open_ob(&(ref->bf), slice->bf.ob, NULL, flags, mode);
  if (r.err_no) goto error;
  
  // If read or write access is requested, seek to the start of the slice.
  if (flags & (ROOTFS_O_READ | ROOTFS_O_WRITE)) {
    rootfs_lseek_ret_t sr = sys_rootfs_lseek(&(ref->bf), slice->start, SEEK_SET);
    if (sr.err_no) { r.err_no = sr.err_no; goto error; }
    ref->pos = 0;
  } else {
    ref->pos = -1;
  }
  
  // Save the backing file reference as the driver reference.
  r.drv_ref = (void*)ref;
  return r;
  
error:
  free(ref);
  return r;
}

/**
 * Returns the amount of bytes that should be read/written for the given
 * requested amount, taking the size of the slice into consideration. Also
 * checks arguments for read/write, returning -1 for a EBADF.
 */
static off_t drv_fsl_access_cnt(void *mount_ref, void *file_ref, size_t cnt) {
  
  // Check arguments.
  if (!mount_ref || !file_ref) return -1;
  slice_info_t *slice = (slice_info_t*)mount_ref;
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  
  // Check bounds.
  if (ref->pos < 0) return -1;
  if (ref->pos + cnt > slice->size) cnt = slice->size - ref->pos;
  
  // Return the bound-checked cnt.
  return (off_t)cnt;
}

/**
 * Driver read implementation.
 */
static rootfs_read_ret_t drv_fsl_read(void *mount_ref, void *file_ref, void *buf, size_t cnt) {
  rootfs_read_ret_t r = {};
  
  // Check arguments and bound-check cnt.
  off_t ocnt = drv_fsl_access_cnt(mount_ref, file_ref, cnt);
  if (ocnt == -1) { r.err_no = EBADF; return r; }
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  cnt = (size_t)ocnt;
  
  // Forward the read.
  r = sys_rootfs_read(&(ref->bf), buf, cnt);
  if (r.err_no) return r;
  
  // Increment our offset.
  ref->pos += r.amount;
  
  // Success.
  return r;
}

/**
 * Driver write implementation.
 */
static rootfs_write_ret_t drv_fsl_write(void *mount_ref, void *file_ref, const void *buf, size_t cnt) {
  rootfs_write_ret_t r = {};
  
  // Check arguments and bound-check cnt.
  off_t ocnt = drv_fsl_access_cnt(mount_ref, file_ref, cnt);
  if (ocnt == -1) { r.err_no = EBADF; return r; }
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  cnt = (size_t)ocnt;
  
  // Forward the write.
  r = sys_rootfs_write(&(ref->bf), buf, cnt);
  if (r.err_no) return r;
  
  // Increment our offset.
  ref->pos += r.amount;
  
  // Success.
  return r;
}

/**
 * Driver close callback.
 */
static int drv_fsl_close(void *mount_ref, void *file_ref) {
  
  // Check arguments.
  if (!file_ref) return EBADF;
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  
  // Forward the close.
  int res = sys_rootfs_close(&(ref->bf));
  if (res) return res;
  
  // Free the file structure.
  free(ref);
  
  // Success.
  return 0;
}

/**
 * Driver lseek callback.
 */
static rootfs_lseek_ret_t drv_fsl_lseek(void *mount_ref, void *file_ref, off_t offs, int whence) {
  rootfs_lseek_ret_t r = {};
  
  // Check arguments.
  if (!mount_ref || !file_ref) { r.err_no = EBADF; return r; }
  slice_info_t *slice = (slice_info_t*)mount_ref;
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  
  // Figure out the desired position.
  off_t new_pos = offs;
  switch (whence) {
    case SEEK_SET: break;
    case SEEK_CUR: new_pos += ref->pos; break;
    case SEEK_END: new_pos += slice->size; break;
    default: { r.err_no = EINVAL; return r; }
  }
  
  // Check range.
  if ((new_pos < 0) || (new_pos > slice->size)) {
    r.err_no = EINVAL;
    return r;
  }
  
  // If the desired offset equals the current one, don't do anything.
  if (new_pos == ref->pos) {
    r.pos = new_pos;
    return r;
  }
  
  // Forward the seek as a SEEK_SET.
  r = sys_rootfs_lseek(&(ref->bf), new_pos + slice->start, SEEK_SET);
  if (r.err_no) return r;
  
  // Subtract the slice offset from the position retrieved from lseek again,
  // and save it as the return value and our own position.
  r.pos -= slice->start;
  ref->pos = r.pos;
  
  // Success.
  return r;
}

/**
 * Driver stat callback.
 */
static int drv_fsl_stat(void *mount_ref, void *file_ref, struct stat *sb) {
  
  // Check arguments.
  if (!mount_ref || !file_ref) return EBADF;
  slice_info_t *slice = (slice_info_t*)mount_ref;
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  
  // Forward the stat.
  int res = sys_rootfs_stat(&(ref->bf), sb);
  if (res) return res;
  
  // Override the size.
  sb->st_size = slice->size;
  
  // Success.
  return 0;
};

/**
 * Driver ioctl callback.
 */
static rootfs_ioctl_ret_t drv_fsl_ioctl(void *mount_ref, void *file_ref, int request, void *param) {
  rootfs_ioctl_ret_t r = {};
  
  // Check arguments.
  if (!file_ref) { r.err_no = EBADF; return r; }
  slice_object_ref_t *ref = (slice_object_ref_t*)file_ref;
  
  // Forward the ioctl.
  return sys_rootfs_ioctl(&(ref->bf), request, param);
}

/**
 * Register the driver.
 */
ROOTFS_REGISTER_DRIVER(fileslice) {
  .name   = DRIVER_NAME,
  .d_type = DT_BLK,
  .mount  = drv_fsl_mount,
  .umount = drv_fsl_umount,
  .open   = drv_fsl_open,
  .read   = drv_fsl_read,
  .write  = drv_fsl_write,
  .close  = drv_fsl_close,
  .lseek  = drv_fsl_lseek,
  .stat   = drv_fsl_stat,
  .ioctl  = drv_fsl_ioctl
};

/**
 * Mount function for this driver. This is pretty much just "mount", but the
 * mount file is first created.
 */
int mount_fileslice(const char *src, const char *target, off_t start, off_t size) {
  
  // Make sure the file exists.
  if (touch(target)) return -1;
  
  // Try to do the mount.
  mount_fileslice_arg_t arg = { .start = start, .size = size };
  return mount(src, target, DRIVER_NAME, 0, (const void*)&arg);
}

