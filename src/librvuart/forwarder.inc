#include <driver-config.h>

#if ISSUE == 2
#define ISSUE_2 ;;
#define ISSUE_4 ;;
#elif ISSUE == 4
#define ISSUE_2
#define ISSUE_4 ;;
#else
#define ISSUE_2
#define ISSUE_4
#endif

#define RVEX_UART_FUNC(x) rvex_uart_ ## x

  .section .init
  .proc
FUNC::
  mov     $r0.5       = $r0.4
  mov     $r0.4       = $r0.3
ISSUE_2
  ldw     $r0.3       = RVEX_UART[$r0.0]
ISSUE_4
  goto    TARGET_FUNC
;;
  .endp

