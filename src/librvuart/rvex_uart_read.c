
#include <rvex-uart.h>

/**
 * POSIX-read-like interface for the UART.
 */
int rvex_uart_read(volatile rvex_uart_regs_t *regs, void *buf, int count) {
  char *cbuf = (char*)buf;
  int remain = count;
  
  // Read into the buffer.
  #pragma unroll(0)
  while (remain--) {
    
    // Wait for buffer ready.
    #pragma unroll(0)
    while (!(regs->stat & (RVU_CTI | RVU_RXDR)));
    
    // Read a byte.
    *cbuf++ = regs->data;
    
  }
  
  return count;
}

