
#include <rvex-uart.h>

/**
 * Reads from stdin until len-1 bytes have been read or \n is encountered.
 */
int rvex_uart_gets(volatile rvex_uart_regs_t *regs, char *buf, int len) {
  int remain = len - 1;
  
  // Read into the buffer.
  #pragma unroll(0)
  while (remain--) {
    
    // Wait for buffer ready.
    #pragma unroll(0)
    while (!(regs->stat & (RVU_CTI | RVU_RXDR)));
    
    // Read a byte.
    char b = regs->data;
    *buf++ = b;
    
    // Stop if we read a \n.
    if (b == '\n') {
      break;
    }
    
  }
  
  // Append null.
  *buf = '\0';
  
  return 0;
}
