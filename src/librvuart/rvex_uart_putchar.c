
#include <rvex-uart.h>

/**
 * putchar-like interface for the UART.
 */
int rvex_uart_putchar(volatile rvex_uart_regs_t *regs, int c) {
  
  // Wait for buffer ready.
  #pragma unroll(0)
  while (!(regs->stat & RVU_TXDR));
  
  // Write the character.
  regs->data = (char)c;
  
  return 0;
}
