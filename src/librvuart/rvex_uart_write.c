
#include <rvex-uart.h>

/**
 * POSIX-write-like interface for the UART.
 */
int rvex_uart_write(volatile rvex_uart_regs_t *regs, const void *buf, int count) {
  const char *cbuf = (const char*)buf;
  int remain = count;
  
  // Write all the data.
  #pragma unroll(0)
  while (remain--) {
    
    // Wait for buffer ready.
    #pragma unroll(0)
    while (!(regs->stat & RVU_TXDR));
    
    // Write a byte.
    regs->data = *cbuf++;
    
  }
  
  return count;
}
