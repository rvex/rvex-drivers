
#include <rvex-uart.h>

/**
 * Writes a hex string to the UART.
 */
int rvex_uart_putx(volatile rvex_uart_regs_t *regs, int value) {
  unsigned int val = (unsigned int)value;
  int i;
  char c;
  
  rvex_uart_putchar(regs, '0');
  rvex_uart_putchar(regs, 'x');
  
  #pragma unroll(0)
  for (i = 0; i < 8; i++) {
    c = (char)(val >> 28);
    c = (c < 10) ? ('0' + c) : ('A' + c - 10);
    rvex_uart_putchar(regs, c);
    val <<= 4;
  }
  
  return 0;
}
