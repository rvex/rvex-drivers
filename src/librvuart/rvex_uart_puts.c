
#include <rvex-uart.h>

/**
 * puts-like interface for the UART.
 */
int rvex_uart_puts(volatile rvex_uart_regs_t *regs, const char *s) {
  
  // Write all the data.
  #pragma unroll(0)
  while (*s) {
    
    // Wait for buffer ready.
    #pragma unroll(0)
    while (!(regs->stat & RVU_TXDR));
    
    // Write a byte.
    regs->data = *s++;
    
  }
  
  return 0;
}
