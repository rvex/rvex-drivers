
#include <irqmp.h>

/**
 * Clears a pending interrupt.
 */
void irq_clear(int irq) {
  IRQMP->clear = 1 << irq;
}
