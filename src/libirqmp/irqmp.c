
#include <driver-config.h>
#include <irqmp.h>
#include <rvex.h>

/**
 * Callback function lookup table/registry.
 */
static void (*volatile irqmp_handlers[NUM_CONTEXTS * IRQMP_IRQ_COUNT])(unsigned long data);

/**
 * Registry for the user-supplied "data" to pass to each callback function.
 */
static unsigned long irqmp_handlers_data[NUM_CONTEXTS * IRQMP_IRQ_COUNT];

/**
 * Interrupt callback.
 */
void IRQMP_HOOK_FN(int irq) {
  
  // Range checking.
  if ((irq < 0) || (irq >= IRQMP_IRQ_COUNT)) {
    return;
  }
  
  // Figure out which handler belongs to this interrupt/context.
  int h = CR_CID * IRQMP_IRQ_COUNT + irq;
  void (*handler)(unsigned long data) = irqmp_handlers[h];
  
  // If the handler exists, call it.
  if (handler) {
    handler(irqmp_handlers_data[h]);
  }
  
}

/**
 * Registers the specified interrupt handler function for the specified IRQ.
 * Only one handler can be registered at a time.
 */
void irq_register(
  int irq,
  void (*handler)(unsigned long data),
  unsigned long data
) {
  int h = CR_CID * IRQMP_IRQ_COUNT + irq;
  irqmp_handlers[h] = handler;
  irqmp_handlers_data[h] = data;
}
