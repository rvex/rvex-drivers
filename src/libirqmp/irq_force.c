
#include <irqmp.h>

/**
 * Forces the specified interrupt on the specified context.
 */
void irq_force(int irq, int context) {
  IRQMP->force[context] = 1 << irq;
}
