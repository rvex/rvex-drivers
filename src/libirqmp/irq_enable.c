
#include <irqmp.h>
#include <rvex.h>

/**
 * Enables or masks an interrupt.
 */
void irq_enable(int irq, int enable) {
  if (enable) {
    CR_CCR = CR_CCR_IEN_C;
    IRQMP->mask[CR_CID] |= 1 << irq;
    CR_CCR = CR_CCR_IEN;
  } else {
    CR_CCR = CR_CCR_IEN_C;
    IRQMP->mask[CR_CID] &= ~(1 << irq);
    CR_CCR = CR_CCR_IEN;
  }
}
