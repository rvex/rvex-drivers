
#include <irqmp.h>

/**
 * Returns whether the specified interrupt is pending.
 */
int irq_ispending(int irq) {
  return (IRQMP->pending & (1 << irq)) ? 1 : 0;
}
