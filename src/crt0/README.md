This driver boots the r-VEX when the "bare" runtime is chosen. It sets up the
stack, optionally clears the `.bss` section and registers, sets up a trap and
panic handler and then calls `main()`.

If interrupts are enabled through `rvex-cfg.py`, the user should supply a
function with the following prototype for handling interrupts:

```
void interrupt(int irq);
```

irq is set to the trap argument.

If panic print is enabled, `putc` should be defined.
