This is a simple r-VEX debug UART driver, directly implementing newlib's
`sys_read()` and `sys_write()` syscalls. It only supports one UART, and only
works when the UART is the only "file" that's used, but is much more lightweight
than the complete filesystem driver.
