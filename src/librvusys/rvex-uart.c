/**
 * Simple r-VEX debug UART driver, directly implementing newlib's sys_read()
 * and sys_write() syscalls. Only uses the first defined UART, and only works
 * when the UART is the only "file" that's used, but is much more lightweight
 * than the complete filesystem driver.
 */

#include <driver-config.h>
#include <sys/types.h>
#include <sys/errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <machine/rvex-sys.h>
#include <rvusys.h>

/**
 * Define the plug-and-play list for statically getting the UART address from
 * the program.
 */
PNP_DEFINE(rvusys_device);

#define RVEX_UART ((volatile rvex_uart_regs_t*)(rvusys_device_list[1]))

/**
 * Implements the read syscall.
 */
#ifdef ENABLE_SYS_READ
rvex_sysreturn_t sys_read(int fd, void *buf, size_t cnt) {
  char *cbuf = (char*)buf;
  int remain = cnt;
  
  // Check the file descriptor.
  if (fd != 0) {
    rvex_sysreturn_err(EBADF);
  }
  
  // Read into the buffer.
  #pragma unroll(0)
  while (remain--) {
    
    // Wait for buffer ready.
    #pragma unroll(0)
    while (!(RVEX_UART->stat & (RVU_CTI | RVU_RXDR)));
    
    // Read a byte.
    *cbuf++ = RVEX_UART->data;
    
  }
  
  rvex_sysreturn_ok32(cnt);
}
#endif

/**
 * Implements the write syscall.
 */
#ifdef ENABLE_SYS_WRITE
rvex_sysreturn_t sys_write(int fd, const void *buf, size_t cnt) {
  const char *cbuf = (const char*)buf;
  int remain = cnt;
  
  // Check the file descriptor.
  if ((fd != 1) && (fd != 2)) {
    rvex_sysreturn_err(EBADF);
  }
  
  // Write all the data.
  #pragma unroll(0)
  while (remain--) {
    
    // Wait for buffer ready.
    #pragma unroll(0)
    while (!(RVEX_UART->stat & RVU_TXDR));
    
    // Write a byte.
    RVEX_UART->data = *cbuf++;
    
  }
  
  rvex_sysreturn_ok32(cnt);
}
#endif
