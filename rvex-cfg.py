#!/usr/bin/env python3

import sys
import json

if __name__ == '__main__':
    
    # Check command line/print usage.
    if len(sys.argv) < 2:
        print('Usage: %s <cfg.json>' % sys.argv[0], file=sys.stderr)
        print('', file=sys.stderr)
        print('Configures the source tree in the current directory according to the given', file=sys.stderr)
        print('configuration file. Returns 0 if successful, 3 if the tool should not be', file=sys.stderr)
        print('installed, or something else (usually 1) if an error occurred.', file=sys.stderr)
        print('', file=sys.stderr)
        print('You can also run `%s -` to have the script generate default versions of the', file=sys.stderr)
        print('source files. These are the ones that should be checked into the git repos.', file=sys.stderr)
        sys.exit(2)
    
    # Load the configuration file.
    if sys.argv[1].strip() == '-':
        cfg_json = {}
    else:
        with open(sys.argv[1], 'r') as f:
            cfg_json = json.load(f)

class Config(object):
    
    def __init__(self, d, prefix=''):
        self._d = d
        self._p = prefix
        self._s = set()
        for key in d:
            if not key.startswith(prefix):
                continue
            key = key[len(prefix):].split('.')
            if len(key) == 1:
                continue
            self._s.add(key[0])
    
    def __getattr__(self, name):
        val = self._d.get(self._p + name, None)
        if val is not None:
            return val
        
        if name in self._s:
            return Config(self._d, self._p + name + '.')
        
        raise AttributeError(name)
    
    def __str__(self):
        data = ['    %s: %s\n' % (key[len(self._p):], value) for key, value in sorted(self._d.items()) if key.startswith(self._p)]
        return 'Config(\n' + ''.join(data) + ')'

def parse_cfg(cfg_json, defaults):
    """Parses the config.json contents into a Config object that is guaranteed
    to contain the keys in `defaults`. An error is printed if the given cfg_json
    dict contains non-default keys which are not present in the `defaults`
    dict, which implies that the configuration file has a higher version than
    this file and is doing something that isn't backwards compatible."""
    
    # Check that we don't have any "required" keys that we don't know about.
    for key, value in cfg_json.items():
        if value['req'] and key not in defaults:
            print('Error: found non-default key "%s", which this version of rvex-cfg.py doesn\'t know about!' % key, file=sys.stderr)
            sys.exit(1)
    
    # Build our cfg dictionary.
    cfg = {}
    for key, default in defaults.items():
        value = cfg_json.get(key, {'val': default})
        if value is None:
            print('Error: no configuration value specified for key "%s"!' % key, file=sys.stderr)
            sys.exit(1)
        cfg[key] = value['val']
    
    return Config(cfg)

def template(infname, outfname, *args, **kwargs):
    """Template engine simply using Python's str.format()."""
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            outf.write(inf.read().format(*args, **kwargs))

def template2(infname, outfname, *args, **kwargs):
    """Template engine using Python's str.format(), but with different
    characters that are more suitable for C:
     - open:   <|
     - close:  |>
     - escape: <-| to <|
               |-> to |>
    """
    with open(infname, 'r') as inf:
        with open(outfname, 'w') as outf:
            data = inf.read()
            data = data.replace('{', '{{').replace('}', '}}')
            data = data.replace('<|', '{').replace('|>', '}')
            data = data.format(*args, **kwargs)
            data = data.replace('<-|', '<|').replace('|->', '|>')
            outf.write(data)

def dont_build(s=''):
    if s:
        s = ': ' + s
    print('This tool does not need to be built%s' % s, file=sys.stderr)
    sys.exit(3)

def misconfigured(s):
    if s:
        s = ': ' + s
    print('Configuration error%s' % s, file=sys.stderr)
    sys.exit(1)

def done(s):
    if s:
        s = ': ' + s
    print('Configuration complete%s' % s, file=sys.stderr)
    sys.exit(0)

#===============================================================================
# Tool-specific from here onwards
#===============================================================================

import os

cfg = parse_cfg(cfg_json, {
    
    # Hardware configuration.
    'resources.num_lanes': 8,
    'resources.num_contexts': 4,
    
    # Memory configuration.
    'memory.stack_size': 131072,
    
    # Runtime.
    'software.runtime': 'bare',
    
    # Which syscalls are enabled.
    'software.syscalls.setup':        True,
    'software.syscalls.exit':         True,
    'software.syscalls.read':         True,
    'software.syscalls.write':        True,
    'software.syscalls.open':         True,
    'software.syscalls.close':        True,
    'software.syscalls.lseek':        True,
    'software.syscalls.stat':         True,
    'software.syscalls.fstat':        True,
    'software.syscalls.ioctl':        True,
    'software.syscalls.fcntl':        True,
    'software.syscalls.link':         True,
    'software.syscalls.unlink':       True,
    'software.syscalls.mkdir':        True,
    'software.syscalls.rename':       True,
    'software.syscalls.mount':        True,
    'software.syscalls.umount':       True,
    'software.syscalls.fork':         True,
    'software.syscalls.execve':       True,
    'software.syscalls.waitpid':      True,
    'software.syscalls.kill':         True,
    'software.syscalls.gettimeofday': True,
    'software.syscalls.times':        True,
    'software.syscalls.sbrk':         True,
    'software.syscalls.irq':          True,
    'software.syscalls.panic':        True,
    'software.syscalls.getpdata':     True,
    
    # crt0 configuration.
    'software.crt0.disable_bss_clear': False,
    'software.crt0.disable_regfile_clear': False,
    'software.crt0.disable_interrupts': False,
    'software.crt0.disable_panic': False,
    'software.crt0.disable_argv': False,
    
    # libfs configuration.
    'software.libfs.max_fd': 16,
    'software.libfs.enable_rvex_uart': True,
    'software.libfs.enable_devmem': True,
    
    # irqmp configuration.
    'software.irqmp.irq_count': 16
    
})

if not os.path.exists('config'):
    os.makedirs('config')

with open('config/driver-config.h', 'w') as f:
    def define(d, c):
        if isinstance(c, bool):
            if c:
                f.write('#define %s\n' % d)
            else:
                f.write('#undef  %s\n' % d)
        else:
            f.write('#define %s %s\n' % (d, c))
    
    f.write('\n')
    f.write('// Generic assembly file configuration.\n')
    define('ISSUE', cfg.resources.num_lanes)
    define('NUM_CONTEXTS', cfg.resources.num_contexts)
    f.write('\n')
    f.write('// crt0 configuration.\n')
    define('CONTEXT_STACK_SIZE', '0x%X' % (cfg.memory.stack_size // cfg.resources.num_contexts))
    define('ENABLE_BSS_CLEAR',     not cfg.software.crt0.disable_bss_clear)
    define('ENABLE_REGFILE_CLEAR', not cfg.software.crt0.disable_regfile_clear)
    define('ENABLE_ARGV',          not cfg.software.crt0.disable_argv)
    define('ENABLE_PANIC_PRINT',   not cfg.software.crt0.disable_panic)
    define('ENABLE_INTERRUPTS',    not cfg.software.crt0.disable_interrupts)
    f.write('\n')
    f.write('// newlib syscall usage selection.\n')
    for sc in [
        'setup', 'exit', 'read', 'write', 'open', 'close', 'lseek', 'stat',
        'fstat', 'ioctl', 'fcntl', 'link', 'unlink', 'mkdir', 'rename', 'fork',
        'execve', 'waitpid', 'kill', 'gettimeofday', 'times', 'sbrk', 'irq',
        'panic', 'getpdata', 'mount', 'umount'
    ]:
        define('ENABLE_SYS_%s' % sc.upper(), getattr(cfg.software.syscalls, sc))
    f.write('\n')
    f.write('// libfs configuration.\n')
    define('MAX_FD', cfg.software.libfs.max_fd)
    f.write('\n')
    define('ENABLE_RVEX_UART_READ',  cfg.software.libfs.enable_rvex_uart and cfg.software.syscalls.read)
    define('ENABLE_RVEX_UART_WRITE', cfg.software.libfs.enable_rvex_uart and cfg.software.syscalls.write)
    define('ENABLE_RVEX_UART_FSTAT', cfg.software.libfs.enable_rvex_uart and (cfg.software.syscalls.stat or cfg.software.syscalls.fstat))
    f.write('\n')
    define('ENABLE_DEVMEM_READ',  cfg.software.libfs.enable_devmem and cfg.software.syscalls.read)
    define('ENABLE_DEVMEM_WRITE', cfg.software.libfs.enable_devmem and cfg.software.syscalls.write)
    define('ENABLE_DEVMEM_LSEEK', cfg.software.libfs.enable_devmem and cfg.software.syscalls.lseek)
    define('ENABLE_DEVMEM_FSTAT', cfg.software.libfs.enable_devmem and (cfg.software.syscalls.stat or cfg.software.syscalls.fstat))
    f.write('\n')
    f.write('// irqmp configuration.\n')
    define('IRQMP_IRQ_COUNT', cfg.software.irqmp.irq_count)
    define('IRQMP_HOOK_FN', 'sys_irq' if cfg.software.runtime == 'newlib' else 'interrupt')
    f.write('\n')

libs = []
if cfg.software.runtime == 'bare':
    libs.append('crt0')
    libs.append('librvuart')
else:
    libs.append('libfs')
    libs.append('librvusys')
libs.append('libirqmp')

libs = ' '.join(libs)

template2('Makefile.in.tpl', 'Makefile.in', libs=libs)

done('drivers enabled: %s' % libs)
