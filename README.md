r-VEX drivers
=============

This repository contains some libraries for the r-VEX, most notably `crt0` for
the `bare` runtime. The libraries for the `newlib` runtime can hardly be called
complete, but at least they provide an example for implementing `newlib`'s
syscalls.


Configuration and build process
-------------------------------

Configuring and building the drivers works like this:

 - `rvex-cfg.py <json>`. This stage configures which libraries should be
   compiled based on the selected runtime, and configures the features of the
   compiled libraries. This is automatically run by the r-VEX toolchain
   generator, in which case the JSON file is generated based on a user-friendly
   INI file. If you're building manually, you can run `rvex-cfg.py -` instead to
   generate the required files with default values (`bare` runtime).
 - `configure`. Generates a Makefile for the later steps. Allows you to set a
   custom build directory (by running the `configure` command from another
   working directory) and a custom installation directory (defaults to
   `install`).
 - `make`. Compiles the tools.
 - `make install`. Copies the generated files to the specified `install`
   directory.

