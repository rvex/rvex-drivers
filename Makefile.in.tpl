
.SUFFIXES:

PREFIX  = @prefix@
SRCDIR = @srcdir@
LIBS = <|libs|>

VPATH = $(SRCDIR)

.PHONY: all
all: $(LIBS)

.PHONY: install
install: $(patsubst %,install-%,$(LIBS))

.PHONY: clean
clean:
	rm -rf bin

.PHONY: $(LIBS)
$(LIBS): %:
	mkdir -p bin/$@
	$(MAKE) -C bin/$@ -f $(SRCDIR)/src/$@/Makefile SRCDIR=$(SRCDIR)/src/$@ PREFIX=$(PREFIX)

.PHONY: $(patsubst %,install-%,$(LIBS))
$(patsubst %,install-%,$(LIBS)): install-%: %
	$(MAKE) -C bin/$< -f $(SRCDIR)/src/$</Makefile SRCDIR=$(SRCDIR)/src/$< PREFIX=$(PREFIX) install

